import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
//import commonjs from '@rollup/plugin-commonjs'
import path from 'path';

// https://vitejs.dev/config/
export default defineConfig(env => ({
  plugins: [
    vue()
  ],
  define: {
    PX_PLATFORM_BROWSER: false,
    PX_PLATFORM_ELECTRON: false,
    PX_PLATFORM_ELECTRON_MAIN: true,
    PX_VERSION: {
      version: '0.1.0'
    }
  },
  build: {
    lib: {
      entry: env.forgeConfigSelf.entry,
      fileName: () => '[name].js',
      formats: 'es',
    },
  },
  /*build: {
    rollupOptions: {
      plugins: [
        commonjs({
          dynamicRequireTargets: 'src/electron/api/storage-sqlite.ts'
        })
      ]
    }
  },*/
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "./src"),
    },
  }
}))