# Charsy

Charsy is an image gallery / booru client for desktop and web designed to make content easier to discover and manage.

## Supported services
- Danbooru
- E621
- Reddit
- DeviantArt
- Derpibooru

## Project setup
### Install dependencies
```
npm install
```

### Run in Electron
Update `services.json` and `preferences.json` (can be copied from `defaults` directory) to change list of enabled services and set various options like home screen items.

Note that for now services that require API keys (Reddit and DeviantArt) do not have their keys stored in this repository.
```
npm run electron:serve
```

### Serve for browser
```
npm run serve
```