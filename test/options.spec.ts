import { assert } from 'chai';
import { describe, it } from 'vitest';
import { Options, OptionsObject } from '../src/electron/options/options';

describe('Options', function () {
    describe('Observable', function () {
        describe('Simple', function () {
            it('Should not finish', function () {
                const options = new Options(1);
                options.getValueObservable().subscribe({
                    complete() {
                        assert.fail('Observable completed');
                    },
                    error(err) {
                        assert.fail(`Observable completed with an error: ${err}`);
                    }
                });
            });
            it('Should return initial value', function () {
                const options = new Options(1);
                let value = null;
                options.getValueObservable().subscribe({
                    next(nextValue) {
                        value = nextValue;
                    }
                });
                assert.equal(value, 1);
            });
            it('Should return new value on update', function () {
                const options = new Options(1);
                let value = null;
                options.getValueObservable().subscribe({
                    next(nextValue) {
                        value = nextValue;
                    }
                });
                options.setValue(2);
                assert.equal(value, 2);
            });
        });
        describe('Object', function () {
            it('Should return initial value', function () {
                const options = new OptionsObject({ a: 1 });
                let value = null;
                options.getValueObservable().subscribe({
                    next(nextValue) {
                        value = nextValue;
                    }
                });
                assert.deepEqual(value, { a: 1 });
            });
            it('Should return new value on ref update', function () {
                const options = new OptionsObject<{ a?: number, b?: number }>({ a: 1 });
                let value = null;
                options.getValueObservable().subscribe({
                    next(nextValue) {
                        value = nextValue;
                    }
                });
                options.setValue({ b: 2 });
                assert.deepEqual(value, { b: 2 });
            });
            it('Should return new value on direct child update', function () {
                const options = new OptionsObject<{ a?: number, b?: number }>({ a: 1 });
                let value = null;
                options.getValueObservable().subscribe({
                    next(nextValue) {
                        value = nextValue;
                    }
                });
                const child = options.getChild('a');
                child.setValue(2);
                assert.deepEqual(value, { a: 2 });
            });
            it('Should return new value on indirect child update', function () {
                const options = new OptionsObject({ a: { b: 1 } });
                let value = null;
                options.getValueObservable().subscribe({
                    next(nextValue) {
                        value = nextValue;
                    }
                });
                const child = options.getChild('a');
                const nestedChild = child.getChild('b');
                nestedChild.setValue(2);
                assert.deepEqual(value, { a: { b: 2 } });
            });
            it('Should not return old child value on its removal', function () {
                const options = new OptionsObject<{ a?: number, b?: number }>({ a: 1, b: 2 });
                let value = null;
                options.getValueObservable().subscribe({
                    next(nextValue) {
                        value = nextValue;
                    }
                });
                const child = options.getChild('a');
                child.setValue(undefined);
                assert.deepEqual(value, { b: 2 });
            });
            it('Should not return old child value on ref clear', function () {
                const options = new OptionsObject<{ a?: number, b?: number }>({ a: 1, b: 2 });
                let value = null;
                options.getValueObservable().subscribe({
                    next(nextValue) {
                        value = nextValue;
                    }
                });
                options.setValue(null);
                assert.deepEqual(value, null);
            });
            it('Should not return old child value on its update after ref clear', function () {
                const options = new OptionsObject<{ a?: number, b?: number }>({ a: 1, b: 2 });
                let value = null;
                options.getValueObservable().subscribe({
                    next(nextValue) {
                        value = nextValue;
                    }
                });
                const child = options.getChild('a');
                options.setValue(null);
                child.setValue(2);
                assert.deepEqual(value, null);
            });
            it('Should return child value on its update after ref clear and reset', function () {
                const options = new OptionsObject<{ a?: number, b?: number }>({ a: 1, b: 2 });
                let value = null;
                options.getValueObservable().subscribe({
                    next(nextValue) {
                        value = nextValue;
                    }
                });
                const child = options.getChild('a');
                options.setValue(null);
                options.setValue({ a: 1 });
                child.setValue(2);
                assert.deepEqual(value, { a: 2 });
            });
        });
    });
});