import { createApp } from 'vue';
import App from './App.vue';
import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router';
import DownloadsView from '@/components/download/downloads-view.vue';
import ServiceHomeScreen from '@/components/service/service-home-screen.vue';
import TestView from '@/components/test/test-view.vue';
import _ from 'lodash';
import { take } from 'rxjs/operators';
import { initLogger } from '@/logging';
import { remoteLog } from '@/api/logging';

const routes: RouteRecordRaw[] = [{
    name: 'gallery',
    path: '/gallery/:pathType/:descriptor/:page/:path(.+)',
    // @ts-ignore
    props: (route) => {
        const params = _.cloneDeep(route.params);
        if (_.isString(params.descriptor)) {
            params.descriptor = JSON.parse(atob(params.descriptor));
        }
        if (_.isString(params.items)) {
            params.items = JSON.parse(params.items);
        }
        if (params.page === 'null') {
            delete params.page;
        }
        return params;
    },
    // @ts-ignore
    component: async () => { const PxGallery = await import('@/components/gallery/gallery.vue'); return PxGallery }
}, {
    name: 'downloads',
    path: '/downloads',
    // @ts-ignore
    component: DownloadsView
}, {
    name: 'service-home-screen',
    path: '/service/:service/:path(.+)',
    // @ts-ignore
    props: (route) => {
        const params = _.cloneDeep(route.params);
        if (_.isString(params.service)) {
            params.service = JSON.parse(atob(params.service));
        }
        return params;
    },
    // @ts-ignore
    component: ServiceHomeScreen
}, {
    name: 'home',
    path: '/',
    props: {
        global: true,
        // @ts-ignore
        path: '/home'
    },
    // @ts-ignore
    component: ServiceHomeScreen
}, {
    name: 'omniboxSearch',
    path: '/search/:query(.+)',
    // @ts-ignore
    props: route => ({
        global: true,
        query: route.params.query
    }),
    // @ts-ignore
    component: ServiceHomeScreen
}, {
    name: 'test',
    path: '/test',
    // @ts-ignore
    component: TestView
}];

const router = createRouter({
    history: createWebHashHistory(),
    routes
});

const logger = initLogger();
logger.addReporter(remoteLog);

// @ts-ignore
const vue = createApp(App);

vue.use(router);

// @ts-ignore
if (!PX_PLATFORM_BROWSER) {
    vue.mount('#app');
} else {
    Promise.all([
        import('@/electron/api/service'),
        import('@/electron/api/storage'),
        import('@/electron/api/bookmark'),
        import('@/electron/api/omnibox'),
        import('@/electron/api/preferences'),
        import('@/electron/api/screen')
    ]).then(modules => {
        const [serviceM, storageM, bookmarkM, omniboxM, preferencesM, screenM] = modules;

        const omnibox = new omniboxM.default();
        omnibox.registerIpcHandlers();

        const preferences = new preferencesM.PreferencesApi();

        const serviceApi = new serviceM.ServiceApi({
            derpibooru: {
                type: 'derpibooru',
                title: 'Derpibooru',
                options: {
                    ratingFilter: ['s']
                }
            }
        }, omnibox, preferences);
        const storageApi = new storageM.StorageApi(preferences);
        serviceApi.registerIpcHandlers();
        storageApi.registerIpcHandlers();

        const bookmarkApi = new bookmarkM.BookmarkApi(storageApi, serviceApi);
        bookmarkApi.registerIpcHandlers();

        const screenApi = new screenM.ScreenApi(preferences, omnibox, storageApi, serviceApi);
        screenApi.registerIpcHandlers();

        serviceApi.getServiceObservable()
            .pipe(
                take(1)
            )
            .subscribe(() => vue.mount('#app'));
    });
}
