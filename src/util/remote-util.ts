import _ from 'lodash';

export const _browserIpcHandlers: {
    [channel: string]: (...args: any[]) => any
} = {};

export function isElectron(): boolean {
    return PX_PLATFORM_ELECTRON || PX_PLATFORM_ELECTRON_MAIN;
}

export let invokeIpcFn: <T>(...args: any[]) => Promise<T> | T;
export let registerIpcHandler: <T>(channel: string, handler: (...args: any[]) => Promise<T> | T) => void;

if (isElectron()) {
    let electronPromise: Promise<any>,
        logger: any;
    invokeIpcFn = async function <T>(channel: string, ...args: any[]) {
        return CharsyApi.sendMessage<T>(channel, ...args.map(_.cloneDeep));
    }
    registerIpcHandler = function <T>(channel: string, handler: (...args: any[]) => Promise<T> | T) {
        if (electronPromise == null) {
            electronPromise = import('electron');
        }
        electronPromise.then(electron => {
            electron.ipcMain?.handle(channel, async (event: Electron.IpcMainInvokeEvent, ...args: any[]) => {
                if (logger == null) {
                    const logging = await import('@/logging');
                    logger = logging.createLogger('IPC');
                }

                try {
                    return await handler(...args);
                } catch (e) {
                    logger.error(`Failed to handle ${channel}: ${e}`, e);
                    throw e;
                }
            });
        })
    }
} else {
    invokeIpcFn = function (channel: string, ...args: any[]) {
        return _browserIpcHandlers[channel]?.(...args);
    }
    registerIpcHandler = function <T>(channel: string, handler: (...args: any[]) => Promise<T> | T) {
        _browserIpcHandlers[channel] = handler;
    }
}