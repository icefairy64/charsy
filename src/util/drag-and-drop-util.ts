import * as _ from 'lodash';
import { Post } from '@/electron/service/service';

export function parsePostDropData(dropData: any): Post[] | null {
    const postJson = dropData['text/json'];

    if (postJson != null) {
        const posts = JSON.parse(postJson);
        if (_.isArray(posts)) {
            return posts;
        } else {
            return [posts];
        }
    }

    return null;
}