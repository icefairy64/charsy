import {
    DB_TABLE_BOOKMARKS,
    DB_TABLE_COLLECTIONS,
    DB_TABLE_DOWNLOADS, DB_TABLE_FILECACHE, DB_TABLE_POSTS,
    DB_TABLE_POSTS_FTS
} from '@/electron/api/storage-sqlite';

let migrations: string[][] | undefined = undefined;

function getMigrations() {
    if (!migrations) {
        migrations = [];

        migrations.push([
            `ALTER TABLE ${DB_TABLE_BOOKMARKS}
                ADD COLUMN download BOOLEAN NOT NULL DEFAULT false;`,

            `ALTER TABLE ${DB_TABLE_COLLECTIONS}
                ADD COLUMN download BOOLEAN NOT NULL DEFAULT false;`
        ]);

        migrations.push([
            `CREATE TABLE ${DB_TABLE_DOWNLOADS} (
                post_url TEXT NOT NULL PRIMARY KEY,
                sub_dir TEXT,
                status TEXT
            );`
        ]);

        migrations.push([
            `ALTER TABLE ${DB_TABLE_COLLECTIONS}
                ADD COLUMN sync_descriptor TEXT;`,
            `ALTER TABLE ${DB_TABLE_COLLECTIONS}
                ADD COLUMN sync_enabled BOOLEAN NOT NULL DEFAULT false;`
        ]);

        migrations.push([
            `CREATE VIRTUAL TABLE ${DB_TABLE_POSTS_FTS} USING fts5(post_url UNINDEXED, title, author);`,

            `INSERT INTO ${DB_TABLE_POSTS_FTS} SELECT p.url post_url, json_extract(p.data, '$.title') title, json_extract(p.data, '$.author') author FROM posts p;`,

            `CREATE TRIGGER posts_sync_fts_after_insert AFTER INSERT ON ${DB_TABLE_POSTS} BEGIN
              INSERT INTO ${DB_TABLE_POSTS_FTS}(post_url, title, author) VALUES (NEW.url, json_extract(NEW.data, '$.title'), json_extract(NEW.data, '$.author'));
            END;`,

            `CREATE TRIGGER posts_sync_fts_after_delete AFTER DELETE ON ${DB_TABLE_POSTS} BEGIN
              INSERT INTO ${DB_TABLE_POSTS_FTS}(${DB_TABLE_POSTS_FTS}, post_url, title, author) VALUES ('delete', OLD.url, json_extract(OLD.data, '$.title'), json_extract(OLD.data, '$.author'));
            END;`,

            `CREATE TRIGGER posts_sync_fts_after_update AFTER UPDATE ON ${DB_TABLE_POSTS} BEGIN
              DELETE FROM ${DB_TABLE_POSTS_FTS} WHERE post_url = OLD.url;
              INSERT INTO ${DB_TABLE_POSTS_FTS}(post_url, title, author) VALUES (NEW.url, json_extract(NEW.data, '$.title'), json_extract(NEW.data, '$.author'));
            END;`
        ]);

        migrations.push([
            `ALTER TABLE ${DB_TABLE_FILECACHE}
                ADD COLUMN size INTEGER;`,
            `ALTER TABLE ${DB_TABLE_FILECACHE}
                ADD COLUMN created_at INTEGER;`
        ]);

        migrations.push([
            `UPDATE posts SET data = json_replace(data, '$.rating', 's') WHERE substr(url, 0, 14) = 'https://derpi' AND json_extract(data, '$.tags') like '%"safe"%';`,
            `UPDATE posts SET data = json_replace(data, '$.rating', 'q') WHERE substr(url, 0, 14) = 'https://derpi' AND (json_extract(data, '$.tags') like '%"suggestive"%' OR json_extract(data, '$.tags') like '%"questionable"%');`,
            `UPDATE posts SET data = json_replace(data, '$.rating', 'e') WHERE substr(url, 0, 14) = 'https://derpi' AND json_extract(data, '$.tags') like '%"explicit"%';`
        ])
    }
    return migrations;
}

export function getMigrationScripts(currentSchemaVersion: number) {
    return getMigrations()
        .map((sql, index) => ({
            sql,
            version: index + 1
        }))
        .slice(currentSchemaVersion);

}

