import { isElectron } from '@/util/remote-util';
import { getIpcEventSubject } from '@/ipc-event-reactive';

export let broadcastClientEvent: (eventName: string, ...args: any[]) => void;

if (isElectron() && !(global && global.navigator)) {
    const electronPromise = import('electron');
    broadcastClientEvent = function (eventName: string, ...args: any[]) {
        electronPromise.then(electron => electron.webContents.getAllWebContents().forEach(wnd => wnd.send(eventName, ...args)));
    }
} else {
    broadcastClientEvent = function (eventName: string, ...args: any[]) {
        getIpcEventSubject(eventName).next(args);
    };
}