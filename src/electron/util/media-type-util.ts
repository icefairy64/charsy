import path from 'path';

const extensionToMediaTypeMap: Record<string, string[]> = {
    jpg: ['image', 'jpeg'],
    jpeg: ['image', 'jpeg'],
    png: ['image', 'png'],
    gif: ['image', 'gif'],
    webp: ['image', 'webp'],
    mp4: ['video', 'mp4'],
    webm: ['video', 'webm'],
    gifv: ['video', 'mp4'],
    zip: ['application', 'zip'],
}

export type MediaTypeRecord = {
    type: string,
    subtype: string,
    extension?: string,
}

export function getFilenameMediaType(filename: string): MediaTypeRecord | null {
    if (filename.startsWith('http')) {
        filename = new URL(filename).pathname;
    }
    let filenameExt = path.extname(filename);
    if (filenameExt != null && filenameExt.indexOf('?') > 0) {
        filenameExt = filenameExt.substring(0, filenameExt.indexOf('?'));
    }
    if (filenameExt != null && filenameExt.startsWith('.')) {
        filenameExt = filenameExt.substring(1);
    }
    if (filenameExt != null) {
        const recordData = extensionToMediaTypeMap[filenameExt.toLocaleLowerCase()];
        if (recordData != null) {
            return {
                type: recordData[0],
                subtype: recordData[1],
                extension: filenameExt
            };
        }
    }
    return null;
}

export function parseMediaType(mediaType: string): MediaTypeRecord {
    const parts = mediaType.split('/');
    return {
        type: parts[0],
        subtype: parts[1]
    };
}

export function getMediaTypeString(mediaType: MediaTypeRecord): string {
    return `${mediaType.type}/${mediaType.subtype}`;
}

