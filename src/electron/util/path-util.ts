import _ from 'lodash';

const sep = '/';

export function splitPath(path: string): Array<string> {
    let parts = path.split(sep);
    if (parts[0] === '') {
        parts = parts.slice(1);
    }
    return [parts[0], ['', ...parts.slice(1)].join(sep)];
}

export function splitPathLast(path: string): Array<string> {
    let parts = path.split(sep);
    if (parts[0] === '') {
        parts = parts.slice(1);
    }
    return [['', ...parts.slice(0, parts.length - 1)].join(sep), parts[parts.length - 1]];
}

export function joinPath(...parts: string[]) {
    return parts
        .map(x => _.trim(x, '/'))
        .filter(x => !_.isEmpty(x))
        .join('/');
}