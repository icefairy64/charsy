import _ from 'lodash';

export function getQueryString(params: Record<string, any | undefined>): string {
    const items = Object.entries(params)
        .filter(p => p[1] != null)
        .map(p => {
            const [key, value] = p;
            if (_.isObject(value) || _.isArray(value)) {
                throw new Error(`Unsupported value type ${typeof value}`);
            }
            return `${encodeURIComponent(key)}=${encodeURIComponent(value.toString())}`;
        });
    if (items.length > 0) {
        return '?' + items.join('&');
    } else {
        return '';
    }
}