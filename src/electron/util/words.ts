import _ from 'lodash';
import fs from 'fs';
import { createLogger } from '@/logging';
import { Logger } from '@/electron/log/logger';

export class WordUtil {
    private wordsDb?: Record<string, string[]>;
    private readonly logger: Logger;

    constructor() {
        this.logger = createLogger('WordUtil');
        this.loadWordsDatabase().catch(e => this.logger.error('Failed to load words DB'));
    }

    capitalizeRedGifsSlug(slug: string) {
        if (!this.wordsDb) {
            return null;
        }
        const matchWord = (words: string[], line: string): string[] | undefined => {
            this.logger.debug(`Matching line ${line}`)
            const options = this.wordsDb?.[line.substring(0, 3)] || [],
                matchingOptions = options.filter(x => line.startsWith(x)).sort((a, b) => b.length - a.length);
            this.logger.debug(`Total options: ${options.length}, matching: ${JSON.stringify(matchingOptions)}`);
            for (const option of matchingOptions) {
                const rest = line.substring(option.length),
                    newWords = [...words, option];
                if (rest === '') {
                    return newWords;
                } else {
                    const result = matchWord(newWords, rest);
                    if (result) {
                        return result;
                    }
                }
            }
            if (words.length === 2) {
                this.logger.warn(`Failed to match third word "${line}" for slug ${slug}, autocapitalizing`);
                // RedGifs slugs contain three words, so we can somewhat safely return here
                return [...words, line];
            }
        }
        return matchWord([], slug.toLocaleLowerCase())?.map(x => _.capitalize(x))?.join('');
    }

    async loadWordsDatabase() {
        try {
            const lines = (await fs.promises.readFile('words.txt', 'utf8')).split('\n'),
                db: Record<string, string[]> = {};
            let wordCount = 0;
            for (const word of lines) {
                const bucketLen = 3;
                const bucket = word.substring(0, bucketLen);
                if (bucket.length === bucketLen) {
                    if (!db[bucket]) {
                        db[bucket] = [];
                    }
                    wordCount++;
                    db[bucket].push(word.toLocaleLowerCase());
                }
            }
            this.logger.info(`Loaded ${wordCount} words`);
            this.wordsDb = db;
            return db;
        }
        catch (e) {
            this.logger.info(`Failed to load word DB`, e);
            return null;
        }
    }
}

let instance: WordUtil;

export function getWordUtil() {
    if (!instance) {
        instance = new WordUtil();
    }
    return instance;
}