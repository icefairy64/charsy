export class DelayedTask {
    timeoutHandle?: number
    fn: Function
    lastDelay?: number
    delayed: boolean = false

    constructor(fn: Function) {
        this.fn = fn;
    }

    delay(timeout?: number, args?: Array<any>) {
        if (this.timeoutHandle) {
            clearTimeout(this.timeoutHandle);
        }
        if (timeout === undefined) {
            timeout = this.lastDelay;
        }
        this.timeoutHandle = setTimeout((args: any[]) => {
            this.delayed = false;
            if (args) {
                this.fn(...args);
            } else {
                this.fn();
            }
        }, timeout, args);
        this.lastDelay = timeout;
        this.delayed = true;
    }

    cancel() {
        this.delayed = false;
        if (this.timeoutHandle) {
            clearTimeout(this.timeoutHandle);
        }
    }
}