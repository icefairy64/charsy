import { Post, PostRating, Tag } from '@/electron/service/service';
import { getFilenameMediaType, parseMediaType } from '@/electron/util/media-type-util';
import { SCHEMA_CACHE_PASSTHROUGH } from '@/electron/api/storage-symbols';
import _ from 'lodash';
import { isElectron } from '@/util/remote-util';

export function getPostMainAttachmentInfo(post: Post) {
    const fileMediaType = post.mediaType ? parseMediaType(post.mediaType) : getFilenameMediaType(post.fileUrl);
    if (fileMediaType != null && fileMediaType.type === 'application' && fileMediaType.subtype === 'zip') {
        const fallbackFileUrl = post.previewUrl || post.thumbUrl || null;
        return fallbackFileUrl == null ? null : {
            fileUrl: fallbackFileUrl,
            mediaType: getFilenameMediaType(fallbackFileUrl)
        };
    }
    return { fileUrl: post.fileUrl, mediaType: fileMediaType };
}

export function getCachePassthroughUrl(url: string) {
    if (isElectron()) {
        return SCHEMA_CACHE_PASSTHROUGH + url.substring(4);
    }
    return url;
}

export function getNormalizedTagName(tag: Tag) {
    return `${getTagCategory(tag)}:${getShortTagName(tag)}`;
}

export function getShortTagName(tag: Tag) {
    if (_.isString(tag)) {
        return tag;
    }
    return tag.name;
}

export function getSearchTagName(tag: Tag) {
    if (_.isString(tag)) {
        return tag;
    }
    return tag.searchName || tag.name;
}

export function getTagCategory(tag: Tag) {
    if (_.isString(tag)) {
        return 'general';
    }
    return tag.category;
}

const tagCategoryPriority = ['artist', 'copyright', 'character', 'general', 'meta', 'relationship', 'extreme_content'];

export function sortTagsByCategoryPriority(tags: Tag[]): Tag[] {
    return tags.sort((a, b) => tagCategoryPriority.indexOf(getTagCategory(a)) - tagCategoryPriority.indexOf(getTagCategory(b)));
}

const ratings: PostRating[] = ['s', 'q', 'e'];

export function filterByRating(post: Post, targetRating?: PostRating) {
    if (!targetRating) {
        return true;
    }
    if (!post.rating) {
        return false;
    }
    return ratings.indexOf(post.rating) <= ratings.indexOf(targetRating)
}