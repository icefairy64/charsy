export class Deferred<T> {
    promise: Promise<T>;
    resolve!: (value?: (PromiseLike<T> | T)) => void;
    reject!: (reason?: any) => void;

    constructor() {
        this.promise = new Promise<T>((resolve, reject) => {
            // @ts-ignore
            this.resolve = resolve;
            this.reject = reject;
        })
    }
}