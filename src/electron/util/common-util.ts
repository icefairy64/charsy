import { EventEmitter } from 'events';

export function formatFileSize(size: number) {
    const suffices = ['B', 'KiB', 'MiB', 'GiB', 'TiB'];
    let suffixIndex = 0;
    while (size >= 1024) {
        size = size / 1024;
        suffixIndex++;
    }
    const scaledSize = Math.floor(size * 100);
    return `${scaledSize / 100} ${suffices[suffixIndex]}`;
}

export function prettifyEnumValue(value: string) {
    return value.split('_')
        .map(word => word.toLocaleLowerCase())
        .map(word => word[0].toLocaleUpperCase() + word.slice(1))
        .join(' ');
}

export function formatTime(seconds: number) {
    const suffices = ['s', 'min', 'hr', 'days'],
        factors = [60, 60, 24];
    let suffixIndex = 0,
        lastRemainder = 0;
    do {
        lastRemainder = Math.floor(seconds % factors[suffixIndex]);
        seconds = Math.floor(seconds / factors[suffixIndex]);
        suffixIndex++;
    } while (factors[suffixIndex - 1] && seconds >= factors[suffixIndex - 1]);
    return `${seconds} ${suffices[suffixIndex]}`;
}

export function sleep(timeout: number): Promise<void> {
    return new Promise(resolve => setTimeout(resolve, timeout));
}

export function eventPromise(emitter: EventEmitter, event: string) {
    return new Promise(resolve => {
        emitter.on(event, resolve);
    });
}