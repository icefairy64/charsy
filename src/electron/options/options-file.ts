import { Logger } from '@/electron/log/logger';
import { createLogger } from '@/logging';
import fs from 'fs';
import { fromEvent } from 'rxjs';
import { distinctUntilChanged, filter, skip, throttleTime } from 'rxjs/operators';
import _ from 'lodash';
import { OptionsObject } from '@/electron/options/options';

export class OptionsFileHandle<T extends { [K in keyof T]: T[K] }> {
    path: string;
    options: OptionsObject<T>;

    private applyingFileChanges: boolean = false;
    private savingOptions: boolean = false;
    private logger: Logger;

    constructor(path: string, defaultValue?: T) {
        this.path = path;
        this.options = new OptionsObject<T>();
        this.logger = createLogger(`OptionsFileHandle ${path}`);
        this.init(defaultValue).catch(e => this.logger.error('Failed to initialize', e));
    }

    private async init(defaultValue?: T) {
        if (defaultValue) {
            try {
                await fs.promises.access(this.path);
            } catch (e) {
                await fs.promises.writeFile(this.path, JSON.stringify(defaultValue, undefined, 4), 'utf8');
            }
        }

        const watcher = fs.watch(this.path);
        fromEvent(watcher, 'change')
            .pipe(
                filter(value => !this.savingOptions),
                throttleTime(1000, undefined, {
                    trailing: true
                })
            )
            // @ts-ignore
            .subscribe(this.onFileChange.bind(this));
        this.options.getValueObservable()
            .pipe(
                skip(1),
                filter(value => !this.applyingFileChanges),
                throttleTime(1000, undefined, {
                    trailing: true
                }),
                distinctUntilChanged(_.isEqual)
            )
            .subscribe(this.onValueChange.bind(this));
    }

    async loadFromFile() {
        if (this.applyingFileChanges) {
            return;
        }
        this.applyingFileChanges = true;
        try {
            this.logger.info(`Loading from file`);
            const json = await fs.promises.readFile(this.path, 'utf8'),
                value = JSON.parse(json);
            this.options.setValue(value);
            this.logger.info(`Load complete`);
        } finally {
            this.applyingFileChanges = false;
        }
    }

    async saveToFile(value?: T) {
        if (this.savingOptions) {
            return;
        }
        this.savingOptions = true;
        try {
            this.logger.info(`Saving to file`);
            const json = JSON.stringify(value, null, 4);
            await fs.promises.writeFile(this.path, json, 'utf8');
            this.logger.info(`Save complete`);
        } finally {
            this.savingOptions = false;
        }
    }

    onFileChange([eventType]: string[]) {
        if (!this.savingOptions && eventType === 'change') {
            this.logger.info(`File updated`);
            return this.loadFromFile();
        }
    }

    onValueChange(value?: T) {
        if (!this.applyingFileChanges) {
            this.logger.info(`Value changed`);
            return this.saveToFile(value);
        }
    }
}