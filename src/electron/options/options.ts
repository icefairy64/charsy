import { merge, Observable, of, Subject } from 'rxjs';
import { distinctUntilChanged, map, mergeMap, scan, startWith } from 'rxjs/operators';
import _ from 'lodash';

type OptionsValue<T> = T extends string ? string :
    T extends number ? number :
    OptionsTree<T>;

interface OptionsTree<T> {
    [key: string]: OptionsValue<keyof T>
}

export class Options<T> {
    currentValue?: T;
    valueSubject: Subject<T>;

    constructor(value?: T) {
        this.currentValue = value;
        this.valueSubject = new Subject<T>();
    }

    setValue(value?: T) {
        this.currentValue = value;
        this.valueSubject.next(value);
    }

    getValueObservable() {
        return this.valueSubject.pipe(
            startWith(this.currentValue)
        );
    }
}

export class OptionsObject<T extends { [K in keyof T]: T[K] }> extends Options<T> {
    children: {
        [K in keyof T]?: NonNullable<T[K]> extends object ? OptionsObject<NonNullable<T[K]>> : Options<T[K]>
    };

    childrenSubject: Subject<{
        [K in keyof T]?: NonNullable<T[K]> extends object ? OptionsObject<NonNullable<T[K]>> : Options<T[K]>
    } | null>;

    deepValueSubject: Subject<T>;

    constructor(value?: T) {
        super(value);
        this.children = {};
        this.childrenSubject = new Subject<{[K in keyof T]?: T[K] extends object ? OptionsObject<T[K]> : Options<T[K]>} | null>();
        const deepValueObservable = this.childrenSubject.pipe(
            mergeMap(children => {
                if (children) {
                    // @ts-ignore
                    return merge(...Object.entries(children).map(<K extends keyof T>([key, child]: [K, Options<T[K]>]) => {
                        return child.getValueObservable().pipe(
                            map(value => ({ key, value }))
                        );
                    }));
                } else {
                    return of(null);
                }
            }),
            // @ts-ignore
            scan(<K extends keyof T>(a, x: { key: K, value: T[K] } | null ) => {
                if (this.currentValue == null) {
                    return null;
                }
                if (x) {
                    if (!a) {
                        a = {};
                    } else {
                        a = _.cloneDeep(a);
                    }
                    a[x.key] = x.value;
                    if (x.value === undefined) {
                        delete a[x.key];
                    }
                    return a;
                } else {
                    return null;
                }
            }, value)
        );
        this.deepValueSubject = new Subject<T>();
        deepValueObservable.subscribe(this.deepValueSubject);
        this.deepValueSubject.subscribe(value => this.currentValue = value);
        if (value) {
            (Object.getOwnPropertyNames(value) as (keyof T)[])
                .forEach(key => this.createChild(key, value[key]));
        }
    }

    setValue(value?: T) {
        const prevValue = this.currentValue;
        this.currentValue = value
        if (value) {
            const newKeys = Object.getOwnPropertyNames(value);
            const currentKeys = (prevValue && Object.getOwnPropertyNames(prevValue) || [])
                .filter(key => !newKeys.includes(key));
            ([...newKeys, ...currentKeys] as (keyof T)[])
                .forEach(key => this.setChildValue(key, value[key]));
        } else {
            this.childrenSubject.next(this.children);
        }
    }

    setChildValue<K extends keyof T>(key: K, value: T[K]) {
        if (this.children[key] == null) {
            this.createChild(key, value);
        }
        this.children[key]?.setValue(value);
    }

    adoptChild<K extends keyof T>(key: K, option: NonNullable<T[K]> extends object ? OptionsObject<NonNullable<T[K]>> : Options<T[K]>) {
        if (this.children[key] != null) {
            throw new Error('Child is already present');
        }
        this.children[key] = option;
        this.childrenSubject.next(this.children);
    }

    ensureChild<K extends keyof T>(key: K, defaultValue: NonNullable<T[K]>): NonNullable<T[K]> extends object ? OptionsObject<NonNullable<T[K]>> : Options<NonNullable<T[K]>> {
        if (!this.children[key]) {
            this.setChildValue(key, defaultValue);
        }
        const child = this.children[key];
        if (!child) {
            throw new Error('Child was not created');
        }
        // @ts-ignore
        return child;
    }

    private createChild<K extends keyof T>(key: K, value: T[K]) {
        let child;
        if (typeof value === 'object' && !_.isArray(value)) {
            child = new OptionsObject(value);
        } else {
            child = new Options(value);
        }
        // @ts-ignore
        this.children[key] = child;
        this.childrenSubject.next(this.children);
        return child;
    }

    getChild<K extends keyof T>(key: K) {
        return this.children[key];
    }

    getValueObservable(): Observable<T> {
        return this.deepValueSubject.pipe(
            startWith(this.currentValue),
            distinctUntilChanged(_.isEqual)
        );
    }
}

