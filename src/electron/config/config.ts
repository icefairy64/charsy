export type ConfigDescription<T> = {
    [k in keyof T]: ConfigPropertyDescription<T[k]>
}

export type ConfigPropertyType<T> = T extends string ? 'string'
    : T extends number ? 'number'
    : T extends boolean ? 'boolean'
    : T extends object ? 'object'
    : 'unknown';

export interface ConfigComboboxEditor<T> {
    type: 'combobox',
    options: { label: string, value: T }[]
}

export type ConfigEditor<T> = ConfigComboboxEditor<T>;

export type ConfigPropertyDescription<T> = {
    type: ConfigPropertyType<T>
    label: string
    placeholder?: T
    description?: string
    editor?: ConfigEditor<T>
} & (T extends object ? {
    children: ConfigDescription<T>
} : {})