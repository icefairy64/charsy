import { LogLevel } from '@/electron/log/logger';

const methodMap = {
    ERROR: 'error',
    WARN: 'warn',
    INFO: 'log',
    DEBUG: 'debug',
    TRACE: 'debug'
}

export function consoleReporter(level: LogLevel, timestamp: number, message: string) {
    // @ts-ignore
    console[methodMap[level]](message);
}