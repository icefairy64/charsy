export type LogLevel = 'ERROR' | 'WARN' | 'INFO' | 'DEBUG' | 'TRACE';

export type LogReporter = (level: LogLevel, timestamp: number, message: string) => void;

const logLevelSorted: LogLevel[] = ['TRACE', 'DEBUG', 'INFO', 'WARN', 'ERROR'];

export class Logger {
    private readonly reporters: LogReporter[] = [];
    private readonly component?: string;
    private readonly levelFilterFn!: (level: LogLevel) => boolean;

    constructor (componentName?: string, threshold?: LogLevel, parent?: Logger) {
        this.component = componentName;
        if (parent) {
            this.reporters = parent.reporters;
            this.levelFilterFn = parent.levelFilterFn;
        }
        if (threshold || !this.levelFilterFn) {
            // @ts-ignore
            const levelMap: Record<LogLevel, boolean> = {};
            logLevelSorted.forEach((level, index) => {
                levelMap[level] = index >= logLevelSorted.indexOf(threshold || 'INFO');
            });
            this.levelFilterFn = level => levelMap[level];
        }
    }

    createChild(componentName?: string, threshold?: LogLevel) {
        return new Logger(componentName, threshold, this);
    }

    addReporter(reporter: LogReporter) {
        this.reporters.push(reporter);
    }

    log(level: LogLevel, message: string, error?: Error, timestamp?: number) {
        if (!this.levelFilterFn(level)) {
            return;
        }
        if (!timestamp) {
            timestamp = Date.now();
        }
        if (this.component) {
            message = `[${this.component}] ${message}`;
        }
        if (error) {
            message += `: ${error.message}\n${error.stack}`;
        }
        for (const reporter of this.reporters) {
            reporter(level, timestamp, message);
        }
    }

    trace(message: string, error?: Error) {
        this.log('TRACE', message, error);
    }

    debug(message: string, error?: Error) {
        this.log('DEBUG', message, error);
    }

    info(message: string, error?: Error) {
        this.log('INFO', message, error);
    }

    warn(message: string, error?: Error) {
        this.log('WARN', message, error);
    }

    error(message: string, error?: Error) {
        this.log('ERROR', message, error);
    }
}