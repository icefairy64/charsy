import fs, { WriteStream } from 'fs';
import { LogLevel } from '@/electron/log/logger';
import path from 'path';

export class FileLogReporter {
    private readonly stream: WriteStream;

    constructor() {
        if (!fs.existsSync('log')) {
            fs.mkdirSync('log');
        }
        this.stream = fs.createWriteStream(path.join('log', `log-${new Date().toISOString().replaceAll(/[:.]/g, '')}.txt`), { encoding: 'utf-8' });
    }

    log(level: LogLevel, timestamp: number, message: string) {
       this.stream.write(`${level} ${new Date(timestamp).toString()} - ${message}\n`);
    }

    free() {
        this.stream.end((e: any) => {
            if (!e) {
                this.stream.close();
            }
        });
    }
}