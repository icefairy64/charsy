import { Deferred } from '../util/deferred';
import { Logger } from '@/electron/log/logger';
import { createLogger } from '@/logging';

export interface HttpStreamingResponse {
    stream: ReadableStream,
    length: number,
    range?: string
}

const siteHeaderHandlers: Record<string, (url: string, headers: any) => void> = {};

export function registerSiteHeaderHandler(siteId: string, handler: (url: string, headers: any) => void) {
    siteHeaderHandlers[siteId] = handler;
}

function addSiteHeaders(url: string, headers: any) {
    for (const siteHandler of Object.values(siteHeaderHandlers)) {
        siteHandler(url, headers);
    }
    return headers;
}

export abstract class HttpClient<Req = unknown, Head = unknown> {
    protected logger: Logger

    defaultHeaders?: object
    cooldown?: number

    cooldownPromise?: Promise<void>

    constructor(defaultHeaders?: object, cooldown?: number) {
        if (defaultHeaders === undefined) {
            defaultHeaders = {
                'User-Agent': 'Charsy/0.1'
            };
        }
        this.defaultHeaders = defaultHeaders;
        this.cooldown = cooldown;
        this.logger = createLogger('HttpClient');
    }

    updateDefaultHeaders(headers: object) {
        if (this.defaultHeaders != null) {
            headers = {
                ...this.defaultHeaders,
                ...headers
            };
        }
        this.defaultHeaders = headers;
    }

    scheduleCooldown() {
        if (this.cooldown != null) {
            const deferred = new Deferred<void>();
            global.setTimeout(() => deferred.resolve(), this.cooldown);
            this.cooldownPromise = deferred.promise;
        }
    }

    async get(url: string, headers?: object): Promise<string> {
        return this.request(url, 'GET', headers);
    }

    async getJson<T>(url: string, headers?: object): Promise<T> {
        const rawData = await this.request(url, 'GET', headers);
        try {
            return JSON.parse(rawData);
        } catch (e) {
            this.logger.error(`Invalid JSON data: ${rawData}`, e);
            throw new Error('Invalid JSON data');
        }
    }

    async post(url: string, data: string, headers?: object): Promise<string> {
        return this.request(url, 'POST', headers);
    }

    abstract request(url: string, method: 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE', headers?: object, requestSetupFn?: (req: Req) => void): Promise<string>;

    sendJson(url: string, method: 'POST' | 'PUT' | 'PATCH' | 'DELETE', data: any): Promise<any> {
        return this.sendData(url, method, JSON.stringify(data), {
            'Content-Type': 'application/json'
        });
    }

    abstract sendData(url: string, method: 'POST' | 'PUT' | 'PATCH' | 'DELETE', data: string, headers: any): Promise<any>;

    protected getEffectiveHeaders(headers: object | undefined, url: string): any {
        let effectiveHeaders = {};
        if (this.defaultHeaders != null) {
            effectiveHeaders = {
                ...effectiveHeaders,
                ...this.defaultHeaders
            }
        }
        if (headers != null) {
            effectiveHeaders = {
                ...effectiveHeaders,
                ...headers
            };
        }
        addSiteHeaders(url, effectiveHeaders);
        return effectiveHeaders;
    }

    abstract stream(url: string, headers?: object, method?: 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE', requestSetupFn?: (req: Req) => void, requestContext?: any): Promise<HttpStreamingResponse>;

    abstract headers(url: string, headers?: object): Promise<Head>;
}

class FetchHttpClient extends HttpClient<Request, Headers> {
    headers(url: string, headers?: object): Promise<Headers> {
        throw new Error('Not supported');
    }

    async request(url: string, method: "GET" | "POST" | "PUT" | "PATCH" | "DELETE", headers?: object, requestSetupFn?: (req: Request) => void): Promise<string> {
        headers = this.getEffectiveHeaders(headers, url);
        const response = await fetch(url,{ headers: headers as Record<string, string> ?? {} });
        return response.text();
    }

    sendData(url: string, method: "POST" | "PUT" | "PATCH" | "DELETE", data: string, headers: any): Promise<any> {
        throw new Error('Not supported');
    }

    async stream(url: string, headers?: object, method?: "GET" | "POST" | "PUT" | "PATCH" | "DELETE", requestSetupFn?: (req: Request) => void, requestContext?: any): Promise<HttpStreamingResponse> {
        const req = await fetch({
            url,
            // @ts-ignore
            headers,
            method: method || 'GET'
        });

        if (!req.body) {
            throw new Error('No body in response');
        }

        const length = req.headers.get('Content-Length');

        return {
            stream: req.body,
            length: length && +length || 0,
            range: req.headers.get('Range') || undefined
        };
    }
}

let impl: { new(defaultHeaders?: object, cooldown?: number): HttpClient } | undefined;

impl = FetchHttpClient;

if (PX_PLATFORM_ELECTRON_MAIN) {
    import('@/electron/http/node.js').then(m => {
        impl = m.NodeHttpClient
    });
}

export function getHttpClient(defaultHeaders?: object, cooldown?: number) {
    defaultHeaders = {
        'User-Agent': `Charsy/${PX_VERSION.version}`,
        ...defaultHeaders ?? {}
    }
    if (!impl) {
        throw new Error('No HTTP implementation');
    }
    return new impl(defaultHeaders, cooldown || 500);
}