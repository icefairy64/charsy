import { NodeHttpClient } from '@/electron/http/node';

export default class HttpStreamHandler {
    private readonly http: NodeHttpClient;
    private readonly url: string;

    constructor(client: NodeHttpClient, url: string) {
        this.http = client;
        this.url = url;
    }

    async getChunk(range?: string) {
        const response = await this.http.streamNode(this.url, {
            range: range
        });
        return {
            stream: response,
            length: Number(response.headers['content-length']),
            range: response.headers.range || response.headers.range
        };
    }
}