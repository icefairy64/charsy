import { ClientRequest, IncomingHttpHeaders, IncomingMessage, RequestOptions } from 'http';
import { Deferred } from '@/electron/util/deferred';
import { HttpClient, HttpStreamingResponse } from '@/electron/http/client';
import { convertNodeReadableToWeb } from '@/electron/stream/node-to-web';

export class NodeHttpClient extends HttpClient<ClientRequest, IncomingHttpHeaders> {
    async request(url: string, method: 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE', headers?: object, requestSetupFn?: (req: ClientRequest) => void, retries?: number): Promise<string> {
        await this.cooldownPromise;
        const deferred = new Deferred<string>();
        let body = '';
        this.logger.trace(`Opening stream to ${url}`);
        const stream = await this.streamNode(url, headers, method, requestSetupFn);
        stream.on('data', chunk => {
            body += chunk;
        });
        stream.on('error', (err) => {
            this.scheduleCooldown();
            deferred.reject(err);
        });
        stream.on('end', () => {
            this.scheduleCooldown();
            this.logger.trace(`Finished ${method} to ${url}`);
            if (stream.statusCode && (stream.statusCode < 200 || stream.statusCode >= 400)) {
                if (stream.statusCode === 429) {
                    retries = retries || 0;
                    this.logger.warn(`Request to ${url} was rate-limited; retrying with increased cooldown`);
                    if (retries < 3) {
                        setTimeout(() => deferred.resolve(this.request(url, method, headers, requestSetupFn, retries! + 1)),
                            (this.cooldown || 500) * (retries + 2));
                        return;
                    }
                }
                deferred.reject({
                    url: url,
                    status: stream.statusCode,
                    response: body,
                    toString() {
                        return JSON.stringify(this)
                    }
                });
            } else {
                deferred.resolve(body);
            }
        });
        deferred.promise.catch(e => {
            this.logger.error(`Failed to ${method} ${url}: ${e}`);
        });
        return deferred.promise;
    }

    async sendData(url: string, method: 'POST' | 'PUT' | 'PATCH' | 'DELETE', data: string, headers: any) {
        return this.request(url, method, {
            ...headers,
            'Content-Length': Buffer.byteLength(data)
        }, req => {
            req.write(data);
        });
    }

    private static async getSchemaHandler(url: string) {
        const parsedUrl = new URL(url.toLocaleLowerCase());
        switch (parsedUrl.protocol) {
            case 'http:':
                return import('http');
            case 'https:':
                return import('https');
            default:
                throw new Error(`Unsupported protocol ${parsedUrl.protocol}`);
        }
    }

    async streamNode(url: string, headers?: object, method: 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE' = 'GET', requestSetupFn?: (req: ClientRequest) => void, requestContext?: any): Promise<IncomingMessage> {
        const deferred = new Deferred<IncomingMessage>(),
            headersDeferred = new Deferred<IncomingHttpHeaders>();
        if (requestContext) {
            requestContext.headers = headersDeferred.promise;
        }
        await this.cooldownPromise;
        const options: RequestOptions = {};
        options.headers = this.getEffectiveHeaders(headers, url);
        options.method = method;
        const req = (await NodeHttpClient.getSchemaHandler(url)).request(url, options, res => {
            this.scheduleCooldown();
            headersDeferred.resolve(res.headers);
            deferred.resolve(res);
        });
        req.on('error', error => {
            this.scheduleCooldown();
            deferred.reject(error);
        });
        requestSetupFn?.(req);
        req.end();
        return deferred.promise;
    }

    async stream(url: string, headers?: object, method?: "GET" | "POST" | "PUT" | "PATCH" | "DELETE", requestSetupFn?: (req: ClientRequest) => void, requestContext?: any): Promise<HttpStreamingResponse> {
        const nodeStream = await this.streamNode(url, headers, method, requestSetupFn, requestContext);
        const length = nodeStream.headers['content-length'];
        return {
            stream: convertNodeReadableToWeb(nodeStream),
            length: nodeStream.readableLength,
            range: nodeStream.headers.range
        }
    }

    async headers(url: string, headers?: object): Promise<IncomingHttpHeaders> {
        await this.cooldownPromise;
        const deferred = new Deferred<IncomingHttpHeaders>();
        const options: RequestOptions = {
            method: 'HEAD'
        };
        options.headers = this.getEffectiveHeaders(headers, url);
        const req = (await NodeHttpClient.getSchemaHandler(url)).get(url, options, res => {
            this.scheduleCooldown();
            deferred.resolve(res.headers);
        });
        req.on('error', error => {
            this.scheduleCooldown();
            deferred.reject(error);
        });
        return deferred.promise;
    }
}