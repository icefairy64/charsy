import { AbstractStorage } from '@/electron/api/storage-abstract';
import { Database } from 'sqlite3';
import { Logger } from '@/electron/log/logger';
import { createLogger } from '@/logging';
import { getMigrationScripts } from '@/electron/migrations/migrations';
import { Deferred } from '@/electron/util/deferred';
import { Post } from '@/electron/service/service';
import { FileCacheEntry, PostCollection } from '@/electron/api/storage-symbols';
import { Bookmark, BookmarkPost } from '@/electron/api/bookmark-symbols';
import { DownloadInfo } from '@/electron/api/download-symbols';
import { DownloadStatus } from '@/electron/api/download';
import _ from 'lodash';

export const DB_TABLE_FILECACHE = 'filecache';
export const DB_TABLE_COLLECTIONS = 'collections';
export const DB_TABLE_COLLECTION_POSTS = 'collection_posts';
export const DB_TABLE_POSTS = 'posts';
export const DB_TABLE_BOOKMARKS = 'bookmarks';
export const DB_TABLE_BOOKMARK_POSTS = 'bookmark_posts';
export const DB_TABLE_DOWNLOADS = 'downloads';
export const DB_TABLE_POSTS_FTS = 'posts_fts';

const SQL_QUERY_FILECACHE_LIST = `SELECT * FROM ${DB_TABLE_FILECACHE} ORDER BY created_at ASC NULLS FIRST;`;
const SQL_QUERY_FILECACHE_FOR_URL = `SELECT local_path FROM ${DB_TABLE_FILECACHE} WHERE url = ?;`;
const SQL_CREATE_FILECACHE_FOR_URL = `INSERT INTO ${DB_TABLE_FILECACHE} (url, local_path, size, created_at) VALUES (?, ?, ?, ?);`;
const SQL_DELETE_FILECACHE_FOR_URL = `DELETE FROM ${DB_TABLE_FILECACHE} WHERE url = ?;`;

const SQL_QUERY_COLLECTION_LIST = `SELECT * FROM ${DB_TABLE_COLLECTIONS};`;
const SQL_QUERY_COLLECTION_BY_NAME = `SELECT * FROM ${DB_TABLE_COLLECTIONS} WHERE name = ?;`;
const SQL_CREATE_COLLECTION = `INSERT INTO ${DB_TABLE_COLLECTIONS} (name, description, download, sync_descriptor, sync_enabled) VALUES (?, ?, ?, ?, ?);`;
const SQL_REMOVE_COLLECTION = `DELETE FROM ${DB_TABLE_COLLECTIONS} WHERE name = ?;`;
const SQL_MODIFY_COLLECTION = `UPDATE ${DB_TABLE_COLLECTIONS} SET description = ?, download = ?, sync_descriptor = ?, sync_enabled = ? WHERE name = ?;`;
const SQL_QUERY_COLLECTION_POSTS_WINDOWED = `SELECT * FROM ${DB_TABLE_COLLECTION_POSTS} JOIN ${DB_TABLE_POSTS} ON ${DB_TABLE_POSTS}.url = post_url WHERE collection_name = ? ORDER BY created_at DESC LIMIT ? OFFSET ?;`;

const SQL_QUERY_COLLECTION_POSTS_TAGGED_WINDOWED = `SELECT *, json_extract(data, '$.tags') AS tags, json_extract(data, '$.fileUrl') AS file_url 
    FROM ${DB_TABLE_COLLECTION_POSTS} cp 
        JOIN ${DB_TABLE_POSTS} p ON p.url = cp.post_url 
        LEFT OUTER JOIN ${DB_TABLE_POSTS_FTS}('$fts_expr') pf ON p.url = pf.post_url 
            WHERE collection_name = ? AND ($tags_expr OR pf.post_url IS NOT NULL) $extra_expr ORDER BY created_at DESC LIMIT ? OFFSET ?;`;

const SQL_QUERY_COLLECTION_POSTS_TAGGED_WINDOWED_NO_FTS = `SELECT *, json_extract(data, '$.tags') AS tags, json_extract(data, '$.fileUrl') AS file_url  
    FROM ${DB_TABLE_COLLECTION_POSTS} cp 
        JOIN ${DB_TABLE_POSTS} p ON p.url = cp.post_url
            WHERE collection_name = ? AND ($tags_expr) $extra_expr ORDER BY created_at DESC LIMIT ? OFFSET ?;`;

const SQL_ADD_COLLECTION_TO_POST = `INSERT INTO ${DB_TABLE_COLLECTION_POSTS} (collection_name, post_url) values (?, ?);`;
const SQL_REMOVE_COLLECTION_TO_POST = `DELETE FROM ${DB_TABLE_COLLECTION_POSTS} WHERE collection_name = ? AND post_url = ?;`;
const SQL_CREATE_POST = `INSERT INTO ${DB_TABLE_POSTS} (url, data) VALUES (?, ?);`;
const SQL_UPSERT_POST = `INSERT INTO ${DB_TABLE_POSTS} (url, data) VALUES (?, ?) ON CONFLICT (url) DO UPDATE SET data = excluded.data;`;
const SQL_CREATE_OR_IGNORE_POST = `INSERT INTO ${DB_TABLE_POSTS} (url, data) VALUES (?, ?) ON CONFLICT (url) DO NOTHING;`;
const SQL_QUERY_POST_COLLECTION_LIST = `SELECT collection_name FROM ${DB_TABLE_COLLECTION_POSTS} WHERE post_url = ?;`;

const SQL_QUERY_BOOKMARK_LIST = `SELECT * FROM ${DB_TABLE_BOOKMARKS};`;
const SQL_CREATE_BOOKMARK = `INSERT INTO ${DB_TABLE_BOOKMARKS} (name, path, monitor, download) VALUES (?, ?, ?, ?);`;
const SQL_REMOVE_BOOKMARK = `DELETE FROM ${DB_TABLE_BOOKMARKS} WHERE name = ?;`;
const SQL_MODIFY_BOOKMARK = `UPDATE ${DB_TABLE_BOOKMARKS} SET path = ?, monitor = ?, download = ? WHERE name = ?;`;
const SQL_QUERY_BOOKMARK_POSTS_WINDOWED = `SELECT * FROM ${DB_TABLE_BOOKMARK_POSTS} JOIN ${DB_TABLE_POSTS} ON ${DB_TABLE_POSTS}.url = post_url WHERE bookmark_name = ? ORDER BY created_at DESC LIMIT ? OFFSET ?;`;
const SQL_QUERY_BOOKMARK_UNSEEN_POST_COUNT = `SELECT count(*) FROM ${DB_TABLE_BOOKMARK_POSTS} WHERE bookmark_name = ? AND seen = false;`;
const SQL_ADD_OR_IGNORE_BOOKMARK_TO_POST = `INSERT INTO ${DB_TABLE_BOOKMARK_POSTS} (bookmark_name, post_url, seen) VALUES (?, ?, ?) ON CONFLICT (bookmark_name, post_url) DO NOTHING;`;
const SQL_MARK_ALL_BOOKMARK_TO_POST_AS_SEEN = `UPDATE ${DB_TABLE_BOOKMARK_POSTS} SET seen = true WHERE bookmark_name = ?;`;
const SQL_MARK_BOOKMARK_TO_POST_AS_SEEN = `UPDATE ${DB_TABLE_BOOKMARK_POSTS} SET seen = true WHERE bookmark_name = ? AND post_url = ?;`;
const SQL_QUERY_IS_BOOKMARK_POST_SEEN = `SELECT seen FROM ${DB_TABLE_BOOKMARK_POSTS} WHERE bookmark_name = ? AND post_url = ?;`;

const SQL_QUERY_ACTIVE_DOWNLOAD_LIST = `SELECT * FROM ${DB_TABLE_DOWNLOADS} d JOIN ${DB_TABLE_POSTS} ON ${DB_TABLE_POSTS}.url = d.post_url WHERE d.status = 'PENDING' OR d.status = 'IN_PROGRESS';`;
const SQL_SAVE_DOWNLOAD = `INSERT INTO ${DB_TABLE_DOWNLOADS} (post_url, sub_dir, status) VALUES (?, ?, ?) ON CONFLICT (post_url) DO UPDATE SET sub_dir = excluded.sub_dir, status = excluded.status;`;
const SQL_QUERY_DOWNLOADED_POSTS_BY_URL_LIST = `SELECT p.url, d.status FROM ${DB_TABLE_POSTS} p LEFT OUTER JOIN ${DB_TABLE_DOWNLOADS} d ON p.url = d.post_url WHERE p.url IN ($post_url_list);`;

export class SqliteStorageImpl extends AbstractStorage {
    fileStorageSupported = true;
    downloadsSupported = true;

    private db: Database;
    private logger: Logger;
    private inTransaction: boolean = false;

    constructor(dbPath: string) {
        super();
        this.logger = createLogger('SqliteStorage');
        this.db = new Database(dbPath, this.onDbInit.bind(this));
    }

    async onDbInit(err: Error | null) {
        if (!err) {
            try {
                await this.exec(`CREATE TABLE IF NOT EXISTS ${DB_TABLE_FILECACHE} (
                    url TEXT PRIMARY KEY,
                    local_path TEXT NOT NULL
                );`);
                await this.exec(`CREATE TABLE IF NOT EXISTS ${DB_TABLE_COLLECTIONS} (
                    name TEXT PRIMARY KEY,
                    description TEXT,
                    cover_post_url TEXT,
                    created_at INTEGER DEFAULT CURRENT_TIMESTAMP
                );`);
                await this.exec(`CREATE TABLE IF NOT EXISTS ${DB_TABLE_COLLECTION_POSTS} (
                    collection_name TEXT NOT NULL,
                    post_url TEXT NOT NULL,
                    created_at INTEGER DEFAULT CURRENT_TIMESTAMP,
                    CONSTRAINT ${DB_TABLE_COLLECTION_POSTS}_pk PRIMARY KEY (collection_name, post_url)
                );`);
                await this.exec(`CREATE TABLE IF NOT EXISTS ${DB_TABLE_POSTS} (
                    url TEXT PRIMARY KEY,
                    data TEXT NOT NULL,
                    created_at INTEGER DEFAULT CURRENT_TIMESTAMP
                );`);
                await this.exec(`CREATE TABLE IF NOT EXISTS ${DB_TABLE_BOOKMARKS} (
                    name TEXT PRIMARY KEY,
                    path TEXT NOT NULL,
                    monitor BOOLEAN NOT NULL,
                    created_at INTEGER DEFAULT CURRENT_TIMESTAMP
                );`);
                await this.exec(`CREATE TABLE IF NOT EXISTS ${DB_TABLE_BOOKMARK_POSTS} (
                    bookmark_name TEXT NOT NULL,
                    post_url TEXT NOT NULL,
                    seen BOOLEAN NOT NULL,
                    created_at INTEGER DEFAULT CURRENT_TIMESTAMP,
                    CONSTRAINT ${DB_TABLE_BOOKMARK_POSTS}_pk PRIMARY KEY (bookmark_name, post_url)
                );`);
                await this.migrateDbSchema();
                this.initDeferred.resolve();
            }
            catch (e) {
                this.initDeferred.reject(e);
            }
        } else {
            this.initDeferred.reject(err);
        }
    }

    async migrateDbSchema() {
        const currentSchemaVersionRow = await this.queryOne('PRAGMA user_version;'),
            currentSchemaVersion = currentSchemaVersionRow.user_version;
        this.logger.info(`Current DB schema version: ${currentSchemaVersion}`);
        for (const migration of getMigrationScripts(currentSchemaVersion)) {
            this.logger.info(`Migrating DB schema to version ${migration.version}`);
            await this.runInTransaction(async () => {
                for (const statement of migration.sql) {
                    await this.exec(statement);
                }
                await this.exec(`PRAGMA user_version = ${migration.version}`);
            });
        }
    }

    private queryAll(sql: string, ...params: any[]): Promise<Array<any>> {
        const deferred = new Deferred<Array<object>>();
        this.db.all(sql, ...params, (err: Error | null, rows: any[]) => {
            if (err) {
                deferred.reject(err);
            } else {
                deferred.resolve(rows);
            }
        })
        return deferred.promise;
    }

    private queryOne(sql: string, ...params: any[]): Promise<any | undefined> {
        const deferred = new Deferred<any | undefined>();
        this.db.get(sql, ...params, (err: Error | null, rows: any[]) => {
            if (err) {
                deferred.reject(err);
            } else {
                deferred.resolve(rows);
            }
        })
        return deferred.promise;
    }

    private exec(sql: string, ...params: any[]): Promise<void> {
        const deferred = new Deferred<void>();
        this.db.run(sql, ...params, (err: Error | null) => {
            if (err) {
                deferred.reject(err);
            } else {
                deferred.resolve();
            }
        })
        return deferred.promise;
    }

    private async runInTransaction<T>(fn: () => Promise<T>): Promise<T> {
        if (this.inTransaction) {
            return fn();
        }
        try {
            await this.exec('BEGIN TRANSACTION;');
            this.inTransaction = true;
            const result = await fn();
            await this.exec('END TRANSACTION;');
            return result;
        }
        catch (e) {
            await this.exec('ROLLBACK TRANSACTION;');
            throw e;
        }
        finally {
            this.inTransaction = false;
        }
    }

    storePost(post: Post, updateIfPresent: boolean = true): Promise<void> {
        return this.exec(updateIfPresent ? SQL_UPSERT_POST : SQL_CREATE_OR_IGNORE_POST, post.url, JSON.stringify(post));
    }

    addPostToCollection(collectionName: string, post: Post): Promise<void> {
        return this.exec(SQL_ADD_COLLECTION_TO_POST, collectionName, post.url);
    }

    removePostFromCollection(collectionName: string, post: Post): Promise<void> {
        return this.exec(SQL_REMOVE_COLLECTION_TO_POST, collectionName, post.url);
    }

    rowToCollection(row: any): PostCollection {
        return {
            name: row.name,
            description: row.description,
            createdAt: new Date(row.created_at),
            download: !!row.download,
            syncDescriptor: JSON.parse(row.sync_descriptor),
            syncEnabled: !!row.sync_enabled
        };
    }

    async getCollections(): Promise<Array<PostCollection>> {
        const rows = await this.queryAll(SQL_QUERY_COLLECTION_LIST);
        return rows.map(row => this.rowToCollection(row));
    }

    async getCollectionByName(name: string): Promise<PostCollection> {
        const row = await this.queryOne(SQL_QUERY_COLLECTION_BY_NAME, name);
        return this.rowToCollection(row);
    }

    createCollection(collection: PostCollection): Promise<void> {
        const { name, description, download, syncDescriptor, syncEnabled } = collection;
        return this.exec(SQL_CREATE_COLLECTION, name, description, download, JSON.stringify(syncDescriptor), syncEnabled);
    }

    removeCollection(name: string): Promise<void> {
        return this.exec(SQL_REMOVE_COLLECTION, name);
    }

    modifyCollection(collection: PostCollection): Promise<void> {
        const { name, description, download, syncDescriptor, syncEnabled } = collection;
        return this.exec(SQL_MODIFY_COLLECTION, description, download, syncDescriptor, syncEnabled, name);
    }

    async getPostsInCollection(collectionName: string, query: string | null, offset: number, limit: number): Promise<Array<Post>> {
        let rows: any[];
        if (query == null) {
            rows = await this.queryAll(SQL_QUERY_COLLECTION_POSTS_WINDOWED, collectionName, limit, offset);
        } else {
            const terms = query
                .split(' ')
                .map(term => term.trim())
                .filter(term => term && term !== '');

            const systemTerms = _.remove(terms, term => term.startsWith(':'));

            const tagPredicates = terms.map(term => {
                if (term.startsWith('-')) {
                    term = term.substring(1);
                    return `tags NOT LIKE '%"${term}"%'`;
                } else {
                    return `tags LIKE '%"${term}"%'`;
                }
            });

            const systemPredicates = [];

            if (systemTerms.includes(':video')) {
                systemPredicates.push(`(file_url LIKE '%.mp4' OR file_url LIKE '%.webm')`);
            }

            if (systemTerms.includes(':image')) {
                systemPredicates.push(`(file_url NOT LIKE '%.mp4' AND file_url NOT LIKE '%.webm')`);
            }

            const getParametrizedTerm = (termName: string) => {
                const term = systemTerms.find(t => t.startsWith(':' + termName));
                if (term) {
                    const op = term[termName.length + 1],
                        param = term.substring(termName.length + 2);
                    if (!param) {
                        return;
                    }
                    return {
                        op,
                        param
                    }
                }
            }

            const minSizeTerm = getParametrizedTerm('minsize');
            if (minSizeTerm) {
                systemPredicates.push(`(min(cast(json_extract(data, '$.width') as int), cast(json_extract(data, '$.height') as int)) ${minSizeTerm.op} ${minSizeTerm.param})`);
            }

            const ratioTerm = getParametrizedTerm('ratio');
            if (ratioTerm) {
                let ratio: number | string = ratioTerm.param;
                if (Number.isNaN(+ratio)) {
                    switch (ratio[1]) {
                        case 'h': ratioTerm.op = '>'; break;
                        case 'v': ratioTerm.op = '<'; break;
                    }
                    ratio = 1;
                }
                systemPredicates.push(`((cast(json_extract(data, '$.width') as real) / cast(json_extract(data, '$.height') as real)) ${ratioTerm.op} ${ratio})`);
            }

            const ratingTerm = getParametrizedTerm('rating')
            if (ratingTerm) {
                systemPredicates.push(`(json_extract(data, '$.rating') ${ratingTerm.op} '${ratingTerm.param}')`);
            }

            const ftsExpr = terms
                .filter(x => x.length > 0 && !x.startsWith(':') && !x.startsWith('-'))
                .join(' AND ');

            const tagsExpr = tagPredicates.length > 0
                ? tagPredicates.join(' AND ')
                : ftsExpr.length > 0 ? 'FALSE' : 'TRUE';

            const extraExpr = systemPredicates.length > 0
                ? `AND ${systemPredicates.join(' AND ')}`
                : '';

            const sql = ftsExpr.length > 0
                ? SQL_QUERY_COLLECTION_POSTS_TAGGED_WINDOWED
                : SQL_QUERY_COLLECTION_POSTS_TAGGED_WINDOWED_NO_FTS;

            rows = await this.queryAll(sql
                    .replace('$tags_expr', tagsExpr)
                    .replace('$fts_expr', ftsExpr)
                    .replace('$extra_expr', extraExpr),
                collectionName, limit, offset);
        }
        return rows.map(row => JSON.parse(row.data));
    }

    async getPostCollections(postUrl: string): Promise<Array<string>> {
        const rows = await this.queryAll(SQL_QUERY_POST_COLLECTION_LIST, postUrl);
        return rows.map(row => row['collection_name']);
    }

    async getBookmarks(): Promise<Array<Bookmark>> {
        const rows = await this.queryAll(SQL_QUERY_BOOKMARK_LIST);
        return rows.map(row => ({
            name: row.name,
            path: row.path,
            monitor: !!row.monitor,
            createdAt: new Date(row.created_at),
            download: !!row.download
        }));
    }

    createBookmark(bookmark: Bookmark, seenPosts: Array<Post>): Promise<void> {
        const { name, path, monitor, download } = bookmark;
        return this.runInTransaction(async () => {
            await this.exec(SQL_CREATE_BOOKMARK, name, path, monitor, download);
            await this.addPostsToBookmark(bookmark.name, seenPosts, true);
        });
    }

    removeBookmark(name: string): Promise<void> {
        return this.exec(SQL_REMOVE_BOOKMARK, name);
    }

    modifyBookmark(bookmark: Bookmark): Promise<void> {
        const { name, path, monitor, download } = bookmark;
        return this.exec(SQL_MODIFY_BOOKMARK, path, monitor, download, name);
    }

    addPostsToBookmark(bookmarkName: string, posts: Array<Post>, seen?: boolean): Promise<void> {
        return this.runInTransaction(async () => {
            for (const post of posts) {
                await this.storePost(post);
                await this.exec(SQL_ADD_OR_IGNORE_BOOKMARK_TO_POST, bookmarkName, post.url, seen);
            }
        });
    }

    async getPostsInBookmark(bookmarkName: string, offset: number, limit: number): Promise<Array<Post & BookmarkPost>> {
        const rows = await this.queryAll(SQL_QUERY_BOOKMARK_POSTS_WINDOWED, bookmarkName, limit, offset);
        return rows.map(row => ({
            ...JSON.parse(row.data),
            seen: Boolean(row.seen)
        }));
    }

    async getUnseenPostsCountInBookmark(bookmarkName: string): Promise<number> {
        const row = await this.queryOne(SQL_QUERY_BOOKMARK_UNSEEN_POST_COUNT, bookmarkName);
        return row['count(*)'];
    }

    async getBookmarkPostSeen(bookmarkName: string, postUrl: string): Promise<boolean> {
        const row = await this.queryOne(SQL_QUERY_IS_BOOKMARK_POST_SEEN, bookmarkName, postUrl);
        return row && Boolean(row['seen']);
    }

    markAllPostsForBookmarkAsSeen(bookmarkName: string): Promise<void> {
        return this.exec(SQL_MARK_ALL_BOOKMARK_TO_POST_AS_SEEN, bookmarkName);
    }

    markPostForBookmarkAsSeen(bookmarkName: string, postUrl: string): Promise<void> {
        return this.exec(SQL_MARK_BOOKMARK_TO_POST_AS_SEEN, bookmarkName, postUrl);
    }

    async getActiveDownloads(): Promise<DownloadInfo[]> {
        return (await this.queryAll(SQL_QUERY_ACTIVE_DOWNLOAD_LIST))
            .map(record => ({
                post: JSON.parse(record.data),
                subDirPath: record.sub_dir?.split('/'),
                status: record.status
            }));
    }

    async saveDownload(download: DownloadInfo): Promise<void> {
        await this.storePost(download.post);
        await this.exec(SQL_SAVE_DOWNLOAD, download.post.url, download.subDirPath.join('/'), download.status);
    }

    async getPostsDownloadStatuses(postUrls: string[]): Promise<Array<DownloadStatus | null>> {
        const postUrlListString = postUrls.map(url => `'${url}'`).join(', ');
        const rows = await this.queryAll(SQL_QUERY_DOWNLOADED_POSTS_BY_URL_LIST.replace('$post_url_list', postUrlListString));
        return postUrls.map(url => {
            const row = rows.find(row => row.url === url);
            const status = (row?.status ?? null) as DownloadStatus | null;
            if (_.isEmpty(status)) {
                return null;
            } else {
                return status;
            }
        });
    }

    async getCachedFile(url: string): Promise<string | null> {
        const presentRecord = await this.queryOne(SQL_QUERY_FILECACHE_FOR_URL, url);
        return presentRecord?.local_path;
    }

    deleteFileCache(url: string): Promise<void> {
        return this.exec(SQL_DELETE_FILECACHE_FOR_URL, url);
    }

    createFileCache(url: string, localPath: string, size: number): Promise<void> {
        return this.exec(SQL_CREATE_FILECACHE_FOR_URL, url, localPath, size, Date.now() / 1000);
    }

    async getCacheEntries(): Promise<FileCacheEntry[]> {
        const rows = await this.queryAll(SQL_QUERY_FILECACHE_LIST);
        return rows.map(r => ({
            url: r.url,
            localPath: r.local_path,
            size: r.size,
            createdAt: r.created_at && new Date(r.created_at * 1000)
        }));
    }
}