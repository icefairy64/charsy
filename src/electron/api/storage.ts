import {
    IPC_BOOKMARK_CREATE,
    IPC_BOOKMARK_LIST,
    IPC_BOOKMARK_MARK_ALL_POSTS_AS_SEEN,
    IPC_BOOKMARK_MARK_POST_AS_SEEN, IPC_BOOKMARK_MODIFY,
    IPC_BOOKMARK_POST_IS_SEEN,
    IPC_BOOKMARK_POST_LIST, IPC_BOOKMARK_REMOVE,
    IPC_BOOKMARK_UNSEEN_POST_COUNT,
    IPC_COLLECTION_ADD_POST,
    IPC_COLLECTION_CREATE,
    IPC_COLLECTION_GET_POSTS,
    IPC_COLLECTION_LIST,
    IPC_COLLECTION_LIST_FOR_POST,
    IPC_COLLECTION_MODIFY, IPC_COLLECTION_REMOVE,
    IPC_COLLECTION_REMOVE_POST,
    IPC_EVENT_COLLECTION_CREATED,
    IPC_EVENT_COLLECTION_MODIFIED, IPC_EVENT_COLLECTION_REMOVED,
    IPC_EVENT_POST_ADDED_TO_COLLECTION,
    IPC_EVENT_POST_REMOVED_FROM_COLLECTION,
    PostCollection,
    SCHEMA_CACHE_PASSTHROUGH
} from './storage-symbols';
import { Deferred } from '../util/deferred';
import { getHttpClient, HttpClient } from '../http/client';
import { Post } from '../service/service';
import {
    Bookmark,
    BookmarkPost,
    IPC_EVENT_BOOKMARK_CREATED, IPC_EVENT_BOOKMARK_MODIFIED,
    IPC_EVENT_BOOKMARK_POST_SEEN, IPC_EVENT_BOOKMARK_REMOVED,
    IPC_EVENT_BOOKMARK_UNSEEN_CLEAR,
    IPC_EVENT_BOOKMARK_UNSEEN_UPDATED
} from './bookmark-symbols';
import { broadcastClientEvent } from '../util/electron';
import { getFilenameMediaType, getMediaTypeString } from '@/electron/util/media-type-util';
import { Observable, Subject } from 'rxjs';
import { DownloadInfo } from '@/electron/api/download-symbols';
import { isElectron, registerIpcHandler } from '@/util/remote-util';
import { Readable } from 'stream';
import HttpStreamHandler from '@/electron/http/stream-handler';
import OmniboxApi from '@/electron/api/omnibox';
import { createLogger } from '@/logging';
import { Logger } from '@/electron/log/logger';
import { AbstractStorage } from '@/electron/api/storage-abstract';
import { AbstractFileStorage } from '@/electron/api/filestorage-abstract';
import { DownloadStatus } from '@/electron/api/download';
import { OmniboxResult } from '@/electron/api/omnibox-symbols';
import _ from 'lodash';
import { PreferencesApi } from '@/electron/api/preferences';

const STORAGE_PATH = 'file_storage';
const FILECACHE_PATH = 'cache';

export interface CollectionToPostInfo {
    collection: PostCollection,
    post: Post
}

let getDbImpl: (dbPath?: string) => Promise<AbstractStorage>;

if (isElectron()) {
    getDbImpl = async dbPath => {
        if (!dbPath) {
            throw new Error('No path provided');
        }
        return new (await import('./storage-sqlite.js')).SqliteStorageImpl(dbPath);
    }
} else {
    getDbImpl = async () => new (await import('./storage-pouchdb.js')).PouchDbStorageImpl();
}

let getFileStorageImpl: (dbImpl: AbstractStorage, preferences: PreferencesApi) => Promise<AbstractFileStorage | undefined>;

if (isElectron()) {
    getFileStorageImpl = async (dbImpl: AbstractStorage, preferences: PreferencesApi) => new (await import('./filestorage-node.js')).NodeFileStorageImpl(dbImpl, preferences, STORAGE_PATH, FILECACHE_PATH);
} else {
    getFileStorageImpl = () => Promise.resolve(undefined);
}

export class StorageApi {
    private readonly initDeferred: Deferred<void>;
    private readonly http: HttpClient;
    private readonly collectionNewPostSubject: Subject<CollectionToPostInfo>;
    private readonly collectionRemovedPostSubject: Subject<CollectionToPostInfo>;
    private readonly preferences: PreferencesApi;

    private dbImpl!: AbstractStorage;
    private fsImpl?: AbstractFileStorage;

    private logger: Logger;

    private readonly cachePromises: {
        [url: string]: Promise<string>
    };

    private readonly postStorePromises: {
        [url: string]: Promise<void>
    };

    private readonly streamHandlers: {
        [url: string]: HttpStreamHandler
    };

    constructor(preferences: PreferencesApi, dbPath?: string) {
        this.initDeferred = new Deferred<void>();
        this.http = getHttpClient();
        this.logger = createLogger('Storage');
        this.collectionNewPostSubject = new Subject<CollectionToPostInfo>();
        this.collectionRemovedPostSubject = new Subject<CollectionToPostInfo>();
        this.preferences = preferences;

        this.cachePromises = {};
        this.postStorePromises = {};
        this.streamHandlers = {};

        this.initDeferred.promise.catch(e => this.logger.error('Failed to initialize', e));

        if (PX_PLATFORM_ELECTRON) {
            import('electron').then(electron => {
                electron.protocol.registerStreamProtocol(`${SCHEMA_CACHE_PASSTHROUGH}`, this.handleCachePassthroughUrlRequest.bind(this));
                electron.protocol.registerStreamProtocol(`${SCHEMA_CACHE_PASSTHROUGH}s`, this.handleCachePassthroughUrlRequest.bind(this));
            });
        }

        getDbImpl(dbPath)
            .then(async db => {
                this.dbImpl = db;
                await db.initDeferred.promise;
                this.onDbInit(null);
            })
            .catch(this.onDbInit.bind(this));
    }

    async onDbInit(err: Error | null) {
        if (!err) {
            try {
                this.fsImpl = await getFileStorageImpl(this.dbImpl, this.preferences);
                if (this.fsImpl) {
                    await this.fsImpl.init();
                }
                this.initDeferred.resolve();
            }
            catch (e) {
                this.initDeferred.reject(e);
            }
        } else {
            this.initDeferred.reject(err);
        }
    }

    registerIpcHandlers() {
        registerIpcHandler(IPC_COLLECTION_LIST, this.getCollections.bind(this));
        registerIpcHandler(IPC_COLLECTION_CREATE, this.createCollection.bind(this));
        registerIpcHandler(IPC_COLLECTION_REMOVE, this.removeCollection.bind(this));
        registerIpcHandler(IPC_COLLECTION_MODIFY, this.modifyCollection.bind(this));
        registerIpcHandler(IPC_COLLECTION_ADD_POST, this.addPostToCollection.bind(this));
        registerIpcHandler(IPC_COLLECTION_REMOVE_POST, this.removePostFromCollection.bind(this));
        registerIpcHandler(IPC_COLLECTION_GET_POSTS, this.getPostsInCollection.bind(this));
        registerIpcHandler(IPC_COLLECTION_LIST_FOR_POST, this.getPostCollections.bind(this));

        registerIpcHandler(IPC_BOOKMARK_LIST, this.getBookmarks.bind(this));
        registerIpcHandler(IPC_BOOKMARK_CREATE, this.createBookmark.bind(this));
        registerIpcHandler(IPC_BOOKMARK_REMOVE, this.removeBookmark.bind(this));
        registerIpcHandler(IPC_BOOKMARK_MODIFY, this.modifyBookmark.bind(this));
        registerIpcHandler(IPC_BOOKMARK_POST_LIST, this.getPostsInBookmark.bind(this));
        registerIpcHandler(IPC_BOOKMARK_UNSEEN_POST_COUNT, this.getUnseenPostsCountInBookmark.bind(this));
        registerIpcHandler(IPC_BOOKMARK_POST_IS_SEEN, this.getBookmarkPostSeen.bind(this));
        registerIpcHandler(IPC_BOOKMARK_MARK_ALL_POSTS_AS_SEEN, this.markAllPostsForBookmarkAsSeen.bind(this));
        registerIpcHandler(IPC_BOOKMARK_MARK_POST_AS_SEEN, this.markPostForBookmarkAsSeen.bind(this));
    }

    getFileStorage(): Promise<AbstractFileStorage | undefined> {
        return this.initDeferred.promise.then(() => this.fsImpl);
    }

    async getCachedFileStream(url: string, range?: string): Promise<{ stream: ReadableStream; length: number | Promise<number>; range: string | undefined } | ReadableStream> {
        await this.initDeferred.promise;

        if (!this.fsImpl) {
            throw new Error('Not supported in current environment');
        }

        const result = await this.fsImpl.getCachedFileStream(url, range);
        if (!result) {
            return this.http.stream(url, {
                range
            }).then(x => x.stream);
        }
        return result;
    }

    private async handleCachePassthroughUrlRequest(request: Electron.ProtocolRequest, callback: (response: Readable | Electron.ProtocolResponse) => void) {
        if (PX_PLATFORM_ELECTRON) {
            if (!this.fsImpl) {
                throw new Error('Not supported in current environment');
            }

            const remoteUrl = request.url.replace(SCHEMA_CACHE_PASSTHROUGH, 'http');
            const range = request.headers.Range;
            const mediaType = getFilenameMediaType(remoteUrl);
            let data = await this.getCachedFileStream(remoteUrl, range);
            const headers: Record<string, string | string[]> = {
                'Content-Type': mediaType && getMediaTypeString(mediaType) || 'application/text'
            };

            if (!data) {
                data = await this.http.stream(remoteUrl, {
                    range
                }).then(x => x.stream);
            }

            const nodeWebStreams = await import('stream/web'),
                nodeReadableStream = nodeWebStreams.ReadableStream as { new(): ReadableStream };

            if (!(data instanceof nodeReadableStream)) {
                headers['Content-Length'] = (await data.length).toString();
                if (data.range) {
                    headers['Accept-Ranges'] = 'bytes';
                    headers['Content-Range'] = data.range;
                }
            }

            const stream = data instanceof nodeReadableStream ? data : data.stream,
                converters = await import('@/electron/stream/node-to-web');

            callback({
                statusCode: range == null ? 200 : 206,
                headers: headers,
                data: converters.convertWebReadableToNode(stream)
            });
        }
    }

    async storePost(post: Post, updateIfPresent: boolean = false): Promise<void> {
        if (this.postStorePromises[post.url]) {
            return this.postStorePromises[post.url];
        }
        await this.initDeferred.promise;
        const deferred = new Deferred<void>();
        this.postStorePromises[post.url] = deferred.promise;
        try {
            await this.dbImpl.storePost(post, updateIfPresent);
            deferred.resolve();
        }
        catch (e) {
            deferred.reject(e);
        }
        finally {
            delete this.postStorePromises[post.url];
        }
        return deferred.promise;
    }

    getPostToCollectionKey(collectionName: string, postUrl: string) {
        return `${collectionName}-${postUrl}`;
    }

    getPostToBookmarkKey(bookmarkName: string, postUrl: string) {
        return `${bookmarkName}-${postUrl}`;
    }

    async addPostToCollection(collectionName: string, post: Post): Promise<void> {
        await this.initDeferred.promise;
        await this.storePost(post);
        await this.dbImpl.addPostToCollection(collectionName, post);
        broadcastClientEvent(IPC_EVENT_POST_ADDED_TO_COLLECTION, collectionName, post);
        this.getCollectionByName(collectionName)
            .then(collection => this.collectionNewPostSubject.next({
                collection,
                post
            }));
    }

    async removePostFromCollection(collectionName: string, post: Post): Promise<void> {
        await this.initDeferred.promise;
        await this.dbImpl.removePostFromCollection(collectionName, post);
        broadcastClientEvent(IPC_EVENT_POST_REMOVED_FROM_COLLECTION, collectionName, post);
        this.getCollectionByName(collectionName)
            .then(collection => this.collectionRemovedPostSubject.next({
                collection,
                post
            }));
    }

    async getCollections(): Promise<Array<PostCollection>> {
        await this.initDeferred.promise;
        return this.dbImpl.getCollections();
    }

    async getCollectionByName(name: string): Promise<PostCollection> {
        await this.initDeferred.promise;
        return this.dbImpl.getCollectionByName(name);
    }

    async createCollection(collection: PostCollection) {
        const { name, description, download, syncDescriptor, syncEnabled } = collection;
        await this.initDeferred.promise;
        await this.dbImpl.createCollection(collection);
        broadcastClientEvent(IPC_EVENT_COLLECTION_CREATED, {
            name,
            description,
            download,
            syncDescriptor,
            syncEnabled
        });
    }

    async removeCollection(collection: PostCollection) {
        const { name } = collection;
        await this.initDeferred.promise;
        await this.dbImpl.removeCollection(name);
        broadcastClientEvent(IPC_EVENT_COLLECTION_REMOVED, name);
    }

    async modifyCollection(collection: PostCollection) {
        const { name, description, download, syncDescriptor, syncEnabled } = collection;
        await this.initDeferred.promise;
        await this.dbImpl.modifyCollection(collection);
        broadcastClientEvent(IPC_EVENT_COLLECTION_MODIFIED, {
            name,
            description,
            download,
            syncDescriptor,
            syncEnabled
        });
    }

    async getPostsInCollection(collectionName: string, query: string | null, offset: number, limit: number): Promise<Array<Post>> {
        await this.initDeferred.promise;
        return this.dbImpl.getPostsInCollection(collectionName, query, offset, limit);
    }

    async getPostCollections(postUrl: string): Promise<Array<string>> {
        await this.initDeferred.promise;
        return this.dbImpl.getPostCollections(postUrl);
    }

    async getBookmarks(): Promise<Array<Bookmark>> {
        await this.initDeferred.promise;
        return this.dbImpl.getBookmarks();
    }

    async removeBookmark(bookmark: Bookmark) {
        const { name } = bookmark;
        await this.initDeferred.promise;
        await this.dbImpl.removeBookmark(name);
        broadcastClientEvent(IPC_EVENT_BOOKMARK_REMOVED, name);
    }

    async createBookmark(bookmark: Bookmark, seenPosts: Array<Post>) {
        const { name, path, monitor, download } = bookmark;
        await this.initDeferred.promise;
        await this.dbImpl.createBookmark(bookmark, seenPosts);
        broadcastClientEvent(IPC_EVENT_BOOKMARK_CREATED, {
            name,
            path,
            monitor
        });
    }

    async modifyBookmark(bookmark: Bookmark) {
        const { name, path, download, monitor } = bookmark;
        await this.initDeferred.promise;
        await this.dbImpl.modifyBookmark(bookmark);
        broadcastClientEvent(IPC_EVENT_BOOKMARK_MODIFIED, {
            name,
            path,
            download,
            monitor
        });
    }

    async addPostsToBookmark(bookmarkName: string, posts: Array<Post>, seen: boolean = false) {
        await this.initDeferred.promise;
        await this.dbImpl.addPostsToBookmark(bookmarkName, posts, seen);
    }

    async getPostsInBookmark(bookmarkName: string, offset: number, limit: number): Promise<Array<Post & BookmarkPost>> {
        await this.initDeferred.promise;
        return this.dbImpl.getPostsInBookmark(bookmarkName, offset, limit);
    }

    async getUnseenPostsCountInBookmark(bookmarkName: string): Promise<number> {
        await this.initDeferred.promise;
        return this.dbImpl.getUnseenPostsCountInBookmark(bookmarkName);
    }

    async getBookmarkPostSeen(bookmarkName: string, postUrl: string): Promise<boolean> {
        await this.initDeferred.promise;
        return this.dbImpl.getBookmarkPostSeen(bookmarkName, postUrl);
    }

    async markAllPostsForBookmarkAsSeen(bookmarkName: string) {
        await this.initDeferred.promise;
        await this.dbImpl.markAllPostsForBookmarkAsSeen(bookmarkName);
        broadcastClientEvent(IPC_EVENT_BOOKMARK_UNSEEN_CLEAR, bookmarkName);
    }

    async markPostForBookmarkAsSeen(bookmarkName: string, postUrl: string) {
        await this.initDeferred.promise;
        await this.dbImpl.markPostForBookmarkAsSeen(bookmarkName, postUrl);
        const unseenCount = await this.getUnseenPostsCountInBookmark(bookmarkName);
        broadcastClientEvent(IPC_EVENT_BOOKMARK_UNSEEN_UPDATED, bookmarkName, unseenCount);
        broadcastClientEvent(IPC_EVENT_BOOKMARK_POST_SEEN, bookmarkName, postUrl);
    }

    getCollectionNewPostObservable(): Observable<CollectionToPostInfo> {
        return this.collectionNewPostSubject;
    }

    getCollectionRemovedPostObservable(): Observable<CollectionToPostInfo> {
        return this.collectionRemovedPostSubject;
    }

    async getActiveDownloads(): Promise<DownloadInfo[]> {
        await this.initDeferred.promise;
        return this.dbImpl.getActiveDownloads();
    }

    async saveDownload(download: DownloadInfo) {
        await this.initDeferred.promise;
        await this.storePost(download.post);
        await this.dbImpl.saveDownload(download);
    }

    async getPostsDownloadStatuses(postUrls: string[]): Promise<Array<DownloadStatus | null>> {
        await this.initDeferred.promise;
        return this.dbImpl.getPostsDownloadStatuses(postUrls);
    }

    registerOmnibox(omnibox: OmniboxApi) {
        omnibox.registerContext({
            type: 'COLLECTION',
            name: 'Collections',
            aliases: ['c'],
            queryCallback: async (query: string) => {
                const collections = await this.getCollections(),
                    results = await Promise.all(collections.map(c => this.collectionOmniboxQuery(c.name, query)));
                return _.flatten(results);
            }
        })

        omnibox.registerContext({
            type: 'GLOBAL',
            name: 'Bookmarks',
            queryCallback: () => Promise.resolve([]),
            queryContexts: async (query) => (await this.getBookmarks()).map(c => ({
                type: 'BOOKMARK' as 'BOOKMARK',
                name: c.name,
                queryCallback: (query: string) => Promise.resolve([])
            })).filter(c => c.name.toLocaleLowerCase().includes(query.toLocaleLowerCase()))
        });
    }

    async collectionOmniboxQuery(collectionName: string, query: string): Promise<OmniboxResult[]> {
        const posts = await this.getPostsInCollection(collectionName, query, 0, 20);

        if (posts.length === 0) {
            return [];
        }

        return [{
            resultType: 'COLLECTION_SEARCH',
            contextType: 'COLLECTION',
            resultName: 'Results',
            collectionName,
            query,
            contextName: `Collection ${collectionName}`
        }];
    }
}

