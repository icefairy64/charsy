import { AbstractPreferencesStorage } from '@/electron/api/preferences-abstract';
import { OptionsObject } from '@/electron/options/options';
import { Preferences } from '@/electron/api/preferences-symbols';
import { throttleTime } from 'rxjs/operators';
import { Logger } from '@/electron/log/logger';
import { createLogger } from '@/logging';
import { OptionsFileHandle } from '@/electron/options/options-file';

export class NodePreferencesStorage extends AbstractPreferencesStorage {
    private preferencesHandle: OptionsFileHandle<Preferences>;
    private preferences: OptionsObject<Preferences>;
    private readonly logger: Logger;

    constructor(preferencesPath: string) {
        super();
        this.logger = createLogger('NodePreferences');
        this.preferencesHandle = new OptionsFileHandle<Preferences>(preferencesPath);
        this.preferences = this.preferencesHandle.options;
        this.preferencesHandle.loadFromFile().catch(e => this.logger.error('Failed to load preferences', e));
        this.preferences.getValueObservable().pipe(
            throttleTime(1000, undefined, {
                trailing: true
            })
        ).subscribe(val => this.onPrefsChanged?.(val));
    }

    getCurrentPrefs() {
        return this.preferences.currentValue;
    }

    updatePrefs(preferences: Preferences): void {
        this.preferences.setValue(preferences);
    }
}