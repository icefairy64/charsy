import { AbstractStorage } from '@/electron/api/storage-abstract';
import { IncomingHttpHeaders } from 'http';
import { Logger } from '@/electron/log/logger';
import { PreferencesApi } from '@/electron/api/preferences';
import _ from 'lodash';
import { FileCacheEntry } from '@/electron/api/storage-symbols';
import { formatFileSize } from '@/electron/util/common-util';

export abstract class AbstractFileStorage {
    protected readonly dbImpl: AbstractStorage;
    protected readonly cachePromises: Record<string, Promise<{ filename: string, size: number }>> = {};
    protected readonly cacheRelPath: string;
    protected readonly preferences: PreferencesApi;

    protected abstract readonly passthroughSupported: boolean;
    protected abstract readonly logger: Logger;

    constructor(dbImpl: AbstractStorage, cacheRelPath: string, preferences: PreferencesApi) {
        this.dbImpl = dbImpl;
        this.cacheRelPath = cacheRelPath;
        this.preferences = preferences;
    }

    abstract isFilePresent(localPath: string[]): Promise<boolean>;
    abstract deleteFile(localPath: string[]): Promise<boolean>;
    abstract getFileStream(localPath: string[], range?: string): Promise<{ stream: ReadableStream; length: number | Promise<number>; range: string | undefined } | ReadableStream>;
    abstract storeFile(url: string, relPath: string[], passthroughStream?: WritableStream, fileDownloadContext?: any, filename?: string): Promise<{ filename: string, size: number }>;
    abstract passthroughFileStream(url: string, targetRelPath: string[] | undefined, range: string | undefined, filename?: string): Promise<{ stream: ReadableStream; length: number | Promise<number>; range: string | undefined }>;

    init(): Promise<void> {
        this.clearCache().catch(e => this.logger.error('Failed to clear cache', e));
        return Promise.resolve();
    }

    protected getNormalizedCachedFileUrl(url: string) {
        const queryStart = url.indexOf('?');
        if (queryStart > 0) {
            url = url.substring(0, queryStart);
        }
        return url;
    }

    protected async saveCacheInfo(normalizedUrl: string, localPath: string, size: number) {
        try {
            await this.dbImpl.createFileCache(normalizedUrl, localPath, size);
        }
        catch (e) {
            this.logger.warn(`Failed to save cache info for ${normalizedUrl}`, e);
        }
        finally {
            delete this.cachePromises[normalizedUrl]
        }
    }

    async getCachedFileStream(url: string, range?: string): Promise<{ stream: ReadableStream; length: number | Promise<number>; range: string | undefined } | ReadableStream | undefined> {
        const normalizedUrl = this.getNormalizedCachedFileUrl(url);
        if (this.cachePromises[normalizedUrl] && !range) {
            return;
        }
        if (!this.dbImpl.fileStorageSupported) {
            return;
        }
        const localPath = await this.dbImpl.getCachedFile(normalizedUrl);
        let localFilePresent = false;
        if (localPath) {
            localFilePresent = await this.isFilePresent([this.cacheRelPath, localPath]);
            if (!localFilePresent) {
                await this.dbImpl.deleteFileCache(normalizedUrl);
            }
        }
        if (localFilePresent && localPath) {
            return this.getFileStream([this.cacheRelPath, localPath], range);
        } else {
            const noCaching = this.cachePromises[normalizedUrl] && range;
            return this.passthroughSupported ? this.passthroughFileStream(url,  noCaching ? undefined : [this.cacheRelPath], range) : undefined;
        }
    }

    async clearCache() {
        if (!this.dbImpl.fileStorageSupported) {
            return;
        }

        const limit = this.preferences.getCurrentPrefs()?.fileCache?.storageLimit;
        if (limit == null) {
            this.logger.info('No cache size limit is set, skipping clearing');
            return;
        }

        this.logger.info('Clearing old cache entries');

        const entries = await this.dbImpl.getCacheEntries(),
            totalSize = entries.reduce((a: number | undefined, x: FileCacheEntry) => {
                if (a == null || x.size == null) {
                    return undefined;
                }
                return a + x.size;
            }, 0),
            excessSize = totalSize == null ? undefined : Math.max(totalSize - limit, 0);

        let cleared = 0, removedEntries = 0, attemptedToRemoveEntries = 0;
        while (excessSize == null || cleared < excessSize) {
            const entry = entries.shift();
            if (!entry) {
                break;
            }

            const isRemoved = await this.deleteFile([this.cacheRelPath, entry.localPath]);

            attemptedToRemoveEntries++;
            if (isRemoved) {
                await this.dbImpl.deleteFileCache(entry.url);
                cleared += entry.size || 0;
                removedEntries++;
            }
        }

        this.logger.info(`Cleared ${formatFileSize(cleared)} across ${removedEntries} (${attemptedToRemoveEntries}) entries`);
    }
}