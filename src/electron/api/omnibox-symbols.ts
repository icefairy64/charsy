export type OmniboxContextType = 'GLOBAL' | 'COLLECTION' | 'SERVICE' | 'BOOKMARK';
export type OmniboxResultType = Exclude<OmniboxContextType, 'GLOBAL'> | 'SERVICE_PAGE' | 'SERVICE_SEARCH' | 'COLLECTION_SEARCH';

export interface OmniboxResultBase {
    contextName: string,
    contextType: OmniboxContextType | 'GLOBAL',
    resultName: string,
    resultType: OmniboxResultType
}

export interface OmniboxResultCollection extends OmniboxResultBase {
    resultType: 'COLLECTION'
}

export interface OmniboxResultCollectionSearch extends OmniboxResultBase {
    resultType: 'COLLECTION_SEARCH',
    collectionName: string,
    query: string
}

export interface OmniboxResultBookmark extends OmniboxResultBase {
    resultType: 'BOOKMARK'
}

export interface OmniboxResultService extends OmniboxResultBase {
    resultType: 'SERVICE',
    serviceName: string
}

export interface OmniboxResultPage extends OmniboxResultBase {
    resultType: 'SERVICE_PAGE',
    pagePath: string
}

export interface OmniboxResultServiceSearch extends OmniboxResultBase {
    resultType: 'SERVICE_SEARCH',
    query: string
}

export type OmniboxResult = OmniboxResultCollection | OmniboxResultBookmark | OmniboxResultService | OmniboxResultPage | OmniboxResultServiceSearch | OmniboxResultCollectionSearch;

export const IPC_OMNIBOX_QUERY = 'omnibox-query';