import { Post } from '@/electron/service/service';
import { DownloadStatus } from '@/electron/api/download';

export const IPC_EVENT_DOWNLOAD_INFO = 'download-info';

export const IPC_DOWNLOAD_LIST = 'download-list';
export const IPC_DOWNLOAD_ENQUEUE = 'download-enqueue';
export const IPC_DOWNLOAD_POSTS_STATUS = 'download-posts-status';

export interface DownloadInfo {
    post: Post,
    filename?: string,
    subDirPath: string[],
    status: DownloadStatus,
    bytesDownloaded?: number,
    bytesTotal?: number,
    startTime?: number,
    rollingDownloadSpeed?: number[]
}