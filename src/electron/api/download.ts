import { BookmarkApi } from "@/electron/api/bookmark";
import { Post } from "@/electron/service/service";
import _, { has } from "lodash";
import { distinctUntilChanged, filter, groupBy, map, mergeMap, throttleTime } from "rxjs/operators";
import { merge, Subject } from "rxjs";
import { getHttpClient, HttpClient } from "@/electron/http/client";
import { getPostMainAttachmentInfo } from "@/electron/util/post-util";
import * as Path from "path";
import { StorageApi } from '@/electron/api/storage';
import {
    DownloadInfo,
    IPC_DOWNLOAD_ENQUEUE,
    IPC_DOWNLOAD_LIST,
    IPC_DOWNLOAD_POSTS_STATUS,
    IPC_EVENT_DOWNLOAD_INFO
} from '@/electron/api/download-symbols';
import { broadcastClientEvent } from '@/electron/util/electron';
import { createLogger } from '@/logging';
import { Logger } from '@/electron/log/logger';
import { registerIpcHandler } from '@/util/remote-util';
import { DelayedTask } from '@/electron/util/delayed-task';
import { Deferred } from '@/electron/util/deferred';

export interface DownloadConfiguration {
    downloadPath: string,
    filenameExpression?: string,
    concurrentDownloads: number
}

export type DownloadStatus = 'COMPLETE' | 'ERROR' | 'PENDING' | 'IN_PROGRESS';

export class DownloadApi {
    private readonly config: DownloadConfiguration;
    private readonly bookmarkService: BookmarkApi;
    private readonly storageService: StorageApi;
    private readonly downloadInfoSubject: Subject<DownloadInfo>;
    private readonly downloadInfo: DownloadInfo[];
    private readonly http: HttpClient;
    private logger: Logger;

    constructor(config: DownloadConfiguration, bookmarkService: BookmarkApi, storageService: StorageApi) {
        this.config = config;
        this.bookmarkService = bookmarkService;
        this.storageService = storageService;
        this.downloadInfoSubject = new Subject<DownloadInfo>();
        this.downloadInfo = [];
        this.http = getHttpClient();
        this.logger = createLogger('Download');

        merge(
            bookmarkService.getNewPostObservable()
                .pipe(
                    filter(info => info.bookmark.download),
                    map(info => ({
                        post: info.post
                    }))
                ),

            storageService.getCollectionNewPostObservable()
                .pipe(
                    filter(info => info.collection.download),
                    map(info => ({
                        post: info.post,
                        relDirPath: [info.collection.name]
                    }))
                )
        )
            .subscribe(this.onNewPost.bind(this));

        this.downloadInfoSubject
            .pipe(
                groupBy(info => info.post.url),
                mergeMap(postInfo$ => postInfo$.pipe(
                    throttleTime(1000, undefined, {
                        leading: true,
                        trailing: true
                    })
                ))
            )
            .subscribe(this.broadcastInfo.bind(this));

        this.downloadInfoSubject
            .pipe(
                groupBy(info => info.post.url),
                mergeMap(postInfo$ => postInfo$.pipe(
                    distinctUntilChanged((a, b) => a.status === b.status),
                    throttleTime(1000, undefined, {
                        leading: true,
                        trailing: true
                    })
                ))
            )
            .subscribe(this.storeInfo.bind(this));

        this.loadStoredDownloads().catch(e => this.logger.error('Failed to load downloads', e));
    }

    async loadStoredDownloads() {
        const downloads = await this.storageService.getActiveDownloads();
        this.logger.info(`Loaded ${downloads.length} downloads`);
        this.downloadInfo.push(...downloads);
        downloads.filter(info => info.status === 'IN_PROGRESS')
            .forEach(info => this.downloadPost(info.post, info.subDirPath));
        for (let i = 0; i < this.config.concurrentDownloads; i++) {
            this.startNextDownload();
        }
    }

    registerIpcHandlers() {
        // @ts-ignore
        registerIpcHandler(IPC_DOWNLOAD_LIST, (...args) => this.getDownloads(...args));
        // @ts-ignore
        registerIpcHandler(IPC_DOWNLOAD_ENQUEUE, (...args) => this.enqueuePost(...args));
        // @ts-ignore
        registerIpcHandler(IPC_DOWNLOAD_POSTS_STATUS, (...args) => this.getPostsStatuses(...args));
    }

    async onNewPost(info: { post: Post, relDirPath?: string[] }) {
        this.logger.info(`New post: ${info.post.url}`);
        try {
            await this.enqueuePost(info.post, info.relDirPath);
            this.startNextDownload();
        }
        catch (e) {
            this.logger.error(`Failed to enqueue new post`, e);
        }
    }

    onDownloadFinish() {
        this.startNextDownload();
    }

    startNextDownload() {
        const nextDownload = _.first(this.downloadInfo.filter(info => info.status === 'PENDING'));
        const activeDownloadCount = this.downloadInfo.filter(info => info.status === 'IN_PROGRESS').length;
        if (nextDownload && activeDownloadCount < this.config.concurrentDownloads) {
            this.downloadPost(nextDownload.post, nextDownload.subDirPath);
        }
    }

    updateDownloadInfo(info: DownloadInfo) {
        const presentInfo = this.findPostDownloadInfo(info.post);
        if (presentInfo) {
            this.downloadInfo[this.downloadInfo.indexOf(presentInfo)] = info;
        } else {
            this.downloadInfo.push(info);
        }
        this.downloadInfoSubject.next(info);
    }

    findPostDownloadInfo(post: Post) {
        return this.downloadInfo.find(i => i.post.url === post.url);
    }

    getFilePath(...relPath: string[]) {
        return Path.join(this.config.downloadPath, ...relPath);
    }

    async enqueuePost(post: Post, subDirPath: string[] = []) {
        const presentInfo = this.findPostDownloadInfo(post);
        if (presentInfo) {
            throw new Error('Post is already present');
        }
        const storedInfo = (await this.storageService.getPostsDownloadStatuses([post.url]))[0];
        if (storedInfo === 'COMPLETE') {
            throw new Error('Post is already downloaded');
        }

        this.updateDownloadInfo({
            post,
            subDirPath,
            status: 'PENDING'
        });
        this.startNextDownload();
    }

    getDownloads() {
        return this.downloadInfo.sort((a, b) => {
            const statuses = ['IN_PROGRESS'];
            return statuses.indexOf(b.status) - statuses.indexOf(a.status);
        });
    }

    broadcastInfo(info: DownloadInfo) {
        broadcastClientEvent(IPC_EVENT_DOWNLOAD_INFO, info);
    }

    storeInfo(info: DownloadInfo) {
        this.storageService.saveDownload(info)
            .catch(e => this.logger.error(`Failed to store download for post ${info.post.url}`, e));
    }

    getPostsStatuses(postUrls: string[]) {
        return this.storageService.getPostsDownloadStatuses(postUrls);
    }

    async downloadPost(post: Post, subDirPath: string[] = []) {
        let filename: string | undefined,
            downloadedSize = 0,
            size = 0;
        try {
            const fileUrl = getPostMainAttachmentInfo(post)?.fileUrl || post.fileUrl;
            if (!fileUrl) {
                throw new Error('No main attachment present');
            }
            filename = _.last(new URL(fileUrl).pathname.split('/'));
            if (this.config.filenameExpression) {
                const filenameFn = new Function('post', `
                    const copyrightTags = post.tags.filter(tag => tag.category === 'copyright').map(tag => tag.name);
                    const characterTags = post.tags.filter(tag => tag.category === 'character').map(tag => tag.name);
                    const artistTags = post.tags.filter(tag => tag.category === 'artist').map(tag => tag.name);
                    const filename = '${filename}';
                    return \`${this.config.filenameExpression}\`;
                `);
                filename = filenameFn(post);
            }
            if (!filename) {
                throw new Error('No filename found');
            }
            this.updateDownloadInfo({
                post,
                subDirPath,
                filename,
                status: "IN_PROGRESS"
            });

            const fsImpl = await this.storageService.getFileStorage();
            if (!fsImpl) {
                throw new Error('No file storage');
            }

            const timeoutDeferred = new Deferred<never>(),
                timeoutTask = new DelayedTask(() => timeoutDeferred.reject('Download timeout'));

            this.logger.info(`Starting download: ${post.url}`);
            this.logger.debug(`File URL: ${post.fileUrl}`)
            timeoutTask.delay(10000);

            const passthrough = await Promise.race([
                    fsImpl.passthroughFileStream(post.fileUrl, [this.config.downloadPath, ...subDirPath], undefined, filename),
                    timeoutDeferred.promise
                ]),
                reader = passthrough.stream.getReader();

            timeoutTask.delay(10000);

            const size = await Promise.race([
                passthrough.length,
                timeoutDeferred.promise
            ]);

            this.logger.info(`Download started: ${post.url}`);

            const startTime = Date.now(),
                rollingDownloadSpeed: number[] = [];
            let lastDataReceiveTime: number | undefined,
                lastBufferSize = 0,
                hasData = true;

            do {
                timeoutTask.delay(10000);

                const data = await Promise.race([reader.read(), timeoutDeferred.promise]),
                    buffer = data.value;
                hasData = !(data.done || !buffer);

                if (hasData) {
                    const now = Date.now();
                    lastBufferSize += buffer.length;
                    if (!lastDataReceiveTime || now - lastDataReceiveTime > 200) {
                        if (lastDataReceiveTime) {
                            const timeDiff = now - lastDataReceiveTime,
                                immediateSpeed = lastBufferSize / timeDiff * 1000;
                            rollingDownloadSpeed.push(immediateSpeed);
                            if (rollingDownloadSpeed.length > 10) {
                                rollingDownloadSpeed.shift();
                            }
                        }
                        lastDataReceiveTime = now;
                        lastBufferSize = 0;
                    }

                    downloadedSize += buffer.length;

                    this.updateDownloadInfo({
                        post,
                        subDirPath,
                        filename,
                        status: 'IN_PROGRESS',
                        bytesDownloaded: downloadedSize,
                        bytesTotal: size,
                        startTime,
                        rollingDownloadSpeed
                    });
                }
            } while (hasData);

            this.logger.info(`Download finished: ${post.url}`);

            timeoutTask.cancel();

            this.updateDownloadInfo({
                post,
                subDirPath,
                filename,
                status: "COMPLETE",
                bytesDownloaded: downloadedSize,
                bytesTotal: size
            });
        } catch (e) {
            this.logger.error(`Download failed: ${post.url}`, e);
            this.updateDownloadInfo({
                post,
                subDirPath,
                filename,
                status: "ERROR",
                bytesDownloaded: downloadedSize,
                bytesTotal: size
            });
            throw e;
        }
        finally {
            this.onDownloadFinish();
        }
    }
}