import { IPC_OMNIBOX_QUERY, OmniboxContextType, OmniboxResult } from '@/electron/api/omnibox-symbols';
import _ from 'lodash';
import { registerIpcHandler } from '@/util/remote-util';
import { Logger } from '@/electron/log/logger';
import { createLogger } from '@/logging';

export interface OmniboxContextDescriptorBase {
    name: string,
    type: OmniboxContextType,
    aliases?: string[],
    queryCallback: (query: string) => Promise<OmniboxResult[]>
}

export interface OmniboxServiceContextDescriptor extends OmniboxContextDescriptorBase {
    type: 'SERVICE',
    serviceName: string
}

export interface OmniboxGlobalContextDescriptor extends OmniboxContextDescriptorBase {
    type: 'GLOBAL',
    queryContexts: (query: string) => Promise<OmniboxContextDescriptor[]>
}

export interface OmniboxGenericContextDescriptor extends OmniboxContextDescriptorBase {
    type: Exclude<OmniboxContextType, 'SERVICE' | 'GLOBAL'>
}

export type OmniboxContextDescriptor = OmniboxServiceContextDescriptor | OmniboxGlobalContextDescriptor | OmniboxGenericContextDescriptor;

export default class OmniboxApi {
    private contexts: OmniboxContextDescriptor[] = [];
    private logger: Logger;

    constructor() {
        this.logger = createLogger('Omnibox');
    }

    registerIpcHandlers() {
        registerIpcHandler(IPC_OMNIBOX_QUERY, this.query.bind(this));
    }

    registerContext(descriptor: OmniboxContextDescriptor) {
        this.contexts.push(descriptor);
    }

    private getContext(contextName: string): OmniboxContextDescriptor | null {
        return this.contexts.find(c => c.name.toLowerCase() === contextName.toLowerCase() || c.aliases != null && c.aliases.includes(contextName.toLowerCase())) || null;
    }

    private mapContextToResult(context: OmniboxContextDescriptor): OmniboxResult {
        switch (context.type) {
            case 'BOOKMARK': return {
                contextName: 'Global',
                contextType: 'GLOBAL' as 'GLOBAL',
                resultName: context.name,
                resultType: 'BOOKMARK'
            };
            case 'COLLECTION': return {
                contextName: 'Global',
                contextType: 'GLOBAL' as 'GLOBAL',
                resultName: context.name,
                resultType: 'COLLECTION'
            };
            case 'SERVICE': return {
                contextName: 'Global',
                contextType: 'GLOBAL' as 'GLOBAL',
                resultName: context.name,
                resultType: 'SERVICE',
                serviceName: context.serviceName
            };
            case 'GLOBAL': throw new Error('Global context should not be mapped to result');
        }
    }

    async query(contextNames: string[], query: string): Promise<OmniboxResult[]> {
        let contexts = contextNames.map(c => this.getContext(c)).filter(c => c != null).map(c => c as OmniboxContextDescriptor);
        if (_.isEmpty(contexts)) {
            contexts = this.contexts;
            query = [...contextNames, query].join(' ');
        }
        contexts = _.flatten(await Promise.all(contexts.map(async c => {
            if (c.type === 'GLOBAL') {
                return await c.queryContexts(query);
            } else {
                return c;
            }
        })));
        const results = _.flatten(await Promise.all(contexts.map(c => c.queryCallback(query).then(r => _.take(r, 5)))));
        if (contextNames.length === 0) {
            results.unshift(...contexts.map(c => this.mapContextToResult(c)).filter(r => r.resultName.toLowerCase().includes(query.toLowerCase())));
        }
        return _.uniqBy(results, r => `${r.resultType}:::${r.resultName}:::${r.contextName}`);
    }
}