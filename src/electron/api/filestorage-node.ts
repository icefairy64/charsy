import { AbstractFileStorage } from '@/electron/api/filestorage-abstract';
import fs from 'fs';
import path from 'path';
import _ from 'lodash';
import { Deferred } from '@/electron/util/deferred';
import { convertNodeReadableToWeb, convertWebWritableToNode } from '@/electron/stream/node-to-web';
import { AbstractStorage } from '@/electron/api/storage-abstract';
import { IncomingHttpHeaders } from 'http';
import { PassThrough, Writable } from 'stream';
import HttpStreamHandler from '@/electron/http/stream-handler';
import { eventPromise } from '@/electron/util/common-util';
import { NodeHttpClient } from '@/electron/http/node';
import { createLogger } from '@/logging';
import { Logger } from '@/electron/log/logger';
import { PreferencesApi } from '@/electron/api/preferences';

export class NodeFileStorageImpl extends AbstractFileStorage {
    private readonly storagePath: string;
    private readonly http: NodeHttpClient;
    private readonly streamHandlers: Record<string, HttpStreamHandler> = {};

    protected readonly logger: Logger;
    protected readonly passthroughSupported: boolean = true;

    constructor(dbImpl: AbstractStorage, preferences: PreferencesApi, storagePath: string, cacheRelPath: string) {
        super(dbImpl, cacheRelPath, preferences);
        this.storagePath = storagePath;
        this.http = new NodeHttpClient();
        this.logger = createLogger('NodeFileStorageImpl');
    }

    async init(): Promise<void> {
        await super.init();
        await fs.promises.mkdir(path.resolve(path.join(this.storagePath, this.cacheRelPath)), { recursive: true })
    }

    async isFilePresent(localPath: string[]): Promise<boolean> {
        try {
            const stat = await fs.promises.stat(path.resolve(this.storagePath, ...localPath));
            return stat.isFile() && stat.size > 0;
        } catch (e) {
            return false;
        }
    }

    async deleteFile(localPath: string[]): Promise<boolean> {
        try {
            await fs.promises.rm(path.resolve(this.storagePath, ...localPath));
            return true;
        }
        catch (e) {
            return false;
        }
    }

    async getFileStream(localPath: string[], range?: string): Promise<{ stream: ReadableStream; length: number | Promise<number>; range: string | undefined } | ReadableStream> {
        const totalLength = (await fs.promises.stat(path.resolve(this.storagePath, ...localPath))).size;
        if (range) {
            const offsets = range.substring(range.indexOf('=') + 1).split('-').map(x => x === '' ? null : +x);
            if (offsets.length > 2) {
                throw new Error('Multiple ranges are not supported');
            }
            if (offsets[1] == null || _.isNaN(offsets[1])) {
                offsets[1] = totalLength - 1;
                offsets.length = 2;
            }
            const [start, end] = offsets;
            const fileStream = fs.createReadStream(path.resolve(this.storagePath, ...localPath), {
                start: start || undefined,
                end
            });
            return {
                stream: convertNodeReadableToWeb(fileStream),
                length: end - start! + 1,
                range: `bytes ${start}-${end}/${totalLength}`
            };
        } else {
            return {
                stream: convertNodeReadableToWeb(fs.createReadStream(path.resolve(this.storagePath, ...localPath))),
                length: totalLength,
                range: undefined
            };
        }
    }

    storeFile(url: string, relPath: string[], passthroughStream?: WritableStream, fileDownloadContext?: any, filename?: string): Promise<{ filename: string, size: number }> {
        return this._storeFile(url, this.getNormalizedCachedFileUrl(url), relPath, passthroughStream && convertWebWritableToNode(passthroughStream), fileDownloadContext, filename);
    }

    private async _storeFile(url: string, normalizedUrl: string, relPath: string[], passthroughStream?: Writable, fileDownloadContext?: any, filename?: string): Promise<{ filename: string, size: number }> {
        const dirPath = path.join(this.storagePath, ...relPath);

        const deferred = new Deferred<{ filename: string, size: number }>();
        const stream = await this.http.streamNode(url, undefined, 'GET', undefined, fileDownloadContext);

        try {
            await fs.promises.access(dirPath);
        }
        catch (e) {
            await fs.promises.mkdir(dirPath, {
                recursive: true
            });
        }

        if (!filename) {
            filename = `${Date.now()}_${path.basename(normalizedUrl)}`;
        }
        const outStream = fs.createWriteStream(path.join(dirPath, filename));

        const streams: Writable[] = [outStream];

        await eventPromise(outStream, 'open');

        if (passthroughStream != null) {
            streams.push(passthroughStream);
        }

        const writePromises = streams.map(x => null as Promise<void> | null),
            drainDeferreds = streams.map(x => null as Deferred<void> | null);

        let size = 0;

        streams.forEach((stream, index) => {
            stream.on('drain', () => {
                const deferred = drainDeferreds[index];
                if (deferred) {
                    delete drainDeferreds[index];
                    deferred.resolve();
                }
            });
        });

        function doWriteTo(index: number, b: any): Promise<undefined> {
            const deferred = new Deferred<undefined>();
            const clear = streams[index].write(b, e => e ? deferred.reject(e) : deferred.resolve());
            if (!clear) {
                const drainDeferred = new Deferred<void>();
                drainDeferreds[index] = drainDeferred;
                // @ts-ignore
                return Promise.all([drainDeferred.promise, deferred.promise]);
            }
            return deferred.promise;
        }

        function writeTo(index: number, b: Buffer) {
            const promise = writePromises[index];
            b = Buffer.alloc(b.length, b);
            if (promise) {
                writePromises[index] = promise.then(() => doWriteTo(index, b));
            } else {
                writePromises[index] = doWriteTo(index, b);
            }
        }

        function write(b: Buffer) {
            size += b.length;
            for (let i = 0; i < streams.length; i++) {
                writeTo(i, b);
            }
        }

        async function close() {
            const promises = [];
            for (let i = 0; i < streams.length; i++) {
                const promise = writePromises[i];
                if (promise) {
                    promises.push(promise.then(() => streams[i].end()));
                } else {
                    streams[i].end();
                }
            }
            await Promise.all(promises);
            deferred.resolve({ filename: filename!, size });
        }

        stream.on('data', write);
        stream.on('end', close);
        stream.on('error', e => streams.forEach(x => x.destroy(e)));

        return deferred.promise;
    }

    async passthroughFileStream(url: string, targetRelPath: string[] | undefined, range: string | undefined, filename?: string): Promise<{ stream: ReadableStream; length: number | Promise<number>; range: string | undefined }> {
        const passthroughStream = new PassThrough(),
            fileDownloadContext: {
                headers?: Promise<IncomingHttpHeaders>
            } = {};
        const normalizedUrl = this.getNormalizedCachedFileUrl(url);
        if (targetRelPath != null) {
            const isCache = targetRelPath[0] === this.cacheRelPath;
            if (!isCache || this.cachePromises[normalizedUrl] == null) {
                const cachePromise = this._storeFile(url, normalizedUrl, targetRelPath, range ? undefined : passthroughStream, fileDownloadContext);
                if (isCache) {
                    this.cachePromises[normalizedUrl] = cachePromise;
                    cachePromise.then(entry => this.saveCacheInfo(normalizedUrl, entry.filename, entry.size));
                }
            }
        }
        if (range) {
            if (this.streamHandlers[normalizedUrl] == null) {
                this.streamHandlers[normalizedUrl] = new HttpStreamHandler(this.http, url);
            }
            const streamHandler = this.streamHandlers[normalizedUrl];
            const chunk = await streamHandler.getChunk(range);
            return {
                ...chunk,
                stream: convertNodeReadableToWeb(chunk.stream)
            };
        } else {
            return {
                stream: convertNodeReadableToWeb(passthroughStream),
                length: fileDownloadContext.headers?.then(headers => +(headers['content-length'] || '0')) ?? 0,
                range: undefined
            };
        }
    }
}