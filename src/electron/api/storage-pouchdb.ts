import { AbstractStorage } from '@/electron/api/storage-abstract';
import PouchDB from 'pouchdb';
import { Post } from '@/electron/service/service';
import { Bookmark, BookmarkPost } from '@/electron/api/bookmark-symbols';
import { FileCacheEntry, PostCollection } from '@/electron/api/storage-symbols';
import { createLogger } from '@/logging';
import { Logger } from '@/electron/log/logger';
import _ from 'lodash';
import { DownloadInfo } from '@/electron/api/download-symbols';
import { DownloadStatus } from '@/electron/api/download';

interface PostToCollectionDocument {
    postUrl: string,
    collectionName: string
}

interface PostToBookmarkDocument {
    postUrl: string,
    bookmarkName: string,
    seen: boolean
}

export class PouchDbStorageImpl extends AbstractStorage {
    private postsDb!: PouchDB.Database<Post>;
    private bookmarksDb!: PouchDB.Database<Bookmark>;
    private postToBookmarkDb!: PouchDB.Database<PostToBookmarkDocument>;
    private collectionsDb!: PouchDB.Database<PostCollection>;
    private postToCollectionDb!: PouchDB.Database<PostToCollectionDocument>;
    private readonly logger: Logger;

    constructor() {
        super();
        this.logger = createLogger('PouchDbStorage');
        this.initLocalDb().catch(e => this.logger.error('Failed to initialize DB', e));
    }

    async initLocalDb() {
        try {
            this.postsDb = new PouchDB('posts');
            this.collectionsDb = new PouchDB('collections');
            this.postToCollectionDb = new PouchDB('postToCollection');
            this.bookmarksDb = new PouchDB('bookmarks');
            this.postToBookmarkDb = new PouchDB('postToBookmark');

            if (!await this.getFromLocalDb(this.postToCollectionDb, '_design/index')) {
                // @ts-ignore
                await this.postToCollectionDb?.put({
                    _id: '_design/index',
                    views: {
                        byPost: {
                            map: function (doc: PostToCollectionDocument) {
                                emit(doc.postUrl);
                            }.toString()
                        },
                        byCollection: {
                            map: function (doc: PostToCollectionDocument) {
                                emit(doc.collectionName);
                            }.toString()
                        }
                    }
                });
            }

            if (!await this.getFromLocalDb(this.postToBookmarkDb, '_design/index')) {
                // @ts-ignore
                await this.postToBookmarkDb?.put({
                    _id: '_design/index',
                    views: {
                        byPost: {
                            map: function (doc: PostToBookmarkDocument) {
                                emit(doc.postUrl);
                            }.toString()
                        },
                        byBookmark: {
                            map: function (doc: PostToBookmarkDocument) {
                                emit(doc.bookmarkName);
                            }.toString()
                        },
                        byBookmarkUnseen: {
                            map: function (doc: PostToBookmarkDocument) {
                                if (!doc.seen) {
                                    emit(doc.bookmarkName);
                                } else {
                                    emit('');
                                }
                            }.toString()
                        }
                    }
                });
            }

            this.initDeferred.resolve();
        }
        catch (e) {
            this.logger.error(`Failed to init local database`, e);
            this.initDeferred.reject(e);
        }
    }

    async queryLocalDb<T>(db: PouchDB.Database<T>, indexName: string, key: any): Promise<T[]> {
        const rows = (await db.query(`index/${indexName}`, {
            key: key,
            include_docs: true
        })).rows;
        // @ts-ignore
        return rows.map((row: { doc: T }) => row.doc);
    }

    async getAllDocs<T>(db: PouchDB.Database<T>): Promise<T[]> {
        // @ts-ignore
        return (await db.allDocs({
            include_docs: true
        })).rows.map(row => row.doc);
    }

    async getFromLocalDb<T>(db: PouchDB.Database<T>, key: any): Promise<T | null> {
        try {
            return await db.get(key);
        }
        catch (e) {
            return null;
        }
    }

    async storePost(post: Post, updateIfPresent?: boolean): Promise<void> {
        post = _.cloneDeep(post);
        for (const [key, prop] of Object.entries(Object.getOwnPropertyDescriptors(post))) {
            if (key.startsWith('_')) {
                // @ts-ignore
                delete post[key];
            }
        }
        const prevPost = await this.getFromLocalDb<Post>(this.postsDb, post.url) || {};
        await this.postsDb.put({
            _id: post.url,
            ...prevPost,
            ...post
        });
    }

    async addPostToCollection(collectionName: string, post: Post): Promise<void> {
        await this.postToCollectionDb.put({
            _id: this.getPostToCollectionKey(collectionName, post.url),
            postUrl: post.url,
            collectionName: collectionName
        });
    }

    async removePostFromCollection(collectionName: string, post: Post): Promise<void> {
        const doc = await this.postToCollectionDb.get(this.getPostToCollectionKey(collectionName, post.url));
        await this.postToCollectionDb.remove(doc);
    }

    getCollections(): Promise<Array<PostCollection>> {
        return this.getAllDocs(this.collectionsDb);
    }

    getCollectionByName(name: string): Promise<PostCollection> {
        return this.collectionsDb.get(name);
    }

    async createCollection(collection: PostCollection): Promise<void> {
        await this.collectionsDb.put({
            _id: collection.name,
            ...collection
        });
    }

    async removeCollection(name: string): Promise<void> {
        const collection = await this.collectionsDb.get(name);
        await this.collectionsDb.remove(collection);
    }

    async modifyCollection(collection: PostCollection): Promise<void> {
        const prevCollection = await this.collectionsDb.get(collection.name) || {};
        await this.collectionsDb.put({
            // @ts-ignore
            _id: collection.name,
            ...prevCollection,
            ...collection
        });
    }

    async getPostsInCollection(collectionName: string, query: string | null, offset: number, limit: number): Promise<Array<Post>> {
        const docs = await this.queryLocalDb<PostToCollectionDocument>(this.postToCollectionDb, 'byCollection', collectionName);
        // TODO: add query support
        return await Promise.all(docs.map(doc => this.postsDb.get(doc.postUrl)));
    }

    async getPostCollections(postUrl: string): Promise<Array<string>> {
        const docs = await this.queryLocalDb<PostToCollectionDocument>(this.postToCollectionDb, 'byPost', postUrl);
        return docs.map(doc => doc.collectionName);
    }

    getBookmarks(): Promise<Array<Bookmark>> {
        return this.getAllDocs(this.bookmarksDb);
    }

    async createBookmark(bookmark: Bookmark, seenPosts: Array<Post>): Promise<void> {
        await this.bookmarksDb.put({
            _id: bookmark.name,
            ...bookmark
        });
        await this.addPostsToBookmark(bookmark.name, seenPosts, true);
    }

    async removeBookmark(name: string): Promise<void> {
        const bookmark = await this.bookmarksDb.get(name);
        await this.bookmarksDb.remove(bookmark);
    }

    async modifyBookmark(bookmark: Bookmark): Promise<void> {
        const prevBookmark = await this.bookmarksDb.get(bookmark.name) || {};
        await this.bookmarksDb.put({
            // @ts-ignore
            _id: bookmark.name,
            ...prevBookmark,
            ...bookmark
        });
    }

    async addPostsToBookmark(bookmarkName: string, posts: Array<Post>, seen: boolean = false): Promise<void> {
        await Promise.all(posts.map(async post => {
            await this.storePost(post);
            const id = this.getPostToBookmarkKey(bookmarkName, post.url);
            const prevDoc = await this.getFromLocalDb(this.postToBookmarkDb!, id);
            if (!prevDoc) {
                await this.postToBookmarkDb!.put({
                    _id: id,
                    postUrl: post.url,
                    bookmarkName,
                    seen
                });
            }
        }));
    }

    async getPostsInBookmark(bookmarkName: string, offset: number, limit: number): Promise<Array<Post & BookmarkPost>> {
        const docs = await this.queryLocalDb<PostToBookmarkDocument>(this.postToBookmarkDb, 'byBookmark', bookmarkName);
        return Promise.all(docs.map(async doc => {
            return {
                ...await this.postsDb!.get<Post>(doc.postUrl),
                seen: doc.seen
            };
        }));
    }

    async getUnseenPostsCountInBookmark(bookmarkName: string): Promise<number> {
        const result = await this.postToBookmarkDb.query('index/byBookmarkUnseen', {
            key: bookmarkName
        });
        return result.rows.length;
    }

    async getBookmarkPostSeen(bookmarkName: string, postUrl: string): Promise<boolean> {
        const doc = await this.postToBookmarkDb.get(this.getPostToBookmarkKey(bookmarkName, postUrl));
        return doc.seen;
    }

    markAllPostsForBookmarkAsSeen(bookmarkName: string): Promise<void> {
        throw new Error('Not supported');
    }

    async markPostForBookmarkAsSeen(bookmarkName: string, postUrl: string): Promise<void> {
        const doc = await this.postToBookmarkDb.get(this.getPostToBookmarkKey(bookmarkName, postUrl));
        await this.postToBookmarkDb.put({
            ...doc,
            seen: true
        });
    }

    getActiveDownloads(): Promise<DownloadInfo[]> {
        throw new Error('Not supported');
    }

    saveDownload(download: DownloadInfo): Promise<void> {
        throw new Error('Not supported');
    }

    getPostsDownloadStatuses(postUrls: string[]): Promise<Array<DownloadStatus | null>> {
        throw new Error('Not supported');
    }

    getCachedFile(url: string): Promise<string | null> {
        throw new Error('Not supported');
    }

    deleteFileCache(url: string): Promise<void> {
        throw new Error('Not supported');
    }

    createFileCache(url: string, localPath: string, size: number): Promise<void> {
        throw new Error('Not supported');
    }

    getCacheEntries(): Promise<FileCacheEntry[]> {
        throw new Error('Not supported');
    }
}