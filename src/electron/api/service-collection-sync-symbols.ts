export const IPC_EVENT_COLLECTION_SYNC_STARTED = 'collection-sync-started';
export const IPC_EVENT_COLLECTION_SYNC_FINISHED = 'collection-sync-finished';