import { Post } from '@/electron/service/service';
import { FileCacheEntry, PostCollection } from '@/electron/api/storage-symbols';
import { Bookmark, BookmarkPost } from '@/electron/api/bookmark-symbols';
import { DownloadInfo } from '@/electron/api/download-symbols';
import { DownloadStatus } from '@/electron/api/download';
import { Deferred } from '@/electron/util/deferred';

export abstract class AbstractStorage {
    fileStorageSupported: boolean = false;
    downloadsSupported: boolean = false;

    initDeferred: Deferred<void>;

    constructor() {
        this.initDeferred = new Deferred();
    }

    abstract storePost(post: Post, updateIfPresent?: boolean): Promise<void>;

    abstract addPostToCollection(collectionName: string, post: Post): Promise<void>;
    abstract removePostFromCollection(collectionName: string, post: Post): Promise<void>;
    abstract getCollections(): Promise<Array<PostCollection>>;
    abstract getCollectionByName(name: string): Promise<PostCollection>;
    abstract createCollection(collection: PostCollection): Promise<void>;
    abstract removeCollection(name: string): Promise<void>;
    abstract modifyCollection(collection: PostCollection): Promise<void>;
    abstract getPostsInCollection(collectionName: string, query: string | null, offset: number, limit: number): Promise<Array<Post>>;
    abstract getPostCollections(postUrl: string): Promise<Array<string>>;

    abstract getBookmarks(): Promise<Array<Bookmark>>;
    abstract createBookmark(bookmark: Bookmark, seenPosts: Array<Post>): Promise<void>;
    abstract removeBookmark(name: string): Promise<void>;
    abstract modifyBookmark(bookmark: Bookmark): Promise<void>;
    abstract addPostsToBookmark(bookmarkName: string, posts: Array<Post>, seen?: boolean): Promise<void>;
    abstract getPostsInBookmark(bookmarkName: string, offset: number, limit: number): Promise<Array<Post & BookmarkPost>>;
    abstract getUnseenPostsCountInBookmark(bookmarkName: string): Promise<number>;
    abstract getBookmarkPostSeen(bookmarkName: string, postUrl: string): Promise<boolean>;
    abstract markAllPostsForBookmarkAsSeen(bookmarkName: string): Promise<void>;
    abstract markPostForBookmarkAsSeen(bookmarkName: string, postUrl: string): Promise<void>;

    abstract getActiveDownloads(): Promise<DownloadInfo[]>;
    abstract saveDownload(download: DownloadInfo): Promise<void>;
    abstract getPostsDownloadStatuses(postUrls: string[]): Promise<Array<DownloadStatus | null>>;

    abstract getCachedFile(url: string): Promise<string | null>;
    abstract deleteFileCache(url: string): Promise<void>;
    abstract createFileCache(url: string, localPath: string, size: number): Promise<void>;
    abstract getCacheEntries(): Promise<FileCacheEntry[]>;

    getPostToCollectionKey(collectionName: string, postUrl: string) {
        return `${collectionName}-${postUrl}`;
    }

    getPostToBookmarkKey(bookmarkName: string, postUrl: string) {
        return `${bookmarkName}-${postUrl}`;
    }
}