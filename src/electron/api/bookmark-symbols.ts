export const IPC_EVENT_BOOKMARK_CREATED = 'bookmark-created';
export const IPC_EVENT_BOOKMARK_REMOVED = 'bookmark-removed';
export const IPC_EVENT_BOOKMARK_MODIFIED = 'bookmark-modified';
export const IPC_EVENT_BOOKMARK_UPDATE_STARTED = 'bookmark-update-started';
export const IPC_EVENT_BOOKMARK_UPDATE_PROGRESS = 'bookmark-update-progress';
export const IPC_EVENT_BOOKMARK_UPDATE_FINISHED = 'bookmark-update-finished';
export const IPC_EVENT_BOOKMARK_UNSEEN_CLEAR = 'bookmark-unseen-clear';
export const IPC_EVENT_BOOKMARK_UNSEEN_UPDATED = 'bookmark-unseen-updated';
export const IPC_EVENT_BOOKMARK_POST_SEEN = 'bookmark-post-seen';

export interface Bookmark {
    name: string,
    path: string,
    monitor: boolean,
    download: boolean,
    createdAt?: Date
}

export interface BookmarkPost {
    seen: boolean
}