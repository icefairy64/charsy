import { ipcMain, BrowserWindow, screen } from 'electron';
import { WINDOW_OPEN_POST } from './window-management-symbols';
import { Post } from '../service/service';
import { SCHEMA_CACHE_PASSTHROUGH } from './storage-symbols';

export class WindowManagementApi {
    registerIpcHandlers() {
        // @ts-ignore
        ipcMain.handle(WINDOW_OPEN_POST, (event, ...args) => this.openWindowForPost(...args));
    }

    openWindowForPost(post: Post, forceNewWindow: boolean) {
        if (post.width == null || post.height == null) {
            throw new Error('Post has no sizes');
        }
        const { width: screenWidth, height: screenHeight } = screen.getPrimaryDisplay().workArea;
        const scale = Math.min(screenWidth / post.width, screenHeight / post.height);
        const wnd = new BrowserWindow({
            width: Math.floor(post.width * scale),
            height: Math.floor(post.height * scale),
            useContentSize: true
        });
        const url = SCHEMA_CACHE_PASSTHROUGH + post.fileUrl.substring(4);
        wnd.loadURL(post.fileUrl);
    }
}