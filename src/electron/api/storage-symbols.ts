export const IPC_COLLECTION_LIST = 'collection-list';
export const IPC_COLLECTION_CREATE = 'collection-create';
export const IPC_COLLECTION_MODIFY = 'collection-modify';
export const IPC_COLLECTION_REMOVE = 'collection-remove';
export const IPC_COLLECTION_ADD_POST = 'collection-add-post';
export const IPC_COLLECTION_REMOVE_POST = 'collection-remove-post';
export const IPC_COLLECTION_GET_POSTS = 'collection-get-posts';
export const IPC_COLLECTION_LIST_FOR_POST = 'collection-list-for-post';

export const IPC_BOOKMARK_LIST = 'bookmark-list';
export const IPC_BOOKMARK_CREATE = 'bookmark-create';
export const IPC_BOOKMARK_REMOVE = 'bookmark-remove';
export const IPC_BOOKMARK_MODIFY = 'bookmark-modify';
export const IPC_BOOKMARK_UNSEEN_POST_COUNT = 'bookmark-unseen-post-count';
export const IPC_BOOKMARK_POST_LIST = 'bookmark-post-list';
export const IPC_BOOKMARK_POST_IS_SEEN = 'bookmark-post-is-seen';
export const IPC_BOOKMARK_MARK_ALL_POSTS_AS_SEEN = 'bookmark-mark-all-posts-as-seen';
export const IPC_BOOKMARK_MARK_POST_AS_SEEN = 'bookmark-mark-post-as-seen';

export const IPC_EVENT_COLLECTION_CREATED = 'collection-created';
export const IPC_EVENT_COLLECTION_REMOVED = 'collection-removed';
export const IPC_EVENT_COLLECTION_MODIFIED = 'collection-modified';
export const IPC_EVENT_POST_ADDED_TO_COLLECTION = 'post-added-to-collection';
export const IPC_EVENT_POST_REMOVED_FROM_COLLECTION = 'post-removed-from-collection';

export const SCHEMA_CACHE_PASSTHROUGH = 'storage-cache-passthrough';

export interface PostCollection {
    name: string,
    description?: string,
    download: boolean,
    syncDescriptor?: PostCollectionSyncDescriptor,
    syncEnabled: boolean,
    createdAt?: Date
}

export interface PostCollectionSyncDescriptor {
    serviceName: string,
    collectionId: string,
    collectionName: string
}

export interface FileCacheEntry {
    url: string,
    localPath: string,
    size?: number,
    createdAt?: Date
}