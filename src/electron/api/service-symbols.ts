import { ServiceDescriptor } from '@/electron/service/service';
import { ConfigPropertyDescription } from '@/electron/config/config';

export const IPC_SERVICE_PAGE = 'service-page';
export const IPC_SERVICE_LIST = 'services';
export const IPC_SERVICE_SEARCH_PATH = 'service-search-path';
export const IPC_SERVICE_SEARCH_AUTOCOMPLETE = 'service-search-autocomplete';
export const IPC_SERVICE_HOME_SCREEN = 'service-home-screen';
export const IPC_SERVICE_HOME_SCREEN_SUPPORTED = 'service-home-screen-supported';
export const IPC_SERVICE_AUTH_USER_INFO = 'service-user-info';
export const IPC_SERVICE_AUTHENTICATE = 'service-authenticate';
export const IPC_SERVICE_AUTH_SUPPORTED = 'service-auth-supported';
export const IPC_SERVICE_DESCRIPTOR_CONFIG_LIST = 'descriptor-configs';

export const IPC_EVENT_SERVICE_LIST_UPDATE = 'service-list-update';

export interface ServiceDescription {
    name: string,
    type: string,
    title: string
}

export interface ServiceUserInfo {
    name: string
}

export type ServiceDescriptorConfigInfo<T extends ServiceDescriptor = ServiceDescriptor> = {
    discriminator: string
    descriptions: Record<string, ConfigPropertyDescription<Partial<Omit<T, 'serviceName'>>>>
}