import { PostRating, ServiceHomeScreenItemOptions } from '@/electron/service/service';
import { LogLevel } from '@/electron/log/logger';

export interface Preferences {
    gallery: {
        hiResThumbnails: boolean,
        ratingFilter?: PostRating
    },
    home: {
        items: ServiceHomeScreenItemOptions[]
    },
    logging?: {
        threshold?: LogLevel
    },
    fileCache?: {
        storageLimit?: number
    }
}

export const IPC_PREFS_GET_CURRENT = 'prefs-get-current';
export const IPC_PREFS_UPDATE = 'prefs-update';

export const IPC_EVENT_PREFS_UPDATED = 'prefs-updated';