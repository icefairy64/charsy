import { Logger, LogLevel } from '@/electron/log/logger';
import { createLogger } from '@/logging';
import { registerIpcHandler } from '@/util/remote-util';
import { IPC_LOG_REPORT } from '@/electron/api/logging-symbols';

export class LoggingApi {
    private readonly logger: Logger;

    constructor() {
        this.logger = createLogger('RemoteLog');
    }

    registerIpcHandlers() {
        registerIpcHandler(IPC_LOG_REPORT, this.onLog.bind(this));
    }

    onLog(level: LogLevel, timestamp: number, message: string, error?: Error) {
        this.logger.log(level, message, error, timestamp);
    }
}