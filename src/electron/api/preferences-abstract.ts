import { Preferences } from '@/electron/api/preferences-symbols';

export abstract class AbstractPreferencesStorage {
    onPrefsChanged?: (prefs: Preferences) => void

    abstract getCurrentPrefs(): Preferences | undefined;
    abstract updatePrefs(preferences: Preferences): void;
}