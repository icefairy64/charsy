import OmniboxApi from '@/electron/api/omnibox';
import { Logger } from '@/electron/log/logger';
import { StorageApi } from '@/electron/api/storage';
import { ServiceApi } from '@/electron/api/service';
import { ServiceHomeScreenItem } from '@/electron/service/service';
import { createLogger } from '@/logging';
import { registerIpcHandler } from '@/util/remote-util';
import { IPC_SERVICE_HOME_SCREEN } from '@/electron/api/service-symbols';
import { PreferencesApi } from '@/electron/api/preferences';
import { Preferences } from '@/electron/api/preferences-symbols';
import _ from 'lodash';

export class ScreenApi {
    private logger: Logger;

    private preferences: PreferencesApi;
    private omnibox: OmniboxApi;
    private storage: StorageApi;
    private service: ServiceApi;

    private homeScreenCache: Record<string, ServiceHomeScreenItem[]> = {};

    constructor(preferences: PreferencesApi, omnibox: OmniboxApi, storage: StorageApi, service: ServiceApi) {
        this.preferences = preferences;
        this.omnibox = omnibox;
        this.storage = storage;
        this.service = service;

        this.logger = createLogger('Screens');

        this.preferences.addUpdateListener(this.onPreferencesUpdate.bind(this));

        setInterval(() => this.clearHomeScreenCache(), 30 * 60 * 1000);
    }

    registerIpcHandlers() {
        registerIpcHandler(IPC_SERVICE_HOME_SCREEN, this.onServiceHomeScreen.bind(this));
    }

    clearHomeScreenCache() {
        this.logger.info('Cleaning up home screen cache');
        this.homeScreenCache = {};
    }

    onPreferencesUpdate(prefs: Preferences, previousPrefs: Preferences | undefined) {
        if (!previousPrefs || !_.isEqual(prefs.home, previousPrefs.home)) {
            this.clearHomeScreenCache();
        }
    }

    async onServiceHomeScreen(path: string): Promise<ServiceHomeScreenItem[] | null> {
        if (this.homeScreenCache[path]) {
            this.logger.info(`Serving home screen items for path ${path} from cache`);
            return this.homeScreenCache[path];
        }

        let items;

        if (path === '/home') {
            items = await this.onGlobalHomeScreen();
        } else if (path.startsWith('/omnibox/')) {
            items = await this.onOmniboxSearchScreen(path.substring('/omnibox/'.length));
        } else {
            items = await this.service.onServiceHomeScreen(path);
        }

        if (items) {
            this.homeScreenCache[path] = items;
        }

        return items || null;
    }

    async onGlobalHomeScreen(): Promise<ServiceHomeScreenItem[]> {
        const itemOptions = this.preferences.getCurrentPrefs()?.home.items || [],
            promises = itemOptions.map(async item => {
                return {
                    title: item.title,
                    galleryRelPath: await this.service.getServicePathByDescriptor(item.descriptor)
                };
            });
        return await Promise.all(promises);
    }

    async onOmniboxSearchScreen(query: string): Promise<ServiceHomeScreenItem[]> {
        const terms = query.split(' '),
            results = await this.omnibox.query(terms.length > 1 ? [terms[0]] : [], (terms.length > 1 ? terms.slice(1) : terms).join(' ')),
            hydratedResults = await Promise.all(results.map(async r => {
                if (r.resultType === 'SERVICE_PAGE') {
                    return {
                        result: r
                    };
                } else if (r.resultType === 'COLLECTION_SEARCH') {
                    return {
                        result: {
                            ...r,
                            pagePath: `/collection/${r.collectionName}/search:${r.query}`
                        }
                    };
                }
            }));
        return hydratedResults.map(r => r && ({
            title: `${r.result.resultName} (${r.result.contextName})`,
            galleryRelPath: r.result.pagePath
        })).filter(x => x != null) as ServiceHomeScreenItem[];
    }
}