import { joinPath, splitPath } from '../util/path-util';
import {
    Page,
    SearchAutocompleteSuggestion,
    Service,
    ServiceBaseOptions,
    ServiceDescriptor,
    ServiceHomeScreenItem
} from '../service/service';
import { DanbooruService } from '../service/danbooru';
import {
    IPC_EVENT_SERVICE_LIST_UPDATE,
    IPC_SERVICE_AUTH_SUPPORTED,
    IPC_SERVICE_AUTH_USER_INFO,
    IPC_SERVICE_AUTHENTICATE,
    IPC_SERVICE_DESCRIPTOR_CONFIG_LIST,
    IPC_SERVICE_HOME_SCREEN_SUPPORTED,
    IPC_SERVICE_LIST,
    IPC_SERVICE_PAGE,
    IPC_SERVICE_SEARCH_AUTOCOMPLETE,
    IPC_SERVICE_SEARCH_PATH,
    ServiceDescription,
    ServiceDescriptorConfigInfo
} from './service-symbols';
import { E621Service } from '../service/e621';
import { SankakuV2Service } from '@/electron/service/sankaku-v2';
import { OptionsObject } from '@/electron/options/options';
import { debounceTime, filter, startWith } from 'rxjs/operators';
import { BehoimiService } from '@/electron/service/3dbooru';
import { Subject } from 'rxjs';
import { registerIpcHandler } from '@/util/remote-util';
import _ from 'lodash';
import { RedditService } from '@/electron/service/reddit';
import OmniboxApi from '@/electron/api/omnibox';
import { PreferencesApi } from '@/electron/api/preferences';
import { createLogger } from '@/logging';
import { Logger } from '@/electron/log/logger';
import { DerpibooruService } from '@/electron/service/derpibooru';
import { KonachanService } from '@/electron/service/konachan';
import { OptionsFileHandle } from '@/electron/options/options-file';
import { RedGifsService } from '@/electron/service/redgifs';
import { broadcastClientEvent } from '@/electron/util/electron';
import { BooruPlusService } from '@/electron/service/booruplus';

export interface ServiceConfiguration {
    [key: string]: {
        type: string,
        title: string,
        options: ServiceBaseOptions & Record<string, any>
    }
}

interface ServicePageCacheEntry {
    timestamp: Date,
    content: Page
}

async function createService(logger: Logger, type: string, options: OptionsObject<any>): Service<any> {
    logger.info(`Creating service ${type} ${JSON.stringify(options.currentValue)}`);
    switch (type) {
        case 'danbooru': return new DanbooruService(options);
        case 'e621': return new E621Service(options);
        case 'sankaku-v2': return new SankakuV2Service(options);
        case '3dbooru': return new BehoimiService(options);
        case 'deviantart': {
            if (!options.getChild('oauth2')) {
                const clientId = process.env.DEVIANTART_CLIENT_ID,
                    clientSecret = process.env.DEVIANTART_CLIENT_SECRET
                logger.info('[DeviantArt] Using client credentials from environment');
                if (clientId && clientSecret) {
                    options.setChildValue('oauth2', {
                        clientId,
                        clientSecret
                    });
                }
            }
            const { DeviantArtService } = await import('@/electron/service/deviantart');
            return new DeviantArtService(options);
        }
        case 'reddit': {
            if (!options.getChild('oauth2')) {
                const clientId = process.env.REDDIT_CLIENT_ID
                logger.info('[Reddit] Using client credentials from environment');
                if (clientId) {
                    options.setChildValue('oauth2', {
                        clientId
                    });
                }
            }
            return new RedditService(options);
        }
        case 'derpibooru': return new DerpibooruService(options);
        case 'konachan': return new KonachanService(options);
        case 'redgifs': return new RedGifsService(options);
        case 'booruplus': return new BooruPlusService(options);
        default: throw new Error(`Unsupported service type ${type}`);
    }
}

export class ServiceApi {
    services: {
        [key: string]: Service<any>
    };

    serviceSubject: Subject<Service<any>[]>;

    private serviceConfigurationHandle?: OptionsFileHandle<ServiceConfiguration>;
    private serviceConfigurationOptions?: OptionsObject<ServiceConfiguration>;
    private omnibox: OmniboxApi;
    private prefrerences: PreferencesApi;
    private logger: Logger;

    private readonly pageCache: Record<string, ServicePageCacheEntry> = {};

    constructor(configPath: string | ServiceConfiguration, omnibox: OmniboxApi, preferences: PreferencesApi) {
        this.omnibox = omnibox;
        this.prefrerences = preferences;
        this.logger = createLogger('Services');
        if (_.isString(configPath)) {
            import('@/electron/options/options-file').then(optionsModule => {
                this.serviceConfigurationHandle = new optionsModule.OptionsFileHandle<ServiceConfiguration>(configPath);
                const options = this.serviceConfigurationHandle.options;
                this.serviceConfigurationHandle.loadFromFile().catch(e => this.logger.error('Failed to load configuration', e));
                this.onOptionsInit(options);
            })
        } else {
            const options = new OptionsObject<ServiceConfiguration>(configPath);
            this.onOptionsInit(options);
        }
        this.services = {};
        this.serviceSubject = new Subject<Service<any>[]>();

        setInterval(this.clearCache.bind(this), 60000);
    }

    onOptionsInit(options: OptionsObject<ServiceConfiguration>) {
        this.serviceConfigurationOptions = options;

        options.getValueObservable()
            .pipe(
                filter(config => config != null),
                debounceTime(100)
            )
            .subscribe(async services => {
                this.logger.info('Config updated');

                for (const key in services) {
                    this.logger.debug(`Handling service ${key}`);
                    const serviceConfig = services[key];

                    if (!this.services[key]) {
                        // @ts-ignore
                        this.services[key] = await createService(this.logger, serviceConfig.type, options.getChild(key)?.getChild('options'));
                        await this.services[key].init();
                        this.services[key].registerOmnibox(this.omnibox, key);
                    }
                }

                for (const key in this.services) {
                    if (this.services[key] && !services[key]) {
                        this.logger.info(`Destroying service ${key}`);
                        await this.services[key].destroy();
                        delete this.services[key];
                    }
                }

                this.serviceSubject.next(Object.values(this.services));

                broadcastClientEvent(IPC_EVENT_SERVICE_LIST_UPDATE, await this.getServiceList());
            });
    }

    async getService(name: string, parent?: Service<any>): Promise<Service<any> | null> {
        if (parent == null) {
            return this.services[name];
        }
        return parent.getSubservice(name);
    }

    registerIpcHandlers() {
        registerIpcHandler(IPC_SERVICE_PAGE, this.getServicePage.bind(this));
        registerIpcHandler(IPC_SERVICE_LIST, this.onServiceList.bind(this));
        registerIpcHandler(IPC_SERVICE_SEARCH_PATH, this.onServiceSearchPath.bind(this));
        registerIpcHandler(IPC_SERVICE_SEARCH_AUTOCOMPLETE, this.onServiceSearchAutocomplete.bind(this));
        registerIpcHandler(IPC_SERVICE_HOME_SCREEN_SUPPORTED, this.onServiceHomeScreenSupported.bind(this));
        registerIpcHandler(IPC_SERVICE_AUTH_USER_INFO, this.onServiceUserInfo.bind(this));
        registerIpcHandler(IPC_SERVICE_AUTHENTICATE, this.onServiceAuthenticate.bind(this));
        registerIpcHandler(IPC_SERVICE_AUTH_SUPPORTED, this.onServiceAuthSupported.bind(this));
        registerIpcHandler(IPC_SERVICE_DESCRIPTOR_CONFIG_LIST, this.onServiceDescriptorConfigList.bind(this));
    }

    getServiceObservable() {
        return this.serviceSubject
            .pipe(
                startWith(...Object.values(this.services))
            );
    }

    private async traverseServicePath<T>(path: string, callback: (service: Service<any>, parentPath: string, relPath: string) => Promise<T | undefined> | T | undefined): Promise<T | undefined> {
        let service: Service<any> | undefined | null = undefined;
        let parentPath = '/';
        do {
            let [serviceName, servicePath] = splitPath(path);
            if (serviceName.includes(':')) {
                [serviceName, servicePath] = [path.substring(1), '/'];
            }
            parentPath = joinPath(parentPath, serviceName);
            service = await this.getService(serviceName, service);
            if (service != null) {
                path = servicePath;
                if (servicePath == null || servicePath === '' || servicePath === '/') {
                    path = '/';
                }
                const callbackResult = await callback(service, parentPath, path);
                if (callbackResult !== undefined) {
                    return callbackResult;
                }
            }
        } while (service != null);
        return;
    }

    async getServicePage(path: string | ServiceDescriptor, page?: number | string, allowCache?: boolean): Promise<Page> {
        if (!_.isString(path)) {
            path = await this.getServicePathByDescriptor(path, page);
        }
        const cacheKey = `${path}:::${page || 'null'}`,
            cachedData = this.pageCache[cacheKey];
        if (allowCache && cachedData) {
            this.logger.debug(`Serving cached data for path ${path}`);
            return cachedData.content;
        }
        const pageData = await this.traverseServicePath(path, async (service, parentPath, relPath) => {
            if (relPath === '/') {
                const pageData = await service.getPage(page);
                pageData.content = service.filterPageContent(pageData.content);
                return pageData;
            }
        });
        if (pageData) {
            this.pageCache[cacheKey] = {
                timestamp: new Date(),
                content: pageData
            };
            return pageData;
        }
        throw new Error(`Unsupported path ${path}`);
    }

    async getServicePathByDescriptor(descriptor: string | ServiceDescriptor, page?: number | string): Promise<string> {
        if (_.isString(descriptor)) {
            return descriptor;
        }
        const service = this.services[descriptor.serviceName];
        if (!service) {
            throw new Error(`Unknown service ${descriptor.serviceName}`);
        }
        const path = await service.getSubServicePathByDescriptor(descriptor);
        if (path) {
            return `${descriptor.serviceName}/${path}`;
        } else {
            throw new Error(`Unsupported service descriptor ${JSON.stringify(descriptor.serviceName)}`);
        }
    }

    private async onServiceList(...args: any[]) {
        return this.getServiceList();
    }

    async getServiceList(): Promise<Array<ServiceDescription>> {
        const result: Array<ServiceDescription> = [];
        for (const serviceName in this.services) {
            const service = this.services[serviceName];
            const serviceConfig = this.serviceConfigurationOptions?.currentValue?.[serviceName];
            if (!serviceConfig) {
                throw new Error(`No config present for service ${serviceName}`);
            }
            result.push({
                name: serviceName,
                type: service.name,
                title: serviceConfig.title
            });
        }
        return result;
    }

    async onServiceSearchPath(path: string, query: string): Promise<string | null> {
        let lastSearchSupportingServicePath: string | null = null;
        const searchServiceName = `search:${query}`;
        const result = await this.traverseServicePath(path, async (service, parentPath, relPath) => {
            if (service.capabilities.includes('SEARCH') && await service.getSubservice(searchServiceName) != null) {
                lastSearchSupportingServicePath = joinPath(parentPath, searchServiceName);
            }
            if (relPath === '/') {
                return lastSearchSupportingServicePath;
            }
        });
        if (result) {
            return result;
        }
        throw new Error(`Unsupported path ${path}`);
    }

    async onServiceSearchAutocomplete(path: string, query: string): Promise<SearchAutocompleteSuggestion[] | null> {
        const suggestions = await this.traverseServicePath(path, async (service, parentPath, relPath) => {
            return await service.getAutocompleteSuggestions(query) || undefined;
        });
        if (suggestions) {
            return suggestions;
        }
        this.logger.warn(`Unsupported autocomplete path: ${path}`);
        return null;
    }

    async onServiceHomeScreen(path: string): Promise<ServiceHomeScreenItem[] | null> {
        const items = await this.traverseServicePath(path, async service => {
            if (service.capabilities.includes('HOME_SCREEN')) {
                const items = await service.getHomeScreenItems() || undefined;
                if (items) {
                    items.forEach(item => {
                        item.items = item.items && service.filterPageContent(item.items);
                    });
                }
                return items;
            }
        });
        return items || null;
    }

    async onServiceHomeScreenSupported(path: string) {
        const result = await this.traverseServicePath(path, async service => {
            if (service.capabilities.includes('HOME_SCREEN')) {
                return true;
            }
        });
        return result || false;
    }

    async onServiceUserInfo(path: string) {
        const service = await this.traverseServicePath(path, async service => {
            if (service.isAuthSupported()) {
                return service;
            }
        });
        return service?.getAuthenticatedUserInfo();
    }

    async onServiceAuthenticate(path: string, credentials: any) {
        const service = await this.traverseServicePath(path, async service => {
            if (service.isAuthSupported()) {
                return service;
            }
        });
        if (!service) {
            throw new Error(`Unsupported path ${path}`);
        }
        return service.authenticate(credentials);
    }

    async onServiceAuthSupported(path: string) {
        const service = await this.traverseServicePath(path, async service => {
            if (service.isAuthSupported()) {
                return service;
            }
        });
        return !!service;
    }

    onServiceDescriptorConfigList(): Record<string, ServiceDescriptorConfigInfo | undefined> {
        return _.mapValues(this.services, s => s.getDescriptorConfigDescriptions());
    }

    clearCache() {
        const keys = Object.keys(this.pageCache),
            now = Date.now();
        for (const key of keys) {
            if (this.pageCache[key].timestamp.getTime() + 30 * 60 * 1000 <= now) {
                delete this.pageCache[key];
            }
        }
    }
}