import { AbstractPreferencesStorage } from '@/electron/api/preferences-abstract';
import { Preferences } from '@/electron/api/preferences-symbols';

export class BrowserPreferencesStorage extends AbstractPreferencesStorage {
    preferences: Preferences;

    constructor(preferences: Preferences) {
        super();
        this.preferences = preferences;
    }

    getCurrentPrefs() {
        return this.preferences;
    }

    updatePrefs(preferences: Preferences) {
        this.preferences = preferences;
    }
}