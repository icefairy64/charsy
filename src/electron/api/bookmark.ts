import { StorageApi } from './storage';
import {
    Bookmark,
    IPC_EVENT_BOOKMARK_UPDATE_FINISHED,
    IPC_EVENT_BOOKMARK_UPDATE_PROGRESS,
    IPC_EVENT_BOOKMARK_UPDATE_STARTED
} from './bookmark-symbols';
import { ServiceApi } from './service';
import { Page, Post } from '../service/service';
import { broadcastClientEvent } from '../util/electron';
import { Observable, Subject } from "rxjs";
import { debounceTime } from 'rxjs/operators';
import _ from 'lodash';
import { createLogger } from '@/logging';
import { Logger } from '@/electron/log/logger';

const BOOKMARK_UPDATE_INTERVAL = 5 * 1000 * 60;

export interface BookmarkNewPostInfo {
    bookmark: Bookmark,
    post: Post
}

export class BookmarkApi {
    private storage: StorageApi;
    private service: ServiceApi;

    private newPostSubject: Subject<BookmarkNewPostInfo>;
    private updating: boolean = false;
    private logger: Logger;

    constructor(storage: StorageApi, service: ServiceApi) {
        this.storage = storage;
        this.service = service;
        this.newPostSubject = new Subject<BookmarkNewPostInfo>();
        this.logger = createLogger('Bookmarks');
        this.service.getServiceObservable()
            .pipe(
                debounceTime(1000)
            )
            .subscribe(() => this.updateBookmarks()
                .catch(e => this.logger.error(`Failed to update bookmarks`, e)));
    }

    registerIpcHandlers() {

    }

    getNewPostObservable(): Observable<BookmarkNewPostInfo> {
        return this.newPostSubject;
    }

    async updateBookmarks() {
        if (this.updating) {
            return;
        }
        try {
            this.updating = true;
            this.logger.info(`Starting update`);
            const bookmarks = await this.storage.getBookmarks();
            this.logger.info(`Updating ${bookmarks.length} bookmarks`);
            for (const bookmark of bookmarks.filter(b => b.monitor)) {
                try {
                    this.logger.info(`Updating bookmark ${bookmark.name}`);
                    await this.updateBookmark(bookmark);
                    this.logger.info(`Finished updating bookmark ${bookmark.name}`);
                }
                catch (e) {
                    this.logger.error(`Failed to update bookmark ${bookmark.name}`, e);
                }
            }
            this.logger.info(`Update finished`);
        }
        catch (e) {
            this.logger.error(`Failed to update`, e);
        }
        finally {
            this.updating = false;
            global.setTimeout(() => this.updateBookmarks(), BOOKMARK_UPDATE_INTERVAL);
        }
    }

    async updateBookmark(bookmark: Bookmark) {
        const lastSeenPosts = await this.storage.getPostsInBookmark(bookmark.name, 0, 20);
        const newPosts: Array<Post> = [];
        let page: number | string | undefined = undefined;
        broadcastClientEvent(IPC_EVENT_BOOKMARK_UPDATE_STARTED, bookmark.name);
        do {
            const pageData: Page = await this.service.getServicePage(bookmark.path, page || 1);
            page = pageData.nextPageToken;
            const pagePosts = pageData.content
                .map(item => item.type === 'POST' ? (item as Post) : null)
                .filter(post => post != null) as Array<Post>;
            if (pagePosts.length === 0) {
                break;
            }
            newPosts.push(...pagePosts);
            pagePosts.forEach(post => {
                this.newPostSubject.next({
                    bookmark,
                    post
                })
            });
            broadcastClientEvent(IPC_EVENT_BOOKMARK_UPDATE_PROGRESS, bookmark.name, newPosts.length);
        } while (page && !_.isEmpty(lastSeenPosts) && newPosts.every(newPost => lastSeenPosts.every(post => post.url !== newPost.url)));
        await this.storage.addPostsToBookmark(bookmark.name, newPosts);
        const unseenPostCount = await this.storage.getUnseenPostsCountInBookmark(bookmark.name);
        broadcastClientEvent(IPC_EVENT_BOOKMARK_UPDATE_FINISHED, bookmark.name, unseenPostCount);
    }
}