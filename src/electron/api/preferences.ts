import {
    IPC_EVENT_PREFS_UPDATED,
    IPC_PREFS_GET_CURRENT,
    IPC_PREFS_UPDATE,
    Preferences
} from '@/electron/api/preferences-symbols';
import { registerIpcHandler } from '@/util/remote-util';
import { broadcastClientEvent } from '@/electron/util/electron';
import { Logger } from '@/electron/log/logger';
import { createLogger } from '@/logging';
import { AbstractPreferencesStorage } from '@/electron/api/preferences-abstract';
import { EventBus } from '@/event-bus';

let getPrefStorageImpl: (path?: string) => Promise<AbstractPreferencesStorage>;
if (PX_PLATFORM_ELECTRON) {
    getPrefStorageImpl = async (path) => {
        if (!path) {
            throw new Error('No path provided');
        }
        return new (await import('./preferences-node')).NodePreferencesStorage(path);
    }
} else {
    getPrefStorageImpl = async () => {
        return new (await import('./preferences-browser')).BrowserPreferencesStorage({
            logging: {
                threshold: 'INFO'
            },
            gallery: {
                hiResThumbnails: false
            },
            home: {
                items: [{
                    title: 'Recent - Derpibooru',
                    descriptor: {
                        serviceName: 'derpibooru',
                        // @ts-ignore
                        type: 'search',
                        query: 'safe "created_at.gte:3 days ago"',
                        sortField: 'score'
                    }
                }, {
                    title: 'Featured - Derpibooru',
                    descriptor: {
                        serviceName: 'derpibooru',
                        // @ts-ignore
                        type: 'search',
                        query: 'safe featured_image',
                        sortField: 'id'
                    }
                }]
            }
        });
    }
}

export class PreferencesApi {
    private impl?: AbstractPreferencesStorage;
    private readonly logger: Logger;
    private readonly eventBus: EventBus;

    private previousPrefs?: Preferences;

    constructor(preferencesPath?: string) {
        this.logger = createLogger('Preferences');
        this.eventBus = new EventBus();
        getPrefStorageImpl(preferencesPath).then(impl => {
            impl.onPrefsChanged = this.onPrefsChanged.bind(this);
            this.impl = impl;
        });
    }

    registerIpcHandlers() {
        registerIpcHandler(IPC_PREFS_GET_CURRENT, this.getCurrentPrefs.bind(this));
        registerIpcHandler(IPC_PREFS_UPDATE, this.updatePrefs.bind(this));
    }

    getCurrentPrefs() {
        return this.impl?.getCurrentPrefs();
    }

    updatePrefs(preferences: Preferences) {
        return this.impl?.updatePrefs(preferences);
    }

    onPrefsChanged(value: Preferences) {
        broadcastClientEvent(IPC_EVENT_PREFS_UPDATED, value);
        this.eventBus.fireEvent('update', value, this.previousPrefs);
        this.previousPrefs = value;
    }

    addUpdateListener(fn: (prefs: Preferences, prev: Preferences) => void) {
        this.eventBus.on('update', fn);
    }
}