import { ServiceApi } from '@/electron/api/service';
import { CollectionToPostInfo, StorageApi } from '@/electron/api/storage';
import { debounceTime, filter, map } from 'rxjs/operators';
import { PostCollection, PostCollectionSyncDescriptor } from '@/electron/api/storage-symbols';
import { Post, Service } from '@/electron/service/service';
import _ from 'lodash';
import { merge } from 'rxjs';
import { broadcastClientEvent } from '@/electron/util/electron';
import {
    IPC_EVENT_COLLECTION_SYNC_FINISHED,
    IPC_EVENT_COLLECTION_SYNC_STARTED
} from '@/electron/api/service-collection-sync-symbols';
import { Logger } from '@/electron/log/logger';
import { createLogger } from '@/logging';

export class ServiceCollectionSyncApi {
    private readonly serviceApi: ServiceApi;
    private readonly storage: StorageApi;
    private readonly logger: Logger;
    private recentlySynced: {
        postUrl: string,
        serviceName: string,
        collectionName: string
    }[];
    private currentlySyncingCollectionName: string | null = null;
    private syncing: boolean = false;

    syncInterval: number = 5 * 60 * 1000;

    constructor(serviceApi: ServiceApi, storage: StorageApi) {
        this.serviceApi = serviceApi;
        this.storage = storage;
        this.logger = createLogger('CollectionSync');
        this.recentlySynced = [];

        merge(
                storage.getCollectionNewPostObservable()
                    .pipe(
                        map(info => ({ info, action: 'add' as 'add' | 'remove' }))
                    ),
                storage.getCollectionRemovedPostObservable()
                    .pipe(
                        map(info => ({ info, action: 'remove' as 'add' | 'remove' }))
                    )
            )
            .pipe(
                filter(({ info }) => info.collection.syncEnabled && !!info.collection.syncDescriptor),
            )
            .subscribe(({ info, action }) => this.onCollectionToPostAdd(info, action));

        serviceApi.getServiceObservable()
            .pipe(
                debounceTime(1000)
            )
            .subscribe(() => this.performSync());
    }

    private getServiceForSyncDescriptor(syncDescriptor: PostCollectionSyncDescriptor) {
        return Object.values(this.serviceApi.services)
            .filter(service => service.isCollectionSyncSupported())
            .find(service => service.name === syncDescriptor.serviceName);
    }

    private wasSyncedRecently(post: Post, collection: PostCollection) {
        const syncInfo = this.recentlySynced.find(info =>
            info.postUrl === post.url
            && info.collectionName === collection.name
            && info.serviceName === collection.syncDescriptor?.serviceName
        );
        if (syncInfo) {
            this.recentlySynced = _.without(this.recentlySynced, syncInfo);
        }
        return !!syncInfo;
    }

    async onCollectionToPostAdd(info: CollectionToPostInfo, action: 'add' | 'remove') {
        if (!info.collection.syncDescriptor) {
            return;
        }
        const service = this.getServiceForSyncDescriptor(info.collection.syncDescriptor),
            syncDescriptor = info.collection.syncDescriptor;
        if (!service
            || this.currentlySyncingCollectionName === info.collection.name
            || !service.doesPostBelong(info.post)
            || this.wasSyncedRecently(info.post, info.collection)) {
            return;
        }

        this.logger.info(`${action === 'add' ? 'Adding' : 'Removing'} post to remote collection ${syncDescriptor.collectionName} for ${syncDescriptor.serviceName}`);

        try {
            if (action === 'add') {
                await service.addPostToUserCollection(syncDescriptor.collectionId, info.post);
            } else {
                await service.removePostFromUserCollection(syncDescriptor.collectionId, info.post);
            }
        }
        catch (e) {
            this.logger.error(`Failed to ${action} post`, e);
        }
    }

    async performSync() {
        if (this.syncing) {
            this.logger.info(`Ignoring request to perform sync: sync in progress`);
            return;
        }
        this.syncing = true;
        try {
            this.logger.info(`Starting sync`);
            for (const serviceDescriptor of await this.serviceApi.getServiceList()) {
                const service = this.serviceApi.services[serviceDescriptor.name];
                if (!service || !service.isCollectionSyncSupported()) {
                    continue;
                }
                await this.syncService(service, serviceDescriptor.title);
            }
            this.logger.info(`Sync finished`);
        }
        catch (e) {
            this.logger.error(`Failed to perform sync`, e);
        }
        finally {
            this.syncing = false;
            global.setTimeout(() => this.performSync().catch(e => this.logger.error(`Failed to perform sync`, e)), this.syncInterval);
        }
    }

    async syncService(service: Service<any>, serviceTitle?: string) {
        this.logger.info(`Attempting to sync service ${service.name}`);
        if (!service.isCollectionSyncEnabled()) {
            this.logger.info(`Sync disabled for service ${service.name}`);
            return;
        }

        try {
            await service.getAuthenticatedUserInfo();
        }
        catch (e) {
            this.logger.info(`Failed to check user auth for service ${service.name}`);
            return;
        }

        try {
            this.logger.info(`Syncing service ${service.name}`);
            const remoteCollections = await service.getUserCollections(),
                localCollections = await this.storage.getCollections();
            for (const remoteCollection of remoteCollections) {
                let localCollection = localCollections.find(c =>
                    c.syncDescriptor?.serviceName === service.name
                    && c.syncDescriptor?.collectionId === remoteCollection.collectionId);
                if (!localCollection) {
                    this.logger.info(`Creating local collection for ${remoteCollection.collectionName}`);
                    localCollection = {
                        name: `${remoteCollection.collectionName} (${serviceTitle ?? service.name})`,
                        download: false,
                        syncDescriptor: remoteCollection,
                        syncEnabled: true
                    };
                    await this.storage.createCollection(localCollection);
                }
                await this.syncCollection(localCollection);
            }
            this.logger.info(`Synced service ${service.name}`);
        }
        catch (e) {
            this.logger.error(`Failed to sync service ${service.name}`, e);
        }
    }

    async syncCollection(collection: PostCollection) {
        const syncDescriptor = collection.syncDescriptor;
        if (!syncDescriptor || !collection.syncEnabled) {
            return;
        }
        const service = this.getServiceForSyncDescriptor(syncDescriptor);
        if (!service || !service.isCollectionSyncEnabled()) {
            return;
        }

        this.logger.info(`Syncing collection ${syncDescriptor.collectionName} for service ${syncDescriptor.serviceName}`);
        this.currentlySyncingCollectionName = collection.name;
        broadcastClientEvent(IPC_EVENT_COLLECTION_SYNC_STARTED, collection.name);

        try {
            const remotePosts: Post[] = [];
            let nextPageToken: string | undefined,
                newPostsRemote = 0;
            do {
                const page = await service.getUserCollectionPage(syncDescriptor.collectionId, nextPageToken);
                nextPageToken = page.nextPageToken;
                const promises = page.content
                    .map(async post => {
                        if (post.type === 'POST') {
                            remotePosts.push(post);
                            const postCollections = await this.storage.getPostCollections(post.url);
                            if (!postCollections.includes(collection.name)) {
                                await this.storage.addPostToCollection(collection.name, post);
                                newPostsRemote++;
                            }
                        }
                    });
                await Promise.all(promises);
            } while (nextPageToken);

            if (newPostsRemote > 0) {
                this.logger.info(`Added ${newPostsRemote} remote posts to collection ${syncDescriptor.collectionName}`);
            }

            const collectionPageSize = 100;
            let hasNextPage = false,
                collectionOffset = 0,
                newPostsLocal = 0;
            do {
                const localCollectionPosts = await this.storage.getPostsInCollection(collection.name, null, collectionOffset, collectionPageSize);
                hasNextPage = localCollectionPosts.length === collectionPageSize;
                collectionOffset += localCollectionPosts.length;
                const additionPromises = localCollectionPosts
                    .filter(post => service.doesPostBelong(post))
                    .filter(post => !remotePosts.find(p => p.url === post.url))
                    .map(async post => {
                        await service.addPostToUserCollection(syncDescriptor.collectionId, post);
                        newPostsLocal++;
                    });
                await Promise.all(additionPromises);
            } while (hasNextPage);

            if (newPostsLocal > 0) {
                this.logger.info(`Added ${newPostsLocal} local posts to collection ${syncDescriptor.collectionName}`);
            }

            this.logger.info(`Synced collection ${collection.name}`);
        }
        catch (e) {
            this.logger.error(`Failed to sync collection ${collection.name}`, e);
        }
        finally {
            this.currentlySyncingCollectionName = null;
            broadcastClientEvent(IPC_EVENT_COLLECTION_SYNC_FINISHED, collection.name);
        }
    }
}