import {
    Page,
    Post, SearchAutocompleteSuggestion,
    Service,
    ServiceBaseOptions,
    ServiceCapability,
    ServiceDescriptor, ServiceHomeScreenItem, Tag
} from '@/electron/service/service';
import { ServiceUserInfo } from '@/electron/api/service-symbols';
import { PostCollectionSyncDescriptor } from '@/electron/api/storage-symbols';
import { getHttpClient, HttpClient } from '@/electron/http/client';
import { OptionsObject } from '@/electron/options/options';
import _ from 'lodash';
import { getQueryString } from '@/electron/util/url-util';
import OmniboxApi from '@/electron/api/omnibox';
import { OmniboxResult } from '@/electron/api/omnibox-symbols';
import { getFilenameMediaType } from '@/electron/util/media-type-util';
import { ConfigPropertyDescription } from '@/electron/config/config';

interface DerpibooruServiceOptions extends ServiceBaseOptions {
    filterId?: number
}

interface DerpibooruSearchDescriptor extends ServiceDescriptor {
    type: 'search',
    query: string,
    sortField: DerpibooruSortField
}

type DerpibooruDescriptor = DerpibooruSearchDescriptor;

interface DerpibooruImage {
    tags: string[],
    width: number,
    height: number,
    view_url: string,
    id: number,
    spoilered: boolean,
    representations: {
        full: string,
        large: string,
        medium: string,
        small: string,
        tall: string,
        thumb: string,
        thumb_small: string,
        thumb_tiny: string
    }
}

interface DerpibooruTag {
    images: number;
    category: string,
    name: string
}

type DerpibooruSortField = 'id' | 'score' | 'wilson_score';

export class DerpibooruService extends Service<DerpibooruServiceOptions, any, ServiceUserInfo, PostCollectionSyncDescriptor, DerpibooruDescriptor> {
    name: string = 'derpibooru';
    capabilities: ServiceCapability[] = ['HOME_SCREEN', 'CONFIGURATION', 'SEARCH'];

    private readonly http: HttpClient;

    protected static URL_ROOT = 'https://www.derpibooru.org';

    constructor(options: OptionsObject<DerpibooruServiceOptions>) {
        super(options);
        this.http = getHttpClient({
            'User-Agent': 'desktop:charsy:0.1'
        });
    }

    init(): Promise<void> {
        this.subservices.home = new DerpibooruSearchService(this.options, ['safe', '"created_at.gte:3 days ago"'], 'score');
        this.subservices.featured = new DerpibooruSearchService(this.options, 'safe featured_image', 'id');
        return Promise.resolve(undefined);
    }

    doesPostBelong(post: Post): boolean {
        return post.url.startsWith(DerpibooruService.URL_ROOT);
    }

    getPageUrl(page?: string) {
        return `${DerpibooruService.URL_ROOT}/api/v1/json/images/featured`;
    }

    convertTag(tag: string): Tag {
        if (tag.startsWith('artist:')) {
            return {
                category: 'artist',
                name: tag.slice('artist:'.length),
                searchName: tag
            }
        } else {
            return tag;
        }
    }

    convertImage(image: DerpibooruImage): Post {
        const thumbMediaType = getFilenameMediaType(image.representations.thumb);
        return {
            type: 'POST',
            url: `${DerpibooruService.URL_ROOT}/images/${image.id}`,
            name: image.id.toString(),
            thumbUrl: thumbMediaType?.type === 'image' ? image.representations.thumb : image.representations.thumb.replace('.' + thumbMediaType?.extension, '.gif'),
            previewUrl: image.representations.medium,
            fileUrl: image.representations.full,
            width: image.width,
            height: image.height,
            tags: image.tags.map(t => this.convertTag(t)),
            rating: image.tags.includes('safe')
                ? 's'
                : (image.tags.includes('suggestive') || image.tags.includes('questionable'))
                    ? 'q'
                    : image.tags.includes('explicit')
                        ? 'e'
                        : undefined,
            rawData: image
        };
    }

    async getPage(page?: string): Promise<Page> {
        const url = this.getPageUrl(page),
            data = await this.http.getJson<{ images?: DerpibooruImage[], image?: DerpibooruImage }>(url),
            images: DerpibooruImage[] = data.images || [data.image!]
        return {
            nextPageToken: data.images && data.images.length > 0 ? ((+(page || 1)) + 1).toString() : undefined,
            content: images.map(i => this.convertImage(i))
        };
    }

    async getHomeScreenItems(): Promise<ServiceHomeScreenItem[] | null> {
        return [{
            title: 'Recent',
            galleryRelPath: '/home'
        }, {
            title: 'Featured',
            galleryRelPath: `/featured`
        }];
    }

    async getSubservice(name: string): Promise<Service<any> | null> {
        if (name.startsWith('search:')) {
            const [query, sortField] = name.slice('search:'.length).split('|||');
            return new DerpibooruSearchService(this.options, query, sortField as DerpibooruSortField);
        }
        return this.subservices[name];
    }

    async getAutocompleteSuggestions(query: string): Promise<SearchAutocompleteSuggestion[] | null> {
        const response = await this.http.getJson<{ tags: DerpibooruTag[] }>(`${DerpibooruService.URL_ROOT}/api/v1/json/search/tags?q=name:${this.canonicalTagToLocal(query)}*`);
        return response.tags.map(t => ({
            category: t.category,
            name: this.localTagToCanonical(t.name),
            postCount: t.images
        }));
    }

    async getSubServicePathByDescriptor(descriptor: DerpibooruDescriptor): Promise<string | null> {
        switch (descriptor.type) {
            case 'search': return 'search:' + [descriptor.query, descriptor.sortField].join('|||');
            default: return null;
        }
    }

    localTagToCanonical(localTag: string): string {
        return localTag.startsWith('"') ? localTag : localTag.replaceAll(' ', '_');
    }

    canonicalTagToLocal(canonicalTag: string): string {
        return canonicalTag.startsWith('"') ? canonicalTag : canonicalTag.replaceAll('_', ' ');
    }

    getCommonQueryParams() {
        return {
            filter_id: this.options.getChild('filterId')?.currentValue
        };
    }

    splitTags(query: string): string[] {
        const result = [];
        let currentTag = '',
            verbatim = false;
        for (const char of query) {
            if (char === '"') {
                verbatim = !verbatim;
            }
            if (char === ' ' && !verbatim) {
                result.push(currentTag);
                currentTag = '';
            } else {
                currentTag += char;
            }
        }
        if (!_.isEmpty(_.trim(currentTag))) {
            result.push(currentTag);
        }
        return result;
    }

    registerOmnibox(omnibox: OmniboxApi, serviceName: string) {
        omnibox.registerContext({
            name: 'Danbooru search',
            type: 'SERVICE',
            serviceName: serviceName,
            aliases: ['db'],
            queryCallback: async query => {
                const terms = query.split(' '),
                    lastTerm = _.last(terms);
                if (!lastTerm || lastTerm.length < 3) {
                    return [];
                }
                const autocomplete = await this.getAutocompleteSuggestions(lastTerm),
                    results: OmniboxResult[] = [];

                const searchPath = await this.getSubServicePathByDescriptor({
                        serviceName: this.name,
                        type: 'search',
                        query: terms.join(','),
                        sortField: 'id'
                    });

                results.unshift({
                    contextName: 'Derpibooru search',
                    contextType: 'SERVICE',
                    resultName: query,
                    resultType: 'SERVICE_PAGE',
                    pagePath: `/${serviceName}/${searchPath}`
                });

                for (const s of autocomplete || []) {
                    const fullTerms = [..._.take(terms, terms.length - 1), s.name];
                    const pagePath = await this.getSubServicePathByDescriptor({
                        serviceName: this.name,
                        type: 'search',
                        query: fullTerms.join(','),
                        sortField: 'id'
                    });
                    if (pagePath) {
                        results.push({
                            contextName: 'Derpibooru search',
                            contextType: 'SERVICE',
                            resultName: fullTerms.join(' '),
                            resultType: 'SERVICE_PAGE',
                            pagePath: `/${serviceName}/${pagePath}`
                        });
                    }
                }

                return results;
            }
        });
    }

    getDescriptorConfigDescriptions() {
        const search: ConfigPropertyDescription<Omit<DerpibooruSearchDescriptor, 'serviceName' | 'type'>> = {
            label: 'Search',
            type: 'object',
            children: {
                query: {
                    type: 'string',
                    label: 'Search query'
                },
                sortField: {
                    type: 'string',
                    label: 'Sort by',
                    editor: {
                        type: 'combobox',
                        options: [{
                            label: 'Post ID',
                            value: 'id'
                        }, {
                            label: 'Score',
                            value: 'score'
                        }, {
                            label: 'Wilson score',
                            value: 'wilson_score'
                        }]
                    }
                }
            }
        }

        return {
            discriminator: 'type',
            descriptions: {
                search
            }
        };
    }
}

class DerpibooruSearchService extends DerpibooruService {
    private readonly terms: string[];
    private readonly sortField?: DerpibooruSortField;

    constructor(options: OptionsObject<DerpibooruServiceOptions>, query: string | string[], sortField?: DerpibooruSortField) {
        super(options);
        this.terms = _.isString(query) ? this.splitTags(query) : query;
        this.sortField = sortField;
        this.capabilities = _.without(this.capabilities, 'SEARCH', 'HOME_SCREEN');
        this.name = 'search';
    }

    getPageUrl(page?: string) {
        return `${DerpibooruService.URL_ROOT}/api/v1/json/search/images/` + getQueryString({
            ...this.getCommonQueryParams(),
            q: this.terms.map(t => this.canonicalTagToLocal(t)).join(','),
            page: page || 1,
            sf: this.sortField
        });
    }
}