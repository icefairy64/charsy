import {
    Page,
    Post,
    Service,
    ServiceBaseOptions,
    ServiceCapability,
    ServiceDescriptor
} from '@/electron/service/service';
import { ServiceUserInfo } from '@/electron/api/service-symbols';
import { PostCollectionSyncDescriptor } from '@/electron/api/storage-symbols';
import { getQueryString } from '@/electron/util/url-util';
import { getHttpClient, HttpClient } from '@/electron/http/client';
import { OptionsObject } from '@/electron/options/options';
import _ from 'lodash';
import OmniboxApi from '@/electron/api/omnibox';
import { OmniboxResult } from '@/electron/api/omnibox-symbols';

export interface BooruPlusServiceOptions extends ServiceBaseOptions {}

type BooruPlusPostOrder = 'hot' | 'new' | 'top' | 'rnd' | 'pro' | 'tsa'

interface BooruPlusBooruDescriptor extends ServiceDescriptor {
    type: 'booru',
    booru: string
    query?: string,
    order: BooruPlusPostOrder
}

type BooruPlusDescriptor = BooruPlusBooruDescriptor

interface BooruPlusThumb {
    elId: string,
    postHref: string,
    isNsfw: boolean,
    lowResLowDpiThumbUrl: string,
    lowResHiDpiThumbUrl: string,
    hiResLowDpiThumbUrl: string,
    hiResHiDpiThumbUrl: string,
    postWidth: number,
    postHeight: number,
    tags: string[]
}

interface BooruPlusPostJsonLd {
    dateCreated: string,
    name: string,
    width: number,
    height: number,
    keywords: string[],
    image: {
        url: string,
        thumbnailUrl: string,
        contentUrl: string
    }
}

interface BooruPlusAutocompleteSuggestion {
    tag_name: string,
    tag_type?: boolean
}

export class BooruPlusService extends Service<BooruPlusServiceOptions, any, ServiceUserInfo, PostCollectionSyncDescriptor, BooruPlusDescriptor> {
    name = 'booruplus';
    capabilities: Array<ServiceCapability> = ['HOME_SCREEN', 'CONFIGURATION', 'SEARCH'];

    protected URL_ROOT = 'https://booru.plus';
    private http: HttpClient;

    constructor(options: OptionsObject<BooruPlusServiceOptions>) {
        super(options);
        this.http = getHttpClient();
    }

    init(): Promise<void> {
        return Promise.resolve(undefined);
    }

    doesPostBelong(post: Post): boolean {
        return post.url.startsWith(this.URL_ROOT);
    }

    protected getPageUrl(page?: number | string): string {
        if (page === 1) {
            page = undefined;
        }
        return this.URL_ROOT + (page || '/');
    }

    getPostDimensionsArray(html: string): [string, number, number][] {
        const pdConstStr = 'const postDimensions = ',
            pdConstIndex = html.indexOf(pdConstStr),
            nextSemicolonIndex = html.indexOf(';', pdConstIndex),
            startIndex = pdConstIndex + pdConstStr.length,
            arrayStr = html.substring(startIndex, nextSemicolonIndex).replaceAll("'", '"').replace('],]', ']]');

        return JSON.parse(arrayStr);
    }

    getPostDimensionsMap(html: string) {
        const ar = this.getPostDimensionsArray(html);
        return ar.reduce((a, x) => {
            a[x[0]] = {
                width: x[1],
                height: x[2]
            };
            return a;
        }, {} as Record<string, { width: number, height: number }>);
    }

    getThumb(html: string, postDimensions: Record<string, { width: number, height: number }>, index: number): BooruPlusThumb {
        const isNsfw = html[index + ('class="post-a').length] !== '"',

            idStartIndex = html.indexOf('id="', index) + 'id="'.length,
            idEndIndex = html.indexOf('"', idStartIndex),
            id = html.substring(idStartIndex + 1, idEndIndex),

            hrefStartIndex = html.indexOf('href="', index) + 'href="'.length,
            hrefEndIndex = html.indexOf('"', hrefStartIndex),
            href = html.substring(hrefStartIndex, hrefEndIndex),

            srcSet1StartIndex = html.indexOf('srcset="', index) + 'srcset="'.length,
            srcSet1EndIndex = html.indexOf('"', srcSet1StartIndex),
            srcSet1 = html.substring(srcSet1StartIndex, srcSet1EndIndex),

            srcSet2StartIndex = html.indexOf('srcset="', srcSet1EndIndex) + 'srcset="'.length,
            srcSet2EndIndex = html.indexOf('"', srcSet2StartIndex),
            srcSet2 = html.substring(srcSet2StartIndex, srcSet2EndIndex),

            imgStartIndex = html.indexOf('img alt="', index) + 'img alt="'.length,
            imgEndIndex = html.indexOf('"', imgStartIndex),
            img = html.substring(imgStartIndex, imgEndIndex);

        const srcSetRegex = /(.+?), (.+?) 2x/,
            [loLoUrl, loHiUrl] = srcSet2.match(srcSetRegex)?.slice(1) || [],
            [hiLoUrl, hiHiUrl] = srcSet1.match(srcSetRegex)?.slice(1) || [];

        return {
            elId: id,
            hiResHiDpiThumbUrl: hiHiUrl,
            hiResLowDpiThumbUrl: hiLoUrl,
            isNsfw,
            lowResHiDpiThumbUrl: loHiUrl,
            lowResLowDpiThumbUrl: loLoUrl,
            postHeight: postDimensions[id].height,
            postHref: href,
            postWidth: postDimensions[id].width,
            tags: img.split(' ').filter(t => t !== '')
        }
    }

    thumbToPost(thumb: BooruPlusThumb): Post {
        const contentKeyMatch = thumb.hiResHiDpiThumbUrl.match(/\.xyz\/([0-9/]+)_.\./),
            contentKey = contentKeyMatch?.[1]?.replaceAll('/', '') ?? thumb.hiResHiDpiThumbUrl.match(/\/([0-9]+)_.\./)?.[1];

        return {
            type: 'POST',
            fileUrl: contentKey ? `https://o3.booru.xyz/${contentKey.slice(0, 2)}/${contentKey.slice(2, 5)}/${contentKey.slice(5)}` : thumb.hiResHiDpiThumbUrl,
            name: thumb.postHref,
            tags: thumb.tags,
            url: this.URL_ROOT + thumb.postHref,
            width: thumb.postWidth,
            height: thumb.postHeight,
            thumbUrl: thumb.lowResLowDpiThumbUrl,
            previewUrl: thumb.hiResHiDpiThumbUrl,
            rating: thumb.isNsfw ? 'e' : 's',
            mediaType: thumb.tags.includes('video') ? 'video/mp4' : 'image/jpeg'
        }
    }

    getNextPageToken(html: string): string {
        const aStr = 'a class="pagination-a" href="',
            aStart = html.lastIndexOf(aStr, html.indexOf('alt="Next"')) + aStr.length,
            aEnd = html.indexOf('"', aStart);
        return html.substring(aStart, aEnd - aStart);
    }

    async getPage(page: number | string | undefined): Promise<Page> {
        const pageUrl = this.getPageUrl(page),
            pageContent = await this.http.get(pageUrl);

        const postDimensionsMap = this.getPostDimensionsMap(pageContent),
            thumbs = [];

        let curIndex = 0;
        while (curIndex >= 0) {
            const nextIndex = pageContent.indexOf('class="post-a', curIndex);
            if (nextIndex > 0) {
                thumbs.push(this.getThumb(pageContent, postDimensionsMap, nextIndex));
                curIndex = nextIndex + 50;
            } else {
                curIndex = -1;
            }
        }

        return {
            content: thumbs.map(t => this.thumbToPost(t))
        }
    }

    async getSubservice(name: string): Promise<Service<any> | null> {
        if (name.startsWith('booru:')) {
            const [booru, query, order] = name.substring('booru:'.length).split('|||');
            return new BooruPlusBooruService(this.options, booru, query, order as BooruPlusPostOrder);
        }
        return this.subservices[name];
    }

    async getSubServicePathByDescriptor(descriptor: BooruPlusDescriptor): Promise<string | null> {
        switch (descriptor.type) {
            case 'booru': return 'booru:' + [descriptor.booru, descriptor.query, descriptor.order].join('|||');
            default: return null;
        }
    }

    async getBooruAutocompleteSuggestions(booru: string, query: string): Promise<BooruPlusAutocompleteSuggestion[]> {
        const url = `${this.URL_ROOT}/_searchautocomplete` + getQueryString({
            booru,
            q: query
        });

        return this.http.getJson(url);
    }

    registerOmnibox(omnibox: OmniboxApi, serviceName: string) {
        omnibox.registerContext({
            name: 'BooruPlus',
            type: 'SERVICE',
            serviceName: serviceName,
            aliases: ['bp'],
            queryCallback: async query => {
                const terms = query.split(' '),
                    booruTerm = terms.find(t => t.startsWith('+')),
                    restTerms = terms.filter(t => t !== booruTerm);

                if (booruTerm == null) {
                    return [];
                }

                const booru = booruTerm.slice(1);

                const results: OmniboxResult[] = [];

                const pagePath = await this.getSubServicePathByDescriptor({
                    serviceName: this.name,
                    type: 'booru',
                    booru,
                    query: restTerms.join('+'),
                    order: 'new'
                });

                results.push({
                    contextName: 'BooruPlus',
                    contextType: 'SERVICE',
                    resultName: `${booru} / ${restTerms.join(' ')}`,
                    resultType: 'SERVICE_PAGE',
                    pagePath: `/${serviceName}/${pagePath}`
                });

                const lastTerm = _.last(restTerms) || '',
                    suggestions = await this.getBooruAutocompleteSuggestions(booru, lastTerm),
                    tags = suggestions.map(s => s.tag_name).filter(t => t !== lastTerm);

                for (const tag of tags) {
                    const terms = [...restTerms.slice(0, -1), tag];

                    const pagePath = await this.getSubServicePathByDescriptor({
                        serviceName: this.name,
                        type: 'booru',
                        booru,
                        query: terms.join('+'),
                        order: 'new'
                    });

                    results.push({
                        contextName: 'BooruPlus',
                        contextType: 'SERVICE',
                        resultName: `${booru} / ${terms.join(' ')}`,
                        resultType: 'SERVICE_PAGE',
                        pagePath: `/${serviceName}/${pagePath}`
                    });
                }

                return results;
            }
        });
    }
}

class BooruPlusBooruService extends BooruPlusService {
    private readonly booru: string
    private readonly query: string
    private readonly order: BooruPlusPostOrder

    constructor(options: OptionsObject<BooruPlusServiceOptions>, booru: string, query: string, order: BooruPlusPostOrder) {
        super(options);
        this.booru = booru;
        this.query = query;
        this.order = order;
        this.capabilities = _.without(this.capabilities, 'SEARCH', 'HOME_SCREEN');
    }

    protected getPageUrl(page?: number | string): string {
        if (page === 1) {
            page = undefined;
        }

        if (page) {
            return this.URL_ROOT + page;
        }

        let subPath = '';
        if (this.query !== '') {
            subPath = `/${this.query}`
        }

        const queryString = getQueryString({
            o: this.order
        });

        return `${this.URL_ROOT}/+${this.booru}${subPath}${queryString}`;
    }
}