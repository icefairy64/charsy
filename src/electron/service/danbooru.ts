import {
    Page,
    Post, PostRating,
    SearchAutocompleteSuggestion,
    Service, ServiceBaseOptions,
    ServiceCapability,
    ServiceHomeScreenItem
} from './service';
import { getHttpClient, HttpClient } from '../http/client';
import _ from 'lodash';
import { OptionsObject } from '@/electron/options/options';
import { PostCollectionSyncDescriptor } from '@/electron/api/storage-symbols';
import OmniboxApi from '@/electron/api/omnibox';

export interface DanbooruAuthCredentials {
    login: string,
    apiKey: string
}

export interface DanbooruUserInfo {
    id: number,
    name: string,
    favorite_tags?: string,
    blacklisted_tags?: string
}

interface DanbooruServiceOptions extends ServiceBaseOptions {
    auth?: DanbooruAuthCredentials,
    cooldown?: number
}

interface DanbooruPost {
    id?: number,
    md5: string,
    file_url: string,
    large_file_url: string,
    preview_file_url: string,
    tag_string: string,
    tag_string_general: string,
    tag_string_character: string,
    tag_string_copyright: string,
    tag_string_artist: string,
    tag_string_meta: string,
    image_width?: number,
    image_height?: number,
    rating?: PostRating
}

interface DanbooruAutocompleteSuggestion {
    type: string,
    label: string,
    value: string,
    category: number,
    post_count: number,
    antecedent?: string
}

type DanbooruPage = Array<DanbooruPost>;

interface DanbooruFavGroup {
    id: number,
    name: string,
    post_ids: number[]
}

interface DanbooruFavGroupCollectionSyncDescriptor extends PostCollectionSyncDescriptor {
    type: 'FAV_GROUP'
}

const tagTypeMap: Record<number, string> = {
    0: 'general',
    1: 'artist',
    3: 'copyright',
    4: 'character',
    5: 'meta'
}

export class DanbooruService extends Service<DanbooruServiceOptions, DanbooruAuthCredentials, DanbooruUserInfo> {
    name: string = 'danbooru';

    private readonly http: HttpClient;

    protected static URL_ROOT = 'https://danbooru.donmai.us';

    public capabilities: Array<ServiceCapability> = ['AUTH', 'CONFIGURATION', 'SEARCH', 'HOME_SCREEN', 'COLLECTION_SYNC'];

    constructor(options: OptionsObject<DanbooruServiceOptions>) {
        super(options);
        this.http = getHttpClient(undefined, options.currentValue?.cooldown || 500);
        this.initSubservices();
    }

    async init(): Promise<void> {

    }

    protected initSubservices() {
        this.subservices.popular = new DanbooruPopularService(this.options);
        this.subservices.curated = new DanbooruCuratedService(this.options);
    }

    protected getPageUrl(page: number | string): string {
        return `/posts.json?page=${page}`;
    }

    protected convertPost(postData: DanbooruPost): Post {
        const thumbScale = 150 / Math.max(postData.image_width || 150, postData.image_height || 150);
        const convertTagString = (category: string, tagString: string) => {
            return tagString.split(' ').map(tag => ({
                category,
                name: tag
            }));
        }
        return {
            type: 'POST',
            name: postData.id?.toString() || 'unknown',
            url: `${DanbooruService.URL_ROOT}/posts/${postData.id}`,
            thumbUrl: postData.preview_file_url,
            previewUrl: postData.large_file_url,
            fileUrl: postData.file_url,
            tags: [
                ...convertTagString('general', postData.tag_string_general),
                ...convertTagString('character', postData.tag_string_character),
                ...convertTagString('copyright', postData.tag_string_copyright),
                ...convertTagString('artist', postData.tag_string_artist),
                ...convertTagString('meta', postData.tag_string_meta),
            ],
            thumbWidth: postData.image_width && postData.image_width * thumbScale,
            thumbHeight: postData.image_height && postData.image_height * thumbScale,
            width: postData.image_width,
            height: postData.image_height,
            rating: postData.rating,
            rawData: postData
        }
    }

    getNextPageToken(currentPage: number | string, page: DanbooruPage) {
        const lastPost = _.last(page.filter(post => post.id));
        return lastPost && `b${lastPost.id}` || undefined;
    }

    async getPage(page: number | string): Promise<Page> {
        const url = DanbooruService.URL_ROOT + this.getPageUrl(page);
        const json = await this.http.get(url);
        const pageData: DanbooruPage = JSON.parse(json);
        return {
            content: pageData.map(this.convertPost.bind(this)),
            nextPageToken: this.getNextPageToken(page, pageData)
        };
    }

    async getSubservices(): Promise<Array<Service<any>>> {
        return Object.values(this.subservices)
            .filter(service => service.name !== 'search');
    }

    async getSubservice(name: string): Promise<Service<any> | null> {
        if (name.startsWith('search:')) {
            this.subservices[name] = new DanbooruSearchService(this.options, name.substring('search:'.length));
        }
        return this.subservices[name];
    }

    async getAutocompleteSuggestions(query: string): Promise<SearchAutocompleteSuggestion[]> {
        const json = await this.http.get(`${DanbooruService.URL_ROOT}/autocomplete.json?search[query]=${encodeURIComponent(query)}&search[type]=tag_query&limit=10`);
        const data = JSON.parse(json) as DanbooruAutocompleteSuggestion[];
        return data.map(s => ({
            category: tagTypeMap[s.category],
            label: s.label,
            name: s.value,
            postCount: s.post_count
        }));
    }

    async getHomeScreenItems(): Promise<ServiceHomeScreenItem[] | null> {
        const services = [{
            title: 'Popular',
            service: this.subservices.popular
        }, {
            title: 'Curated for today',
            service: this.subservices.curated
        }, {
            title: 'Posts',
            service: this
        }];
        return await Promise.all(services.map(async serviceEntry => {
            const page = await serviceEntry.service.getPage(1);
            return {
                title: serviceEntry.title,
                galleryRelPath: serviceEntry.service === this ? '' : '/' + serviceEntry.service.name
            }
        }));
    }

    protected getAuthOptions(): OptionsObject<DanbooruAuthCredentials> {
        if (!this.options.currentValue?.auth) {
            this.options.setChildValue('auth', undefined);
        }
        // @ts-ignore
        return this.options.getChild('auth')!;
    }

    async authenticate(credentials: DanbooruAuthCredentials) {
        function toBase64(data: string) {
            return new Buffer(data).toString('base64');
        }
        this.http.updateDefaultHeaders({
            'Authorization': `Basic ${toBase64(credentials.login + ':' + credentials.apiKey)}`
        });
        try {
            this.logger.info(`Logging in`);
            await this.getAuthenticatedUserInfo();
            this.logger.info(`Login successful`);
            this.getAuthOptions().setValue(credentials);
        }
        catch (e) {
            this.logger.error(`Login failed`, e);
            this.http.updateDefaultHeaders({
                'Authorization': undefined
            });
            throw e;
        }
    }

    async getAuthenticatedUserInfo(): Promise<DanbooruUserInfo | null> {
        if (!this.getAuthOptions().currentValue) {
            return null;
        }
        const json = await this.http.get(`${DanbooruService.URL_ROOT}/profile.json`);
        const profileData = JSON.parse(json);
        if (profileData.success === false) {
            throw new Error(`Failed to retrieve user data: ${profileData.message}`);
        }
        return profileData;
    }

    isAuthSupported(): boolean {
        return true;
    }

    doesPostBelong(post: Post): boolean {
        return post.url.startsWith(DanbooruService.URL_ROOT);
    }

    async getUserCollections(): Promise<PostCollectionSyncDescriptor[]> {
        const userData = await this.getAuthenticatedUserInfo();
        if (!userData) {
            throw new Error('Not logged in');
        }
        const json = await this.http.get(`${DanbooruService.URL_ROOT}/favorite_groups.json?search[creator_name]=${userData.name}`);
        return JSON.parse(json)
            .map((favGroup: DanbooruFavGroup) => ({
                serviceName: this.name,
                collectionName: favGroup.name.replaceAll('_', ' '),
                collectionId: String(favGroup.id),
                type: 'FAV_GROUP'
            }));
    }

    async getUserCollectionPage(collectionId: string, page?: string) {
        const service = new DanbooruFavGroupService(this.options, +collectionId);
        return service.getPage(page || 1);
    }

    async addPostToUserCollection(collectionId: string, post: Post) {
        await this.http.request(`${DanbooruService.URL_ROOT}/favorite_groups/${collectionId}/add_post.json?post_id=${post.name}`, 'PUT');
    }

    async getFavGroup(favGroupId: number) {
        const json = await this.http.get(`${DanbooruService.URL_ROOT}/favorite_groups/${favGroupId}.json`);
        return JSON.parse(json) as DanbooruFavGroup;
    }

    async removePostFromUserCollection(collectionId: string, post: Post) {
        const favGroup = await this.getFavGroup(+collectionId);
        await this.http.sendJson(`${DanbooruService.URL_ROOT}/favorite_groups/${+collectionId}.json`, 'PATCH', {
            post_ids: _.without(favGroup.post_ids, +post.name)
        });
    }

    registerOmnibox(omnibox: OmniboxApi, serviceName: string) {
        omnibox.registerContext({
            name: 'Danbooru',
            type: 'SERVICE',
            aliases: ['d'],
            serviceName: serviceName,
            queryCallback: async query => (await this.getAutocompleteSuggestions(query)).map(s => ({
                contextName: 'Danbooru',
                contextType: 'SERVICE',
                resultName: s.label || s.name,
                resultType: 'SERVICE_PAGE',
                pagePath: `/${serviceName}/search:${s.name}`
            }))
        });
    }
}

export class DanbooruSearchService extends DanbooruService {
    private readonly query: string;

    constructor(options: OptionsObject<DanbooruServiceOptions>, query: string) {
        super(options);
        this.query = query;
        this.capabilities = _.without(this.capabilities, 'SEARCH', 'HOME_SCREEN');
        this.name = 'search';
    }

    protected initSubservices() {

    }

    protected getPageUrl(page: number): string {
        return `/posts.json?tags=${encodeURIComponent(this.query)}&page=${page}`;
    }

    async getSubservice(name: string): Promise<Service<any> | null> {
        return null;
    }

    isAuthSupported(): boolean {
        return false;
    }
}

class DanbooruPopularService extends DanbooruService {
    constructor(options: OptionsObject<DanbooruServiceOptions>) {
        super(options);
        this.capabilities = _.without(this.capabilities, 'SEARCH', 'HOME_SCREEN');
        this.name = 'popular';
    }

    protected initSubservices() {

    }

    protected getPageUrl(page: number): string {
        return `/explore/posts/popular.json?page=${page}`;
    }

    async getSubservice(name: string): Promise<Service<any> | null> {
        return null;
    }

    isAuthSupported(): boolean {
        return false;
    }
}

class DanbooruCuratedService extends DanbooruService {
    constructor(options: OptionsObject<DanbooruServiceOptions>) {
        super(options);
        this.capabilities = _.without(this.capabilities, 'SEARCH', 'HOME_SCREEN');
        this.name = 'curated';
    }

    protected initSubservices() {

    }

    protected getPageUrl(page: number): string {
        return `/explore/posts/curated.json?page=${page}`;
    }

    async getSubservice(name: string): Promise<Service<any> | null> {
        return null;
    }

    isAuthSupported(): boolean {
        return false;
    }
}

export class DanbooruFavGroupService extends DanbooruService {
    private readonly favGroupId: number;

    constructor(options: OptionsObject<DanbooruServiceOptions>, favGroupId: number) {
        super(options);
        this.favGroupId = favGroupId;
        this.capabilities = _.without(this.capabilities, 'SEARCH', 'HOME_SCREEN');
        this.name = 'search';
    }

    protected initSubservices() {

    }

    protected getPageUrl(page: number): string {
        return `/posts.json?tags=${encodeURIComponent(`favgroup:${this.favGroupId} status:any`)}&page=${page}`;
    }

    async getSubservice(name: string): Promise<Service<any> | null> {
        return null;
    }

    isAuthSupported(): boolean {
        return false;
    }

    getNextPageToken(currentPage: number | string, page: DanbooruPage) {
        if (_.isEmpty(page)) {
            return undefined;
        }
        return String((+currentPage) + 1);
    }
}