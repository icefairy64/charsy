import {
    Page,
    PageItem,
    Post,
    SearchAutocompleteSuggestion,
    Service,
    ServiceBaseOptions,
    ServiceCapability, ServiceDescriptor,
    ServiceHomeScreenItem
} from '@/electron/service/service';
import { OptionsObject } from '@/electron/options/options';
import _ from 'lodash';
import { getHttpClient, HttpClient } from '@/electron/http/client';
import { DOMParser } from 'xmldom';
import { AccessToken, create, OAuthClient } from 'simple-oauth2';
import OmniboxApi from '@/electron/api/omnibox';
import { OmniboxResult } from '@/electron/api/omnibox-symbols';
import { ServiceDescriptorConfigInfo } from '@/electron/api/service-symbols';
import { ConfigPropertyDescription } from '@/electron/config/config';

const DeviantArtRSSOrderTypes = {
    newest: 5,
    popularAllTime: 9,
    popular8Hours: 10,
    popularToday: 11,
    popular3Days: 12,
    popularThisWeek: 14,
    popularThisMonth: 15
}

// RSS types

type DeviantArtRSSOrderType = keyof typeof DeviantArtRSSOrderTypes;

const DeviantArtRSSTopics = {
    manga: 'manga',
    digitalArt: {
        _root: 'digitalart',
        drawings: 'drawings'
    }
}

type DeviantArtRSSRating = 'adult' | 'nonadult';

interface DeviantArtRSSImageDescription {
    width: number,
    height: number,
    url: string
}

interface DeviantArtRSSItem {
    title: string,
    url: string,
    publishDate: Date,
    rating: DeviantArtRSSRating,
    thumbnails: DeviantArtRSSImageDescription[]
    content?: DeviantArtRSSImageDescription
}

interface DeviantArtRSSPage {
    nextPageUrl: string,
    items: DeviantArtRSSItem[]
}

// OAuth types

interface DeviantArtDailyDeviationServiceDescriptor extends ServiceDescriptor {
    type: 'dailydeviation'
}

interface DeviantArtSearchServiceDescriptor extends ServiceDescriptor {
    type: 'search',
    query: string,
    order?: string
}

interface DeviantArtTopicServiceDescriptor extends ServiceDescriptor {
    type: 'topic',
    topic: string
}

type DeviantArtServiceDescriptor =
    DeviantArtDailyDeviationServiceDescriptor |
    DeviantArtSearchServiceDescriptor |
    DeviantArtTopicServiceDescriptor;

interface DeviantArtAPIDeviation {
    deviationid: string,
    url: string,
    title?: string,
    author?: DeviantArtAPIUser,
    category?: string,
    category_path?: string,
    content?: DeviantArtAPIImage,
    preview?: DeviantArtAPIImage,
    thumbs?: DeviantArtAPIImage[],
    videos?: DeviantArtAPIVideo[],
    text_content?: string,
    is_mature: boolean
}

interface DeviantArtAPIUser {
    userid: string,
    username: string,
    usericon: string
}

interface DeviantArtAPIPage {
    results: DeviantArtAPIDeviation[],
    has_more: boolean,
    next_offset?: number
}

interface DeviantArtAPIImage {
    src: string,
    height: number,
    width: number,
    transparency: boolean
}

interface DeviantArtAPIVideo {
    src: string,
    quality: string,
    filesize: number,
    duration: number
}

interface DeviantArtAPITagSearchPage {
    results: DeviantArtAPITag[]
}

interface DeviantArtAPIDeviationMetadata {
    deviationid: string,
    tags: DeviantArtAPITag[]
}

interface DeviantArtAPITag {
    tag_name: string
}

interface DeviantArtAPIDeviationMetadataPage {
    metadata: DeviantArtAPIDeviationMetadata[]
}

interface DeviantArtServiceOptions extends ServiceBaseOptions {
    oauth2?: {
        clientId: number,
        clientSecret: string
    }
}

export class DeviantArtService extends Service<DeviantArtServiceOptions> {
    name: string = 'deviantart';
    capabilities: ServiceCapability[] = ['CONFIGURATION', 'HOME_SCREEN', 'SEARCH'];

    private readonly http: HttpClient;

    protected static URL_ROOT = 'https://www.deviantart.com';
    protected static OAUTH2_CLIENT?: OAuthClient;
    protected static OAUTH2_TOKEN?: AccessToken;

    constructor(options: OptionsObject<DeviantArtServiceOptions>) {
        super(options);
        this.http = getHttpClient();
        if (options.currentValue?.oauth2 && !DeviantArtService.OAUTH2_CLIENT) {
            DeviantArtService.OAUTH2_CLIENT = create<"client_id">({
                auth: {
                    tokenHost: DeviantArtService.URL_ROOT,
                    tokenPath: '/oauth2/token'
                },
                client: {
                    id: options.currentValue.oauth2.clientId.toString(),
                    secret: options.currentValue.oauth2.clientSecret
                }
            });
        }
        this.initSubservices();
    }

    async init(): Promise<void> {

    }

    protected initSubservices() {
        if (DeviantArtService.OAUTH2_CLIENT) {
            this.subservices.dailyDeviations = new DeviantArtDailyDeviationService(this.options);
            this.subservices.dailyDeviations.name = 'dailyDeviations';
        } else {
            this.subservices.digitalArtToday = new DeviantArtSearchService(this.options, 'in:digitalart', 'popularToday');
            this.subservices.digitalArtToday.name = 'digitalArtToday';
            this.subservices.digitalArtThisWeek = new DeviantArtSearchService(this.options, 'in:digitalart', 'popularThisWeek');
            this.subservices.digitalArtThisWeek.name = 'digitalArtThisWeek';
            this.subservices.thisMonth = new DeviantArtSearchService(this.options, '', 'popularThisMonth');
            this.subservices.thisMonth.name = 'thisMonth';
        }
    }

    protected async ensureOauthToken(): Promise<AccessToken> {
        if (DeviantArtService.OAUTH2_CLIENT) {
            // @ts-ignore
            if (!DeviantArtService.OAUTH2_TOKEN || DeviantArtService.OAUTH2_TOKEN.expired(60)) {
                const token = await DeviantArtService.OAUTH2_CLIENT.clientCredentials.getToken({

                });
                DeviantArtService.OAUTH2_TOKEN = DeviantArtService.OAUTH2_CLIENT.accessToken.create(token);
            }
            return DeviantArtService.OAUTH2_TOKEN;
        } else {
            throw new Error('No OAuth2 present');
        }
    }

    protected getMatureFilterRequestString(force: boolean = false) {
        if (force || this.options.currentValue?.ratingFilter?.includes('e')) {
            return '&mature_content=true';
        } else {
            return '';
        }
    }

    doesPostBelong(post: Post): boolean {
        return post.url.startsWith(DeviantArtService.URL_ROOT);
    }

    getFullUrl(contentUrl: string): string {
        const cropRegexp = /\/v1\/.+?\?/;
        return contentUrl.replace(cropRegexp, '?');
    }

    convertRssItem(rssItem: DeviantArtRSSItem): PageItem {
        const thumb = _.last(rssItem.thumbnails.sort((a, b) => a.width - b.width));
        return {
            type: 'POST',
            name: rssItem.title,
            title: rssItem.title,
            url: rssItem.url,
            tags: [],
            rating: rssItem.rating === 'nonadult' ? 's' : 'e',
            fileUrl: this.getFullUrl(rssItem.content!!.url),
            width: rssItem.content!!.width,
            height: rssItem.content!!.height,
            previewUrl: rssItem.content!!.url,
            thumbUrl: thumb?.url,
            thumbWidth: thumb?.width,
            thumbHeight: thumb?.height,
            rawData: rssItem
        };
    }

    convertRssPage(rssPage: DeviantArtRSSPage): Page {
        return {
            nextPageToken: rssPage.nextPageUrl,
            content: rssPage.items
                .filter(item => item.content != null)
                .map(item => this.convertRssItem(item))
        };
    }

    parseRssImage(el: Element): DeviantArtRSSImageDescription {
        return {
            url: el.getAttribute('url')!!,
            width: +(el.getAttribute('width')!!),
            height: +(el.getAttribute('height')!!)
        };
    }

    parseXmlResponse(xmlBody: string): DeviantArtRSSPage {
        const parser = new DOMParser(),
            doc = parser.parseFromString(xmlBody);
        function nodeListToArray(list: NodeList): Element[] {
            const arr: Element[] = [];
            for (let i = 0; i < list.length; i++) {
                arr.push(list[i] as Element);
            }
            return arr;
        }
        const channelEl = doc.documentElement.getElementsByTagName('channel')[0]!!,
            // @ts-ignore
            nextPageEl = nodeListToArray(channelEl.getElementsByTagName('atom:link')).find(el => el.getAttribute('rel') === 'next')!!,
            nextPageUrl = nextPageEl.getAttribute('href')!!;
        return {
            nextPageUrl: nextPageUrl,
            // @ts-ignore
            items: nodeListToArray(channelEl.getElementsByTagName('item')).map(itemEl => {
                const contentEl = itemEl.getElementsByTagName('media:content')[0];
                return {
                    title: itemEl.getElementsByTagName('title')[0]!!.textContent!!,
                    url: itemEl.getElementsByTagName('link')[0]!!.textContent!!,
                    publishDate: new Date(itemEl.getElementsByTagName('pubDate')[0]!!.textContent!),
                    rating: itemEl.getElementsByTagName('media:rating')[0]!!.textContent!! as DeviantArtRSSRating,
                    // @ts-ignore
                    thumbnails: nodeListToArray(itemEl.getElementsByTagName('media:thumbnail')).map(this.parseRssImage),
                    content: contentEl && this.parseRssImage(contentEl) || undefined
                }
            })
        };
    }

    convertOAuthDeviation(deviation: DeviantArtAPIDeviation, metadata?: DeviantArtAPIDeviationMetadata): Post | null {
        const thumb = deviation.thumbs?.reduce((a: DeviantArtAPIImage | null, x) => a && a.width > x.width ? a : x, null);
        const content: (DeviantArtAPIImage | DeviantArtAPIVideo) & {
            width?: number,
            height?: number
        } | undefined | null = deviation.content || deviation.videos?.reduce((a: DeviantArtAPIVideo | null, x) => a && a.filesize > x.filesize ? a : x, null);
        if (!content) {
            return null;
        }
        return {
            type: 'POST',
            url: deviation.url,
            name: deviation.deviationid,
            title: deviation.title,
            author: deviation.author?.username,
            tags: metadata?.tags?.map(t => t.tag_name) || [],
            previewUrl: deviation.preview?.src,
            thumbUrl: thumb?.src,
            thumbWidth: thumb?.width,
            thumbHeight: thumb?.height,
            fileUrl: content.src,
            width: content.width,
            height: content.height,
            rating: !deviation.is_mature ? 's' : 'e',
            rawData: deviation
        }
    }

    convertOAuthPage(page: DeviantArtAPIPage, metaData: DeviantArtAPIDeviationMetadataPage): Page {
        return {
            nextPageToken: page.has_more ? page.next_offset?.toString() : undefined,
            content: page.results
                .map(d => this.convertOAuthDeviation(d, metaData.metadata.find(m => m.deviationid === d.deviationid)))
                .filter(post => post)
                .map(post => post as Post)
        };
    }

    async fetchMetadata(token: AccessToken, page: DeviantArtAPIPage) {
        const url = `https://www.deviantart.com/api/v1/oauth2/deviation/metadata?${page.results.map(p => `deviationids[]=${p.deviationid}`).join('&')}&access_token=${token.token.access_token}`;
        return await this.http.getJson<DeviantArtAPIDeviationMetadataPage>(url);
    }

    async getPage(page: string | number): Promise<Page> {
        if (DeviantArtService.OAUTH2_CLIENT) {
            const oauthToken = await this.ensureOauthToken(),
                pageUrl = this.getOAuthPageUrl(page, oauthToken),
                pageData = await this.http.getJson<DeviantArtAPIPage>(pageUrl),
                metaData = await this.fetchMetadata(oauthToken, pageData);
            return this.convertOAuthPage(pageData, metaData);
        } else {
            const pageUrl = this.getRssPageUrl(page),
                rssBody = await this.http.get(pageUrl),
                rssPage = this.parseXmlResponse(rssBody);
            return this.convertRssPage(rssPage);
        }
    }

    protected getRssPageUrl(pageToken: string | number): string {
        if (_.isString(pageToken)) {
            return pageToken;
        }
        return `https://backend.deviantart.com/rss.xml?type=deviation&order=${DeviantArtRSSOrderTypes.popularToday}`;
    }

    protected getOAuthPageUrl(pageToken: string | number, token: AccessToken): string {
        const offset = _.isString(pageToken) ? pageToken : 0;
        return `https://www.deviantart.com/api/v1/oauth2/browse/popular?offset=${offset}&limit=40&timerange=24hr&access_token=${token.token.access_token}${this.getMatureFilterRequestString(true)}`;
    }

    async getSubservice(name: string): Promise<Service<any> | null> {
        if (name === 'dailydeviation') {
            return new DeviantArtDailyDeviationService(this.options);
        }
        if (name.startsWith('search:')) {
            return new DeviantArtSearchService(this.options, name.substring('search:'.length));
        }
        if (name.startsWith('topic:')) {
            return new DeviantArtTopicService(this.options, name.substring('topic:'.length));
        }
        return this.subservices[name];
    }

    async getHomeScreenItems(): Promise<ServiceHomeScreenItem[] | null> {
        let services: {
           title: string,
           service: Service<any>
        }[];
        if (DeviantArtService.OAUTH2_CLIENT) {
            services = [{
                title: 'Daily Deviations',
                service: this.subservices.dailyDeviations
            }, {
                title: 'Popular today',
                service: this
            }];
        } else {
            services = [{
                title: 'Popular this month',
                service: this.subservices.thisMonth
            }, {
                title: 'Digital art - Popular this week',
                service: this.subservices.digitalArtThisWeek
            }, {
                title: 'Digital art - Popular today',
                service: this.subservices.digitalArtToday
            }, {
                title: 'Popular today',
                service: this
            }];
        }
        return await Promise.all(services.map(async serviceEntry => {
            const page = await serviceEntry.service.getPage(1);
            return {
                title: serviceEntry.title,
                galleryRelPath: serviceEntry.service === this ? '' : '/' + serviceEntry.service.name
            }
        }));
    }

    async getAutocompleteSuggestions(query: string): Promise<SearchAutocompleteSuggestion[] | null> {
        query = query.split(':')[0];
        if (DeviantArtService.OAUTH2_CLIENT) {
            const token = await this.ensureOauthToken(),
                queryUrl = `https://www.deviantart.com/api/v1/oauth2/browse/tags/search?tag_name=${query}&access_token=${token.token.access_token}${this.getMatureFilterRequestString()}`,
                resultPage = await this.http.getJson<DeviantArtAPITagSearchPage>(queryUrl);
            return resultPage.results.map(tag => ({
                category: 'general',
                name: tag.tag_name
            }))
        } else {
            return null;
        }
    }

    registerOmnibox(omnibox: OmniboxApi, serviceName: string) {
        omnibox.registerContext({
            name: 'DeviantArt Search',
            type: 'SERVICE',
            serviceName: serviceName,
            aliases: ['da'],
            queryCallback: async query => {
                const suggestions = await this.getAutocompleteSuggestions(query);
                const results = _.take(suggestions, 7).map(s => ({
                    contextName: 'DeviantArt search',
                    contextType: 'SERVICE',
                    resultName: s.name,
                    resultType: 'SERVICE_PAGE',
                    pagePath: `/${serviceName}/search:${s.name}`
                } as OmniboxResult));
                if (!suggestions?.find(s => s.name === query)) {
                    results.unshift({
                        contextName: 'DeviantArt search',
                        contextType: 'SERVICE',
                        resultName: query,
                        resultType: 'SERVICE_PAGE',
                        pagePath: `/${serviceName}/search:${query}`
                    });
                }
                return results;
            }
        });
    }

    async getSubServicePathByDescriptor(descriptor: DeviantArtServiceDescriptor): Promise<string | null> {
        switch (descriptor.type) {
            case 'dailydeviation': {
                return 'dailydeviation';
            }
            case 'search': {
                return ['search', descriptor.query, descriptor.order].filter(x => !_.isEmpty(x)).join(':');
            }
            case 'topic': {
                return ['topic', descriptor.topic].filter(x => !_.isEmpty(x)).join(':');
            }
        }
        return null;
    }


    getDescriptorConfigDescriptions() {
        const search: ConfigPropertyDescription<Omit<DeviantArtSearchServiceDescriptor, 'serviceName' | 'type'>> = {
            label: 'Search',
            type: 'object',
            children: {
                query: {
                    type: 'string',
                    label: 'Search query'
                }
            }
        }, dailydeviation: ConfigPropertyDescription<Omit<DeviantArtDailyDeviationServiceDescriptor, 'serviceName' | 'type'>> = {
            label: 'Daily Deviations',
            type: 'object',
            children: {}
        }, topic: ConfigPropertyDescription<Omit<DeviantArtTopicServiceDescriptor, 'serviceName' | 'type'>> = {
            label: 'Topic',
            type: 'object',
            children: {
                topic: {
                    type: 'string',
                    label: 'Topic'
                }
            }
        };

        return {
            discriminator: 'type',
            descriptions: {
                search,
                dailydeviation,
                topic
            }
        };
    }

}

export class DeviantArtSearchService extends DeviantArtService {
    private readonly query: string;
    private readonly oauthOrder?: string;
    private readonly order?: number;

    constructor(options: OptionsObject<DeviantArtServiceOptions>, query: string, order?: DeviantArtRSSOrderType) {
        super(options);
        [this.query, this.oauthOrder = 'popular'] = query.split(':');
        this.order = order && DeviantArtRSSOrderTypes[order];
        this.capabilities = _.without(this.capabilities, 'SEARCH', 'HOME_SCREEN');
        this.name = 'search';
    }

    protected initSubservices() {

    }

    protected getRssPageUrl(pageToken: string | number): string {
        if (_.isString(pageToken)) {
            return pageToken;
        }
        return `https://backend.deviantart.com/rss.xml?type=deviation&q=${encodeURIComponent(this.query)}${this.order ? `&order=${this.order}` : ''}`;
    }

    protected getOAuthPageUrl(pageToken: string | number, token: AccessToken): string {
        const offset = _.isString(pageToken) ? pageToken : 0;
        return `https://www.deviantart.com/api/v1/oauth2/browse/${this.oauthOrder}?offset=${offset}${this.oauthOrder === 'popular' ? '&timerange=alltime' : ''}&limit=40&q=${this.query}&access_token=${token.token.access_token}${this.getMatureFilterRequestString(true)}`;
    }

    async getSubservice(name: string): Promise<Service<any> | null> {
        return null;
    }
}

export class DeviantArtDailyDeviationService extends DeviantArtService {
    constructor(options: OptionsObject<DeviantArtServiceOptions>) {
        super(options);
        this.capabilities = _.without(this.capabilities, 'SEARCH', 'HOME_SCREEN');
        this.name = 'dailydeviation';
    }

    protected initSubservices() {

    }

    protected getRssPageUrl(pageToken: string | number): string {
        throw new Error('Not supported');
    }

    protected getOAuthPageUrl(pageToken: string | number, token: AccessToken): string {
        return `https://www.deviantart.com/api/v1/oauth2/browse/dailydeviations?access_token=${token.token.access_token}${this.getMatureFilterRequestString()}`;
    }

    async getSubservice(name: string): Promise<Service<any> | null> {
        return null;
    }
}

export class DeviantArtTopicService extends DeviantArtService {
    private readonly topic: string;

    constructor(options: OptionsObject<DeviantArtServiceOptions>, topic: string) {
        super(options);
        this.capabilities = _.without(this.capabilities, 'SEARCH', 'HOME_SCREEN');
        this.name = 'topic';
        this.topic = topic;
    }

    protected initSubservices() {

    }

    protected getRssPageUrl(pageToken: string | number): string {
        throw new Error('Not supported');
    }

    protected getOAuthPageUrl(pageToken: string | number, token: AccessToken): string {
        return `https://www.deviantart.com/api/v1/oauth2/browse/topic?topic=${this.topic}${pageToken ? `&offset=${pageToken}` : ''}&access_token=${token.token.access_token}${this.getMatureFilterRequestString()}`;
    }

    async getSubservice(name: string): Promise<Service<any> | null> {
        return null;
    }
}