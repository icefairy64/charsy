import {
    Page,
    Post,
    SearchAutocompleteSuggestion,
    Service,
    ServiceBaseOptions,
    ServiceCapability,
    Tag
} from './service';
import { getHttpClient, HttpClient } from '../http/client';
import _ from 'lodash';
import { OptionsObject } from '@/electron/options/options';

interface SankakuV2ServiceOptions extends ServiceBaseOptions {
    auth?: {
        login: string,
        apiKey: string
    },
    cooldown?: number
}

interface SankakuV2Post {
    id?: number,
    md5: string,
    file_url: string,
    sample_url: string,
    preview_url: string,
    file_type: string,
    rating: 's' | 'q' | 'e',
    tags: SankakuV2Tag[],
    width?: number,
    height?: number,
    sample_width?: number,
    sample_height?: number,
    preview_width?: number,
    preview_height?: number,
}

interface SankakuV2Tag {
    id: number,
    name: string,
    name_en?: string,
    name_jp?: string,
    type: number,
    locale: string,
    rating: 's' | 'q' | 'e' | null
}

type SankakuV2Page = {
    meta: {
        next?: string,
        prev?: string
    }
    data: Array<SankakuV2Post>
};

interface SankakuV2AutocompleteSuggestion {
    id: number,
    type: number,
    name: string,
    name_en?: string,
    name_ja?: string,
    post_count: number,
    pool_count: number
}


const tagTypeMap: Record<number, string> = {
    0: 'general',
    1: 'artist',
    3: 'copyright',
    4: 'character',
    5: 'relationship',
    8: 'meta',
    9: 'extreme_content'
};

export class SankakuV2Service extends Service<SankakuV2ServiceOptions> {
    name: string = 'sankaku-v2';

    private readonly http: HttpClient;

    protected static URL_ROOT = 'https://capi-v2.sankakucomplex.com';

    public capabilities: Array<ServiceCapability> = ['AUTH', 'CONFIGURATION', 'SEARCH'];

    constructor(options: OptionsObject<SankakuV2ServiceOptions>) {
        super(options);
        this.http = getHttpClient(undefined, options.currentValue?.cooldown || 500);
    }

    async init(): Promise<void> {

    }

    protected getPageUrl(page: number | string): string {
        let nextPageToken;
        if (_.isString(page)) {
            nextPageToken = page;
        }
        return `/posts/keyset${nextPageToken != null ? `?next=${nextPageToken}` : ''}`;
    }

    protected convertPost(postData: SankakuV2Post): Post {
        const thumbScale = 150 / Math.max(postData.width || 150, postData.height || 150);
        const convertTags = (tags: SankakuV2Tag[]) => {
            return tags.map(tag => ({
                category: tagTypeMap[tag.type],
                name: tag.name
            }));
        }
        return {
            type: 'POST',
            name: postData.id?.toString() || 'unknown',
            url: `${SankakuV2Service.URL_ROOT}/posts/${postData.id}`,
            thumbUrl: postData.preview_url,
            previewUrl: postData.sample_url,
            fileUrl: postData.file_url,
            tags: convertTags(postData.tags),
            thumbWidth: postData.preview_width || postData.width && postData.width * thumbScale,
            thumbHeight: postData.preview_height || postData.height && postData.height * thumbScale,
            width: postData.width,
            height: postData.height,
            rating: postData.rating || undefined,
            rawData: postData
        }
    }

    async getPage(page: number | string): Promise<Page> {
        const url = SankakuV2Service.URL_ROOT + this.getPageUrl(page);
        const json = await this.http.get(url);
        const pageData: SankakuV2Page = JSON.parse(json);
        return {
            nextPageToken: pageData.meta.next,
            content: pageData.data.map(this.convertPost.bind(this))
        };
    }

    async getSubservices(): Promise<Array<Service<any>>> {
        return [];
    }

    async getSubservice(name: string): Promise<Service<any> | null> {
        if (name.startsWith('search:')) {
            this.subservices[name] = new SankakuV2SearchService(this.options, name.substring('search:'.length));
        }
        return this.subservices[name];
    }

    async getAutocompleteSuggestions(query: string): Promise<SearchAutocompleteSuggestion[]> {
        const json = await this.http.get(`${SankakuV2Service.URL_ROOT}/tags/autosuggestCreating?lang=en&tag=${encodeURIComponent(query)}&target=post&show_meta=1`);
        const data = JSON.parse(json) as SankakuV2AutocompleteSuggestion[];
        return data.map(s => ({
            category: tagTypeMap[s.type],
            label: s.name,
            name: s.name,
            postCount: s.post_count
        }));
    }

    doesPostBelong(post: Post): boolean {
        return post.url.startsWith(SankakuV2Service.URL_ROOT);
    }
}

export class SankakuV2SearchService extends SankakuV2Service {
    private readonly query: string;

    constructor(options: OptionsObject<SankakuV2ServiceOptions>, query: string) {
        super(options);
        this.query = query;
        this.capabilities = _.without(this.capabilities, 'SEARCH');
    }

    protected getPageUrl(page: number | string): string {
        let nextPageToken;
        if (_.isString(page)) {
            nextPageToken = page;
        }
        return `/posts/keyset?tags=${encodeURIComponent(this.query)}${nextPageToken != null ? `&next=${nextPageToken}` : ''}`;
    }

    async getSubservice(name: string): Promise<Service<any> | null> {
        return null;
    }
}