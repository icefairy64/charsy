import {
    Page,
    Post,
    Service,
    ServiceBaseOptions,
    ServiceCapability,
    ServiceDescriptor
} from '@/electron/service/service';
import { ServiceDescriptorConfigInfo, ServiceUserInfo } from '@/electron/api/service-symbols';
import { PostCollectionSyncDescriptor } from '@/electron/api/storage-symbols';
import { getHttpClient, HttpClient } from '@/electron/http/client';
import { OptionsObject } from '@/electron/options/options';
import { getQueryString } from '@/electron/util/url-util';
import _ from 'lodash';
import OmniboxApi from '@/electron/api/omnibox';
import { OmniboxResult } from '@/electron/api/omnibox-symbols';
import { ConfigPropertyDescription } from '@/electron/config/config';

export interface RedGifsServiceOptions extends ServiceBaseOptions {}

type RedGifsSearchOrder = 'trending' | 'top7' | 'top28' | 'best' | 'latest' | 'oldest'

interface RedGifsSearchDescriptor extends ServiceDescriptor {
    type: 'search',
    query: string,
    order: RedGifsSearchOrder
}

type RedGifsDescriptor = RedGifsSearchDescriptor

interface RedGifsPost {
    id: string,
    width: number,
    height: number,
    published: boolean,
    verified: boolean,
    hasAudio: boolean,
    duration: number,
    type: number,
    tags: string[]
    urls: {
        thumbnail: string,
        poster: string,
        sd: string,
        hd: string
    }
}

interface RedGifsPage {
    gifs: RedGifsPost[],
    page: number,
    pages: number,
    total: number
}

export class RedGifsService extends Service<RedGifsServiceOptions, any, ServiceUserInfo, PostCollectionSyncDescriptor, RedGifsDescriptor> {
    name = 'redgifs';
    capabilities: Array<ServiceCapability> = ['HOME_SCREEN', 'CONFIGURATION', 'SEARCH'];

    protected URL_ROOT: string = 'https://www.redgifs.com';
    private http: HttpClient;

    constructor(options: OptionsObject<RedGifsServiceOptions>) {
        super(options);
        this.http = getHttpClient();
    }

    init(): Promise<void> {
        return Promise.resolve(undefined);
    }

    doesPostBelong(post: Post): boolean {
        return post.url.startsWith(this.URL_ROOT);
    }

    private convertTag(tag: string) {
        return tag.split(' ').map(w => w.toLocaleLowerCase()).join('_');
    }

    protected convertPost(post: RedGifsPost): Post {
        return {
            type: 'POST',
            name: post.id,
            url: `${this.URL_ROOT}/watch/${post.id}`,
            width: post.width,
            height: post.height,
            fileUrl: post.urls.hd,
            previewUrl: post.urls.poster,
            thumbUrl: post.urls.thumbnail,
            tags: post.tags.map(t => this.convertTag(t)),
            rating: 'e'
        }
    }

    protected getCommonRequestParams() {
        return {
            count: 80
        };
    }

    protected getPageUrl(page?: number | string): string {
        return 'https://api.redgifs.com/v2/gifs/search' + getQueryString({
            ...this.getCommonRequestParams(),
            search_text: '',
            order: 'trending',
            page: page
        });
    }

    convertPage(page: RedGifsPage): Page {
        return {
            content: page.gifs.map(p => this.convertPost(p)),
            nextPageToken: page.page < page.pages ? (page.page + 1).toString() : undefined,
            lastPage: page.page
        };
    }

    async getPage(page: number | string | undefined): Promise<Page> {
        const url = this.getPageUrl(page),
            data = await this.http.getJson<RedGifsPage>(url);
        return this.convertPage(data);
    }

    async getSubservice(name: string): Promise<Service<any> | null> {
        if (name.startsWith('search:')) {
            const [query, order] = name.substring('search:'.length).split('|||');
            return new RedGifsSearchService(this.options, query, order as RedGifsSearchOrder);
        }
        return this.subservices[name];
    }

    async getSubServicePathByDescriptor(descriptor: RedGifsDescriptor): Promise<string | null> {
        switch (descriptor.type) {
            case 'search': return 'search:' + [descriptor.query, descriptor.order].join('|||');
            default: return null;
        }
    }

    registerOmnibox(omnibox: OmniboxApi, serviceName: string) {
        omnibox.registerContext({
            name: 'RedGifs search',
            type: 'SERVICE',
            serviceName: serviceName,
            aliases: ['rg'],
            queryCallback: async query => {
                const term = query.split(' ')[0].split('_').map(_.upperFirst).join(' '),
                    results: OmniboxResult[] = [];

                const pagePath = await this.getSubServicePathByDescriptor({
                    serviceName: this.name,
                    type: 'search',
                    query: term,
                    order: 'latest'
                });

                results.push({
                    contextName: 'RedGifs search',
                    contextType: 'SERVICE',
                    resultName: term,
                    resultType: 'SERVICE_PAGE',
                    pagePath: `/${serviceName}/${pagePath}`
                });

                return results;
            }
        })
    }

    getDescriptorConfigDescriptions(): ServiceDescriptorConfigInfo<RedGifsDescriptor> | undefined {
        const search: ConfigPropertyDescription<Omit<RedGifsSearchDescriptor, 'serviceName' | 'type'>> = {
            label: 'Search',
            type: 'object',
            children: {
                query: {
                    type: 'string',
                    label: 'Search query'
                },
                order: {
                    type: 'string',
                    label: 'Sort by',
                    editor: {
                        type: 'combobox',
                        options: [{
                            label: 'Latest',
                            value: 'latest'
                        }, {
                            label: 'Trending',
                            value: 'trending'
                        }, {
                            label: 'Top of the week',
                            value: 'top7'
                        }, {
                            label: 'Top of the month',
                            value: 'top28'
                        }, {
                            label: 'Best',
                            value: 'best'
                        }]
                    }
                }
            }
        }

        return {
            discriminator: 'type',
            descriptions: {
                search
            }
        }
    }
}

class RedGifsSearchService extends RedGifsService {
    private readonly query: string;
    private readonly order: RedGifsSearchOrder;

    constructor(options: OptionsObject<RedGifsServiceOptions>, query: string, order: RedGifsSearchOrder) {
        super(options);
        this.query = query;
        this.order = order;
        this.capabilities = _.without(this.capabilities, 'SEARCH', 'HOME_SCREEN');
    }

    protected getPageUrl(page?: number | string): string {
        return 'https://api.redgifs.com/v2/gifs/search' + getQueryString({
            ...this.getCommonRequestParams(),
            search_text: this.query,
            order: this.order,
            page: page
        });
    }
}