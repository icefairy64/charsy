import {
    Page,
    Post,
    PostRating, SearchAutocompleteSuggestion,
    Service,
    ServiceBaseOptions,
    ServiceCapability, ServiceDescriptor,
    Tag
} from '@/electron/service/service';
import { ServiceUserInfo } from '@/electron/api/service-symbols';
import { PostCollectionSyncDescriptor } from '@/electron/api/storage-symbols';
import { OptionsObject } from '@/electron/options/options';
import { getHttpClient, HttpClient } from '@/electron/http/client';
import { getQueryString } from '@/electron/util/url-util';
import _ from 'lodash';
import OmniboxApi from '@/electron/api/omnibox';
import { OmniboxResult } from '@/electron/api/omnibox-symbols';

export interface KonachanServiceOptions extends ServiceBaseOptions {

}

interface KonachanSearchDescriptor extends ServiceDescriptor {
    type: 'search',
    query: string
}

type KonachanDescriptor = KonachanSearchDescriptor;

interface KonachanPost {
    id: number,
    author: string,
    tags: string,
    source?: string,
    preview_width: number,
    preview_height: number,
    preview_url: string,
    sample_width: number,
    sample_height: number,
    sample_url: string,
    width: number,
    height: number,
    file_url: string,
    rating: PostRating
}

interface KonachanTag {
    name: string,
    type: KonachanTagType,
    count: number
}

type KonachanTagType = 0 | 1 | 3 | 4;

const konachanTagTypeMapping: Record<KonachanTagType, string> = {
    0: 'general',
    1: 'artist',
    3: 'copyright',
    4: 'character'
};

export class KonachanService extends Service<KonachanServiceOptions, any, ServiceUserInfo, PostCollectionSyncDescriptor, KonachanDescriptor> {
    name = 'konachan';
    capabilities: ServiceCapability[] = ['HOME_SCREEN', 'CONFIGURATION', 'SEARCH'];

    protected URL_ROOT: string = 'https://konachan.net';
    private http: HttpClient;

    constructor(options: OptionsObject<KonachanServiceOptions>) {
        super(options);
        this.http = getHttpClient();
    }

    init(): Promise<void> {
        return Promise.resolve(undefined);
    }

    doesPostBelong(post: Post): boolean {
        return post.url.startsWith(this.URL_ROOT);
    }

    protected getPageUrl(page?: number | string): string {
        return '/post.json' + getQueryString({
            ...this.getCommonRequestParams(),
            page
        });
    }

    protected getCommonRequestParams() {
        return {
            limit: 50
        };
    }

    convertTagString(tagString: string): Tag[] {
        return tagString.split(' ');
    }

    convertPost(post: KonachanPost): Post {
        return {
            type: 'POST',
            url: `${this.URL_ROOT}/post/show/${post.id}`,
            width: post.width,
            height: post.height,
            fileUrl: post.file_url,
            previewUrl: post.sample_url,
            thumbWidth: post.preview_width,
            thumbHeight: post.preview_height,
            thumbUrl: post.preview_url,
            rating: post.rating,
            rawData: post,
            name: post.id.toString(),
            tags: this.convertTagString(post.tags)
        };
    }

    convertPage(page: number | string | undefined, posts: KonachanPost[]): Page {
        return {
            content: posts.map(p => this.convertPost(p)),
            nextPageToken: posts.length > 0 ? (+(page ?? 1) + 1).toString() : undefined
        };
    }

    async getPage(page?: number | string): Promise<Page> {
        const relUrl = this.getPageUrl(page),
            data = await this.http.getJson<KonachanPost[]>(`${this.URL_ROOT}${relUrl}`);
        return this.convertPage(page, data);
    }

    async getSubservice(name: string): Promise<Service<any> | null> {
        if (name.startsWith('search:')) {
            const [query] = name.substring('search:'.length).split('|||');
            return new KonachanSearchService(this.options, query);
        }
        return this.subservices[name];
    }

    async getSubServicePathByDescriptor(descriptor: KonachanDescriptor): Promise<string | null> {
        switch (descriptor.type) {
            case 'search': return 'search:' + [descriptor.query].join('|||');
            default: return null;
        }
    }

    async getAutocompleteSuggestions(query: string): Promise<SearchAutocompleteSuggestion[] | null> {
        const url = `${this.URL_ROOT}/tag.json?name=${query}&order=count`,
            tags = await this.http.getJson<KonachanTag[]>(url);
        return tags.sort((a, b) => b.count - a.count)
            .map(t => ({
                category: konachanTagTypeMapping[t.type],
                name: t.name,
                postCount: t.count
            }));
    }

    registerOmnibox(omnibox: OmniboxApi, serviceName: string) {
        omnibox.registerContext({
            name: 'Konachan search',
            type: 'SERVICE',
            serviceName: serviceName,
            aliases: ['kc'],
            queryCallback: async query => {
                const terms = query.split(' '),
                    lastTerm = _.last(terms);
                if (!lastTerm || lastTerm.length < 3) {
                    return [];
                }

                const results: OmniboxResult[] = [],
                    autocomplete = await this.getAutocompleteSuggestions(query);

                for (const s of autocomplete || []) {
                    const fullTerms = [..._.take(terms, terms.length - 1), s.name];
                    const pagePath = await this.getSubServicePathByDescriptor({
                        serviceName: this.name,
                        type: 'search',
                        query: fullTerms.join(' ')
                    });
                    if (pagePath) {
                        results.push({
                            contextName: 'Konachan search',
                            contextType: 'SERVICE',
                            resultName: fullTerms.join(' '),
                            resultType: 'SERVICE_PAGE',
                            pagePath: `/${serviceName}/${pagePath}`
                        });
                    }
                }

                return results;
            }
        });
    }
}

class KonachanSearchService extends KonachanService {
    private readonly query: string;

    constructor(options: OptionsObject<KonachanServiceOptions>, query: string) {
        super(options);
        this.query = query;
        this.capabilities = _.without(this.capabilities, 'SEARCH', 'HOME_SCREEN');
    }

    protected getPageUrl(page?: number | string): string {
        return '/post.json' + getQueryString({
            ...this.getCommonRequestParams(),
            page,
            tags: this.query
        });
    }
}