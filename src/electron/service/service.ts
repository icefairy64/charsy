import { getNormalizedTagName, getShortTagName } from '@/electron/util/post-util';
import { OptionsObject } from '@/electron/options/options';
import { distinctUntilChanged, filter, map, throttleTime } from 'rxjs/operators';
import _ from 'lodash';
import { ServiceDescriptorConfigInfo, ServiceUserInfo } from '@/electron/api/service-symbols';
import { PostCollection, PostCollectionSyncDescriptor } from '@/electron/api/storage-symbols';
import OmniboxApi from '@/electron/api/omnibox';
import { OmniboxResult } from '@/electron/api/omnibox-symbols';
import { Logger } from '@/electron/log/logger';
import { createLogger } from '@/logging';
import { ConfigDescription, ConfigPropertyDescription } from '@/electron/config/config';

export interface Post {
    type: 'POST',

    name: string,
    title?: string,
    author?: string,
    url: string,
    thumbUrl?: string,
    thumbWidth?: number,
    thumbHeight?: number,
    rawData?: object,

    previewUrl?: string,
    fileUrl: string,
    tags: Array<Tag>,
    score?: number,
    width?: number,
    height?: number,
    rating?: PostRating,
    mediaType?: string
}

export type PostRating = 's' | 'q' | 'e';

export type Tag = string | {
    category: string,
    name: string,
    searchName?: string
};

export interface PostDirectory {
    type: 'POST_DIRECTORY',

    name: string,
    url: string,
    thumbUrl?: string,
    thumbWidth?: number,
    thumbHeight?: number,
    rawData?: object
}

export type PageItem = Post | PostDirectory;

export interface Page {
    content: Array<PageItem>;
    nextPageToken?: string;
    lastPage?: number;
}

export interface SearchAutocompleteSuggestion {
    category: string,
    name: string,
    label?: string,
    postCount?: number,
    omniboxResult?: OmniboxResult
}

export interface ServiceHomeScreenItem {
    title: string,
    items?: PageItem[],
    galleryRelPath?: string
}

export type ServiceCapability =
    'CONFIGURATION' |
    'AUTH' |
    'SEARCH' |
    'HOME_SCREEN' |
    'COLLECTION_SYNC';

export interface ServiceBaseOptions {
    tagBlacklist?: string[],
    ratingFilter?: PostRating[],
    collectionSyncEnabled?: boolean,
    homeScreenItems?: ServiceHomeScreenItemOptions[]
}

export interface ServiceDescriptor {
    serviceName: string
}

export interface ServiceHomeScreenItemOptions {
    title: string,
    descriptor: string | ServiceDescriptor
}

export abstract class Service<
        TOptions extends ServiceBaseOptions,
        TCredentials = any,
        TUserInfo extends ServiceUserInfo = ServiceUserInfo,
        TCollectionSyncDescriptor extends PostCollectionSyncDescriptor = PostCollectionSyncDescriptor,
        TServiceDescriptor extends ServiceDescriptor = ServiceDescriptor,
    > {
    protected options: OptionsObject<TOptions>;
    protected subservices: Record<string, Service<any>>

    public abstract name: string;
    public abstract capabilities: Array<ServiceCapability>;

    protected readonly logger: Logger;

    public authenticated: boolean = false;

    protected constructor(options: OptionsObject<TOptions>) {
        this.options = options;
        this.subservices = {};
        this.logger = createLogger(this.constructor.name);
        if (this.isAuthSupported()) {
            this.getAuthOptions().getValueObservable()
                .pipe(
                    filter(c => c != null),
                    distinctUntilChanged(_.isEqual),
                    throttleTime(1000, undefined, {
                        trailing: true
                    }),
                    distinctUntilChanged(_.isEqual),
                    map(c => c!)
                )
                .subscribe(this.authenticate.bind(this));
        }
    }

    protected updateOptions(options: TOptions): void {
        this.options.setValue(options);
    }

    abstract init(): Promise<void>;
    abstract getPage(page?: number | string): Promise<Page>;

    async getSubservices() {
        return Object.values(this.subservices);
    }

    abstract getSubservice(name: string): Promise<Service<any> | null>;

    getSubServicePathByDescriptor(descriptor: TServiceDescriptor): Promise<string | null> {
        return Promise.reject('Not implemented');
    }

    getAutocompleteSuggestions(query: string): Promise<SearchAutocompleteSuggestion[] | null> {
        return Promise.resolve(null);
    }

    async getHomeScreenItems(): Promise<ServiceHomeScreenItem[] | null> {
        const homeScreenItems = this.options.getChild('homeScreenItems')?.currentValue;
        if (homeScreenItems) {
            return (await Promise.all(homeScreenItems.map(async i => {
                const path = _.isString(i.descriptor) ? i.descriptor : await this.getSubServicePathByDescriptor(i.descriptor as TServiceDescriptor);
                if (!path) {
                    throw new Error(`Unsupported descriptor: ${i.descriptor}`);
                }
                return {
                    title: i.title,
                    galleryRelPath: `/${path}`
                };
            }).filter(i => i != null))) as ServiceHomeScreenItem[];
        }
        return null;
    }

    filterPageContent(items: PageItem[]) {
        return items.filter(item => {
            if (item.type !== 'POST') {
                return true;
            }
            if (!item.fileUrl) {
                this.logger.warn(`Post ${item.url} does not have a file URL`);
                return false;
            }
            if (this.options.currentValue?.ratingFilter) {
                if (!item.rating || !this.options.currentValue.ratingFilter.includes(item.rating)) {
                    return false;
                }
            }
            if (this.options.currentValue?.tagBlacklist) {
                const blacklistedTags = item.tags
                    .filter(tag =>
                        this.options.currentValue?.tagBlacklist?.includes(getShortTagName(tag))
                        || this.options.currentValue?.tagBlacklist?.includes(getNormalizedTagName(tag)));
                if (blacklistedTags.length > 0) {
                    return false;
                }
            }

            return true;
        });
    }

    isAuthSupported() {
        return this.capabilities?.includes('AUTH') ?? false;
    }

    protected getAuthOptions(): OptionsObject<TCredentials> {
        throw new Error('Not supported');
    }

    async authenticate(credentials: TCredentials) {
        throw new Error('Not supported');
    }

    async getAuthenticatedUserInfo(): Promise<TUserInfo | null> {
        throw new Error('Not supported');
    }

    abstract doesPostBelong(post: Post): boolean;

    isCollectionSyncSupported() {
        return this.capabilities?.includes('COLLECTION_SYNC') ?? false;
    }

    isCollectionSyncEnabled() {
        return this.isCollectionSyncSupported() && (this.options.getChild('collectionSyncEnabled')?.currentValue ?? false);
    }

    async getUserCollections(): Promise<TCollectionSyncDescriptor[]> {
        throw new Error('Not supported');
    }

    async getUserCollectionPage(collectionId: string, page?: string): Promise<Page> {
        throw new Error('Not supported');
    }

    async addPostToUserCollection(collectionId: string, post: Post) {
        throw new Error('Not supported');
    }

    async removePostFromUserCollection(collectionId: string, post: Post) {
        throw new Error('Not supported');
    }

    registerOmnibox(omnibox: OmniboxApi, serviceName: string) {

    }

    getDescriptorConfigDescriptions(): ServiceDescriptorConfigInfo<TServiceDescriptor> | undefined {
        return undefined;
    }

    async destroy(): Promise<void> {

    }
}