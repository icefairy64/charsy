import {
    Page,
    Post,
    Service,
    ServiceBaseOptions,
    ServiceCapability, ServiceDescriptor,
    ServiceHomeScreenItem
} from '@/electron/service/service';
import { OptionsObject } from '@/electron/options/options';
import { getHttpClient, HttpClient, registerSiteHeaderHandler } from '@/electron/http/client';
import { v4 } from 'uuid';
import _, { words } from 'lodash';
import OmniboxApi from '@/electron/api/omnibox';
import { ServiceUserInfo } from '@/electron/api/service-symbols';
import { PostCollectionSyncDescriptor } from '@/electron/api/storage-symbols';
import { Deferred } from '@/electron/util/deferred';
import { getWordUtil, WordUtil } from '@/electron/util/words';
import { ConfigDescription, ConfigPropertyDescription } from '@/electron/config/config';

interface RedditServiceOptions extends ServiceBaseOptions {
    oauth2?: {
        clientId: string
    },
    accessToken?: AccessToken,
    deviceId?: string
}

interface RedditSubredditServiceDescriptor extends ServiceDescriptor {
    thing: 'subreddit',
    url: string,
    sorting?: RedditSorting
}

type RedditServiceDescriptor = RedditSubredditServiceDescriptor;

interface AccessToken {
    access_token: string,
    token_type: string,
    expires_in: number,
    scope: string
}

interface RedditTypes {
    t3: RedditLink,
    t5: RedditSubreddit,
    Listing: RedditListing<AbstractRedditThing>
}

interface RedditListing<T> {
    before?: string,
    after?: string
    children: T[]
}

interface AbstractRedditThing {
    id: string,
    kind: keyof RedditTypes
}

interface RedditThing<K extends keyof RedditTypes> extends AbstractRedditThing {
    kind: K,
    data: RedditTypes[K]
}

interface RedditLink {
    permalink: string,
    title?: string,
    over_18: boolean,
    thumbnail: string,
    thumbnail_width: number,
    thumbnail_height: number,
    url: string,
    preview?: {
        images?: {
            resolutions: RedditPreview[],
            source: RedditPreview
        }[]
    },
    media?: {
        reddit_video?: {
            fallback_url: string,
            width: number,
            height: number,
            hls_url: string,
            dash_url: string
        },
        oembed?: {
            thumbnail_url?: string,
            html: string
        }
    }
}

interface RedditPreview {
    url: string,
    width: number,
    height: number
}

interface RedditSubreddit {
    display_name: string,
    title: string,
    url: string
}

type RedditSorting = 'hot' | 'new' | 'rising' | 'random' | 'top' | 'controversial' | string;

export class RedditService extends Service<RedditServiceOptions, any, ServiceUserInfo, PostCollectionSyncDescriptor, RedditServiceDescriptor> {
    name: string = 'reddit';
    capabilities: ServiceCapability[] = ['HOME_SCREEN', 'CONFIGURATION', 'SEARCH'];

    private readonly http: HttpClient;

    protected static URL_ROOT = 'https://www.reddit.com';
    protected static OAUTH_ROOT = 'https://oauth.reddit.com';
    protected static ACCESS_TOKEN: AccessToken;
    protected static DEVICE_ID: string;
    protected static WORD_UTIL: WordUtil;

    private static siteHanderInitialized: boolean = false;
    private accessTokenDeferred?: Deferred<unknown>;

    constructor(options: OptionsObject<RedditServiceOptions>) {
        super(options);
        this.http = getHttpClient({
            'User-Agent': 'desktop:charsy:0.1 (by /u/icefairy64)'
        });
        this.initSubservices();
        RedditService.initSiteHeaderHandler();
        RedditService.WORD_UTIL = getWordUtil();
    }

    async init(): Promise<void> {

    }

    protected initSubservices() {

    }

    private initDeviceId() {
        if (this.options.getChild('deviceId')?.currentValue == null) {
            const deviceId = v4();
            this.logger.info(`New device id: ${deviceId}`);
            this.options.setChildValue('deviceId', deviceId);
            RedditService.DEVICE_ID = deviceId;
        } else {
            RedditService.DEVICE_ID = this.options.getChild('deviceId')?.currentValue!!;
            this.logger.info(`Known device id: ${RedditService.DEVICE_ID}`);
        }
    }

    private async retrieveAccessToken(): Promise<AccessToken> {
        const clientId = this.options.getChild('oauth2')?.currentValue?.clientId;
        const data = await this.http.sendData(`${RedditService.URL_ROOT}/api/v1/access_token`, 'POST',
            `grant_type=https://oauth.reddit.com/grants/installed_client&device_id=${RedditService.DEVICE_ID}`, {
                Authorization: `Basic ${Buffer.from(`${clientId}:`).toString('base64')}`
            });
        return JSON.parse(data);
    }

    protected async ensureAccessToken(): Promise<string> {
        if (!this.accessTokenDeferred) {
            this.accessTokenDeferred = new Deferred();
        } else {
            await this.accessTokenDeferred;
        }
        if (RedditService.DEVICE_ID == null) {
            this.initDeviceId();
        }
        try {
            if (RedditService.ACCESS_TOKEN == null && this.options.getChild('accessToken') != null) {
                this.logger.info('Loaded token from options');
                RedditService.ACCESS_TOKEN = this.options.getChild('accessToken')?.currentValue!!;
            }
            if (RedditService.ACCESS_TOKEN == null || RedditService.ACCESS_TOKEN.expires_in <= (Date.now() / 1000 + 60)) {
                this.logger.info(`No access token present, requesting`);
                const promise = this.retrieveAccessToken();
                const handle = setInterval(() => this.logger.info(promise.toString()), 500);
                const token = await promise;
                clearInterval(handle);
                this.logger.info(`Received access token`);
                token.expires_in += Date.now() / 1000;
                RedditService.ACCESS_TOKEN = token;
                this.options.setChildValue('accessToken', token);
            }
            return RedditService.ACCESS_TOKEN.access_token;
        } catch (e) {
            this.logger.error(`Failed to receive access token`, e);
            throw e;
        } finally {
            this.accessTokenDeferred.resolve();
            delete this.accessTokenDeferred;
        }
    }

    protected convertPost(link: RedditThing<'t3'>): Post {
        const imagePreview = link.data.preview?.images && link.data.preview.images[0],
            previewThumb = imagePreview?.resolutions.find(x => Math.max(x.width, x.height) > 200) || imagePreview?.resolutions[0],
            previewMax = imagePreview?.source;
        let fileUrl = link.data.url;
        if (link.data.media?.reddit_video) {
            // Reddit Video
            fileUrl = link.data.media.reddit_video.fallback_url;
        } else if (link.data.url.match(/https:\/\/.*redgifs.com\//)) {
            // RedGifs post
            let slug;
            if (link.data.media?.oembed?.thumbnail_url) {
                slug = link.data.media.oembed.thumbnail_url.match(/https:\/\/.*redgifs.com\/([a-zA-Z]+)[.-].*/)?.[1];
            } else {
                slug = link.data.url.match(/https:\/\/www\.redgifs\.com\/watch\/(.+?)$/)?.[1];
                if (slug) {
                    const uncapitalizedSlug = slug;
                    this.logger.debug(`Trying to capitalize slug ${uncapitalizedSlug}`);
                    slug = RedditService.WORD_UTIL.capitalizeRedGifsSlug(slug);
                    if (!slug) {
                        this.logger.warn(`Failed to capitalize slug ${uncapitalizedSlug}`);
                    }
                }
            }
            if (slug) {
                fileUrl = `https://thumbs2.redgifs.com/${slug}.mp4`;
            }
        } else if (link.data.url.match(/https:\/\/.*imgur.com\/a\//) && link.data.media?.oembed?.thumbnail_url != null) {
            // Imgur album
            const firstImageUrl = link.data.media?.oembed.thumbnail_url.match(/(.*)(?:\?fb)/)?.[1];
            if (firstImageUrl) {
                fileUrl = firstImageUrl;
            }
        } else if (link.data.url.match(/https:\/\/.*imgur.com\/.+?\.gifv$/)) {
            // Imgur GIFV -> MP4
            fileUrl = link.data.url.replace('.gifv', '.mp4');
        } else if (link.data.url.match(/https:\/\/imgur.com\/[\w]+?$/)) {
            // Imgur single image
            fileUrl = link.data.url.replace('//imgur.com', '//i.imgur.com') + '.png';
        }
        return {
            type: 'POST',
            name: link.data.permalink,
            title: link.data.title,
            url: RedditService.URL_ROOT + link.data.permalink,
            thumbUrl: previewThumb?.url,
            thumbHeight: previewThumb?.height,
            thumbWidth: previewThumb?.width,
            previewUrl: previewMax?.url,
            fileUrl: fileUrl,
            width: previewMax?.width,
            height: previewMax?.height,
            rating: link.data.over_18 ? 'e' : 's',
            tags: [],
            rawData: link.data
        };
    }
    
    protected convertPage(listing: RedditListing<AbstractRedditThing>): Page {
        const posts = listing.children
            .filter(x => x.kind === 't3' && (x as RedditThing<'t3'>).data.preview?.images)
            .map(x => this.convertPost(x as RedditThing<'t3'>));
        return {
            content: posts,
            nextPageToken: listing.after
        };
    }

    protected getPageUrl(page?: number | string): string {
        return `${RedditService.OAUTH_ROOT}/top?t=day&raw_json=1${page ? '&after=' + page : ''}`;
    }

    doesPostBelong(post: Post): boolean {
        return post.url.startsWith(RedditService.URL_ROOT);
    }

    async getRequestHeaders(): Promise<any> {
        const accessToken = await this.ensureAccessToken();
        return {
            Authorization: `bearer ${accessToken}`
        };
    }

    async getPage(page?: number | string): Promise<Page> {
        const pageUrl = this.getPageUrl(page),
            listing = await this.http.getJson<RedditThing<'Listing'>>(pageUrl, await this.getRequestHeaders());
        return this.convertPage(listing.data);
    }

    async getSubservice(name: string): Promise<Service<any> | null> {
        if (name.startsWith('search:')) {
            return new RedditSearchService(this.options, name.substring('search:'.length));
        } else if (name.startsWith('sr:')) {
            const [slug, url, sorting] = name.split(':');
            return new RedditSubService(this.options, url, sorting as RedditSorting);
        }
        return this.subservices[name];
    }

    async getHomeScreenItems(): Promise<ServiceHomeScreenItem[] | null> {
        const baseResult = await super.getHomeScreenItems();
        if (baseResult) {
            return baseResult;
        }

        const services: {
            title: string,
            service: Service<any>
        }[] = [{
            title: 'Top Daily',
            service: this
        }];
        return await Promise.all(services.map(async serviceEntry => {
            const page = await serviceEntry.service.getPage(1);
            return {
                title: serviceEntry.title,
                galleryRelPath: serviceEntry.service === this ? '' : '/' + serviceEntry.service.name
            }
        }));
    }

    async searchSubreddits(query: string): Promise<RedditSubreddit[]> {
        const data = await this.http.getJson<RedditThing<'Listing'>>(`${RedditService.OAUTH_ROOT}/subreddits/search?q=${query}&raw_json=1`, await this.getRequestHeaders());
        return data.data.children.map(x => (x as RedditThing<'t5'>).data);
    }

    async getSubredditInfo(subreddit: string): Promise<RedditSubreddit | null> {
        try {
            const data = await this.http.getJson<RedditThing<'t5'>>(`${RedditService.OAUTH_ROOT}/r/${subreddit}/about?raw_json=1`, await this.getRequestHeaders());
            if (data.kind === 't5') {
                return data.data;
            }
        }
        catch (e) {
            this.logger.warn(`Failed to get subreddit info for ${subreddit}`);
        }
        return null;
    }

    registerOmnibox(omnibox: OmniboxApi, serviceName: string) {
        omnibox.registerContext({
            name: 'Subreddit',
            type: 'SERVICE',
            serviceName: serviceName,
            aliases: ['sr'],
            queryCallback: async query => {
                const [subredditExpr, searchQuery] = query.split(' '),
                    [subredditName, sorting = 'new'] = subredditExpr.split(':');
                if (searchQuery == null) {
                    const subreddits = [],
                        specificSubreddit = await this.getSubredditInfo(subredditName);
                    if (specificSubreddit != null) {
                        subreddits.push(specificSubreddit);
                    }
                    subreddits.push(..._.take(await this.searchSubreddits(subredditName), 7));
                    return subreddits.map(r => ({
                        contextName: 'Subreddit',
                        contextType: 'SERVICE',
                        resultName: `${r.url} - ${r.title}`,
                        resultType: 'SERVICE_PAGE',
                        pagePath: `/${serviceName}/sr:${r.url}:${sorting}`
                    }));
                } else {
                    return [];
                }
            }
        });
    }

    async getSubServicePathByDescriptor(descriptor: RedditServiceDescriptor): Promise<string | null> {
        switch (descriptor.thing) {
            case 'subreddit': {
                return ['sr', descriptor.url, descriptor.sorting].filter(x => !_.isEmpty(x)).join(':');
            }
        }
        return null;
    }

    getDescriptorConfigDescriptions() {
        const subreddit: ConfigPropertyDescription<Omit<RedditSubredditServiceDescriptor, 'thing' | 'serviceName'>> = {
            type: 'object',
            label: 'Subreddit',
            children: {
                url: {
                    type: 'string',
                    label: 'Subreddit path',
                    placeholder: 'A subreddit path, e.g. /r/subreddit'
                },
                sorting: {
                    type: 'string',
                    label: 'Sorting',
                    editor: {
                        type: 'combobox',
                        options: [{
                            label: 'Hot',
                            value: 'hot'
                        }, {
                            label: 'New',
                            value: 'new'
                        }, {
                            label: 'Random',
                            value: 'random'
                        }]
                    }
                }
            }
        };

        return {
            discriminator: 'thing',
            descriptions: {
                subreddit
            }
        };
    }

    static initSiteHeaderHandler() {
        if (!RedditService.siteHanderInitialized) {
            const matches = [
                /http[s]?:\/\/.*reddit.com\//,
                /http[s]?:\/\/.*redd.it\//
            ];
            registerSiteHeaderHandler('reddit', (url, headers) => {
                const token = RedditService.ACCESS_TOKEN?.access_token;
                if (token != null && headers['Authorization'] == null && matches.find(m => url.match(m))) {
                    headers['Authorization'] = `bearer ${token}`;
                }
            });
            RedditService.siteHanderInitialized = true;
        }
    }
}

export class RedditSearchService extends RedditService {
    private readonly query: string;
    private readonly subreddit?: string;

    constructor(options: OptionsObject<RedditServiceOptions>, query: string, subreddit?: string) {
        super(options);
        this.query = query;
        this.subreddit = subreddit;
        this.capabilities = _.without(this.capabilities, 'SEARCH', 'HOME_SCREEN');
        this.name = 'search';
    }
}

export class RedditSubService extends RedditService {
    private readonly url: string;
    private readonly sorting: string;

    constructor(options: OptionsObject<RedditServiceOptions>, url: string, sorting: RedditSorting) {
        super(options);
        this.url = url;
        this.sorting = sorting;
        this.capabilities = _.without(this.capabilities, 'SEARCH', 'HOME_SCREEN');
        this.name = 'subreddit';
    }

    protected getPageUrl(page: number | string): string {
        return `${RedditService.OAUTH_ROOT}${this.url}${this.sorting}${this.sorting.includes('?') ? '&' : '?'}raw_json=1${page ? '&after=' + page : ''}`;
    }
}