import {
    Page,
    Post, PostRating,
    SearchAutocompleteSuggestion,
    Service, ServiceBaseOptions,
    ServiceCapability,
    ServiceHomeScreenItem
} from './service';
import { getHttpClient, HttpClient, registerSiteHeaderHandler } from '../http/client';
import _ from 'lodash';
import { OptionsObject } from '@/electron/options/options';

interface BehoimiServiceOptions extends ServiceBaseOptions {
    auth?: {
        login: string,
        apiKey: string
    },
    cooldown?: number
}

interface BehoimiPost {
    id?: number,
    md5: string,
    file_url: string,
    sample_url: string,
    preview_url: string,
    tags: string,
    image_width?: number,
    image_height?: number,
    preview_width?: number,
    preview_height?: number,
    rating?: PostRating
}

type BehoimiPage = Array<BehoimiPost>;

registerSiteHeaderHandler('behoimi', (url, headers) => {
    if (url.match(/http[s]?:\/\/.*behoimi.org\//)) {
        headers['User-Agent'] = 'Mozilla/5.0 (X11; Linux x86_64; rv:77.0) Gecko/20100101 Firefox/77.0';
        headers['Referer'] = BehoimiService.URL_ROOT;
    }
})

export class BehoimiService extends Service<BehoimiServiceOptions> {
    name: string = 'danbooru';

    private readonly http: HttpClient;

    public static URL_ROOT = 'http://behoimi.org';

    public capabilities: Array<ServiceCapability> = ['AUTH', 'CONFIGURATION', 'SEARCH', 'HOME_SCREEN'];

    constructor(options: OptionsObject<BehoimiServiceOptions>) {
        super(options);
        this.http = getHttpClient(undefined, options.currentValue?.cooldown || 500);
        this.initSubservices();
    }

    async init(): Promise<void> {

    }

    protected initSubservices() {
        this.subservices.popular_today = new BehoimiPopularService(this.options);
    }

    protected getPageUrl(page: number): string {
        return `/post/index.json?page=${page}`;
    }

    protected convertPost(postData: BehoimiPost): Post {
        const convertTagString = (category: string, tagString: string) => {
            return tagString.split(' ').map(tag => ({
                category,
                name: tag
            }));
        }
        return {
            type: 'POST',
            name: postData.id?.toString() || 'unknown',
            url: `${BehoimiService.URL_ROOT}/post/${postData.id}`,
            thumbUrl: postData.preview_url,
            previewUrl: postData.sample_url,
            fileUrl: postData.file_url,
            tags: convertTagString('general', postData.tags),
            thumbWidth: postData.preview_width,
            thumbHeight: postData.preview_height,
            width: postData.image_width,
            height: postData.image_height,
            rating: postData.rating,
            rawData: postData
        }
    }

    async getPage(page: number): Promise<Page> {
        const url = BehoimiService.URL_ROOT + this.getPageUrl(page);
        const json = await this.http.get(url);
        const pageData: BehoimiPage = JSON.parse(json);
        const lastPost = _.last(pageData);
        return {
            content: pageData.map(this.convertPost.bind(this)),
            nextPageToken: String(page + 1)
        };
    }

    async getSubservices(): Promise<Array<Service<any>>> {
        return Object.values(this.subservices)
            .filter(service => service.name !== 'search');
    }

    async getSubservice(name: string): Promise<Service<any> | null> {
        if (name.startsWith('search:')) {
            this.subservices[name] = new BehoimiSearchService(this.options, name.substring('search:'.length));
        }
        return this.subservices[name];
    }

    async getHomeScreenItems(): Promise<ServiceHomeScreenItem[] | null> {
        const services = [{
            title: 'Popular today',
            service: this.subservices.popular_today
        }, {
            title: 'Posts',
            service: this
        }];
        return await Promise.all(services.map(async serviceEntry => {
            const page = await serviceEntry.service.getPage(1);
            return {
                title: serviceEntry.title,
                galleryRelPath: serviceEntry.service === this ? '' : '/' + serviceEntry.service.name
            }
        }));
    }

    doesPostBelong(post: Post): boolean {
        return post.url.startsWith(BehoimiService.URL_ROOT);
    }
}

export class BehoimiSearchService extends BehoimiService {
    private readonly query: string;

    constructor(options: OptionsObject<BehoimiServiceOptions>, query: string) {
        super(options);
        this.query = query;
        this.capabilities = _.without(this.capabilities, 'SEARCH', 'HOME_SCREEN');
        this.name = 'search';
    }

    protected initSubservices() {

    }

    protected getPageUrl(page: number): string {
        return `/post/index.json?tags=${encodeURIComponent(this.query)}&page=${page}`;
    }

    async getSubservice(name: string): Promise<Service<any> | null> {
        return null;
    }
}

class BehoimiPopularService extends BehoimiService {
    constructor(options: OptionsObject<BehoimiServiceOptions>) {
        super(options);
        this.capabilities = _.without(this.capabilities, 'SEARCH', 'HOME_SCREEN');
        this.name = 'popular_today';
    }

    protected initSubservices() {

    }

    protected getPageUrl(page: number): string {
        return `/post/popular_by_day.json?page=${page}`;
    }

    async getSubservice(name: string): Promise<Service<any> | null> {
        return null;
    }
}