import {
    Page,
    Post,
    SearchAutocompleteSuggestion,
    Service,
    ServiceBaseOptions,
    ServiceCapability,
    ServiceHomeScreenItem
} from './service';
import { getHttpClient, HttpClient } from '../http/client';
import _ from 'lodash';
import { OptionsObject } from '@/electron/options/options';
import { PostCollectionSyncDescriptor } from '@/electron/api/storage-symbols';
import OmniboxApi from '@/electron/api/omnibox';
import { OmniboxResult } from '@/electron/api/omnibox-symbols';

export interface E621AuthCredentials {
    login: string,
    apiKey: string
}

export interface E621UserInfo {
    id: number,
    name: string,
    favorite_tags?: string,
    blacklisted_tags?: string
}

interface E621ServiceOptions extends ServiceBaseOptions {
    auth?: E621AuthCredentials,
    cooldown?: number
}

interface E621FileDescription {
    width?: number,
    height?: number,
    url: string,
    ext?: string,
    size?: number
    md5?: string
}

export type E621PostRating = 's' | 'q' | 'e';

interface E621Post {
    id?: number,
    file: E621FileDescription,
    preview?: E621FileDescription,
    sample?: E621FileDescription,
    tags: {
        general: Array<string>,
        species: Array<string>,
        character: Array<string>,
        copyright: Array<string>,
        artist: Array<string>,
        invalid: Array<string>,
        lore: Array<string>,
        meta: Array<string>
    },
    rating: E621PostRating
}

type E621Page = {
    posts: Array<E621Post>
};

interface E621AutocompleteSuggestion {
    id?: number,
    name: string,
    category: number,
    antecedent_name?: string
}

interface E621PostSet {
    id: number,
    name: string,
    shortname: string,
    post_ids: number[]
}

interface E621SetCollectionSyncDescriptor extends PostCollectionSyncDescriptor {
    type: 'POST_SET',
    setShortName: string
}

const tagTypeMap: Record<number, string> = {
    0: 'general',
    1: 'artist',
    3: 'copyright',
    4: 'character',
    5: 'species',
    6: 'invalid',
    7: 'lore',
    8: 'meta'
}

export class E621Service extends Service<E621ServiceOptions, E621AuthCredentials, E621UserInfo> {
    name: string = 'e621';

    protected readonly http: HttpClient;

    protected static URL_ROOT = 'https://e621.net';

    public capabilities: Array<ServiceCapability> = ['AUTH', 'CONFIGURATION', 'SEARCH', 'HOME_SCREEN', 'COLLECTION_SYNC'];

    constructor(options: OptionsObject<E621ServiceOptions>) {
        super(options);
        this.http = getHttpClient(undefined, options.currentValue?.cooldown || 1000);
        this.initSubservices();
    }

    async init(): Promise<void> {

    }

    protected initSubservices() {
        this.subservices.popular = new E621PopularService(this.options);
        this.subservices.popular.name = 'popular';
    }

    protected getPageUrl(page: number | string): string {
        return `/posts.json?page=${page}`;
    }

    protected convertPost(postData: E621Post): Post {
        return {
            type: 'POST',
            name: postData.id?.toString() || 'unknown',
            url: `${E621Service.URL_ROOT}/posts/${postData.id}`,
            thumbUrl: postData.preview?.url,
            previewUrl: postData.sample?.url,
            fileUrl: postData.file.url,
            tags: Object.entries(postData.tags)
                .flatMap(([category, tags]) => tags.map(name => ({
                    category,
                    name
                }))),
            thumbWidth: postData.preview?.width,
            thumbHeight: postData.preview?.height,
            width: postData.file.width,
            height: postData.file.height,
            rating: postData.rating,
            rawData: postData
        }
    }

    getNextPageToken(currentPage: number | string, page: E621Page) {
        const lastPost = _.last(page.posts.filter(post => post.id));
        return lastPost && `b${lastPost.id}`;
    }

    async getPage(page: number | string): Promise<Page> {
        const url = E621Service.URL_ROOT + this.getPageUrl(page);
        const json = await this.http.get(url);
        const pageData: E621Page = JSON.parse(json);
        return {
            content: pageData.posts.map(this.convertPost.bind(this)),
            nextPageToken: this.getNextPageToken(page, pageData)
        };
    }

    async getSubservice(name: string): Promise<Service<any> | null> {
        if (name.startsWith('search:')) {
            return new E621SearchService(this.options, name.substring('search:'.length));
        }
        return this.subservices[name];
    }

    async getAutocompleteSuggestions(query: string): Promise<SearchAutocompleteSuggestion[]> {
        const json = await this.http.get(`${E621Service.URL_ROOT}/tags/autocomplete.json?search[name_matches]=${encodeURIComponent(query)}`);
        const data = JSON.parse(json) as E621AutocompleteSuggestion[];
        return data.map(s => ({
            category: tagTypeMap[s.category],
            label: s.name,
            name: s.name
        }));
    }

    async getHomeScreenItems(): Promise<ServiceHomeScreenItem[] | null> {
        const services = [{
            title: 'Popular',
            service: this.subservices.popular
        }, {
            title: 'Posts',
            service: this
        }];
        return await Promise.all(services.map(async serviceEntry => {
            const page = await serviceEntry.service.getPage(1);
            return {
                title: serviceEntry.title,
                galleryRelPath: serviceEntry.service === this ? '' : '/' + serviceEntry.service.name
            }
        }));
    }

    protected getAuthOptions(): OptionsObject<any> {
        if (!this.options.currentValue?.auth) {
            this.options.setChildValue('auth', undefined);
        }
        // @ts-ignore
        return this.options.getChild('auth')!;
    }

    async authenticate(credentials: E621AuthCredentials) {
        function toBase64(data: string) {
            return new Buffer(data).toString('base64');
        }
        this.http.updateDefaultHeaders({
            'Authorization': `Basic ${toBase64(credentials.login + ':' + credentials.apiKey)}`
        });
        try {
            this.logger.info(`Logging in`);
            await this.getAuthenticatedUserInfo();
            this.logger.info(`Login successful`);
            this.getAuthOptions().setValue(credentials);
        }
        catch (e) {
            this.logger.info(`Login failed`);
            this.http.updateDefaultHeaders({
                'Authorization': undefined
            });
            throw e;
        }
    }

    async getAuthenticatedUserInfo(): Promise<E621UserInfo | null> {
        if (!this.getAuthOptions().currentValue) {
            return null;
        }
        const json = await this.http.get(`${E621Service.URL_ROOT}/users/upload_limit.json`);
        const profileData = JSON.parse(json);
        if (profileData.success === false) {
            throw new Error(`Failed to retrieve user data: ${profileData.message}`);
        }
        return profileData;
    }

    isAuthSupported() {
        return true;
    }

    getAuthQueryString() {
        return `login=${this.getAuthOptions().currentValue?.login}&api_key=${this.getAuthOptions().currentValue?.apiKey}`;
    }

    doesPostBelong(post: Post): boolean {
        return post.url.startsWith(E621Service.URL_ROOT);
    }

    async getUserCollections(): Promise<E621SetCollectionSyncDescriptor[]> {
        const userData = await this.getAuthenticatedUserInfo();
        if (!userData) {
            throw new Error('Not logged in');
        }
        const sets = await this.http.getJson<E621PostSet[]>(`${E621Service.URL_ROOT}/post_sets.json?search[creator_id]=${userData.id}`);
        return sets
            .map(set => ({
                serviceName: this.name,
                collectionName: set.name.replaceAll('_', ' '),
                collectionId: String(set.id),
                type: 'POST_SET',
                setShortName: set.shortname
            }));
    }

    async getPostSet(id: number) {
        return this.http.getJson<E621PostSet>(`${E621Service.URL_ROOT}/post_sets/${id}.json`);
    }

    async getUserCollectionPage(collectionId: string, page?: string): Promise<Page> {
        const serviceKey = `set:${collectionId}`;
        let service = this.subservices[serviceKey];
        if (!service) {
            const set = await this.getPostSet(+collectionId);
            service = new E621PostSetService(this.options, set.shortname);
            this.subservices[serviceKey] = service;
        }
        return service.getPage(page || 1);
    }

    async addPostToUserCollection(collectionId: string, post: Post): Promise<void> {
        await this.http.sendJson(`${E621Service.URL_ROOT}/post_sets/${collectionId}/add_posts.json?${this.getAuthQueryString()}`, 'POST', {
            post_ids: [+post.name]
        });
    }

    async removePostFromUserCollection(collectionId: string, post: Post): Promise<void> {
        await this.http.sendJson(`${E621Service.URL_ROOT}/post_sets/${collectionId}/remove_posts.json?${this.getAuthQueryString()}`, 'POST', {
            post_ids: [+post.name]
        });
    }

    registerOmnibox(omnibox: OmniboxApi, serviceName: string) {
        omnibox.registerContext({
            name: 'e621 search',
            type: 'SERVICE',
            serviceName: serviceName,
            aliases: ['e6'],
            queryCallback: async query => {
                const suggestions = await this.getAutocompleteSuggestions(query);
                const results = _.take(suggestions, 7).map(s => ({
                    contextName: 'e621 search',
                    contextType: 'SERVICE',
                    resultName: s.name,
                    resultType: 'SERVICE_PAGE',
                    pagePath: `/${serviceName}/search:${s.name}`
                } as OmniboxResult));
                if (!suggestions?.find(s => s.name === query)) {
                    results.unshift({
                        contextName: 'e621 search',
                        contextType: 'SERVICE',
                        resultName: query,
                        resultType: 'SERVICE_PAGE',
                        pagePath: `/${serviceName}/search:${query}`
                    });
                }
                return results;
            }
        });
    }
}

class E621Subservice extends E621Service {
    constructor(options: OptionsObject<E621ServiceOptions>) {
        super(options);
        this.capabilities = _.without(this.capabilities, 'SEARCH', 'HOME_SCREEN');
    }

    protected initSubservices() {

    }

    async getSubservice(name: string): Promise<Service<any> | null> {
        return null;
    }

    isAuthSupported() {
        return false;
    }
}

class E621SearchService extends E621Subservice {
    private readonly query: string;

    constructor(options: OptionsObject<E621ServiceOptions>, query: string) {
        super(options);
        this.query = query;
        this.name = 'search';
    }

    protected getPageUrl(page: number): string {
        return `/posts.json?tags=${encodeURIComponent(this.query)}&page=${page}`;
    }
}

class E621PopularService extends E621Subservice {
    protected getPageUrl(page: number): string {
        return `/explore/posts/popular.json?page=${page}`;
    }
}

class E621PostSetService extends E621Subservice {
    private readonly setShortName: string;

    constructor(options: OptionsObject<E621ServiceOptions>, setShortName: string) {
        super(options);
        this.setShortName = setShortName;
        this.name = 'set';
    }

    protected getPageUrl(page: number): string {
        return `/posts.json?tags=${encodeURIComponent(`set:${this.setShortName}`)}&page=${page}&${this.getAuthQueryString()}`;
    }

    getNextPageToken(currentPage: number | string, page: E621Page) {
        if (_.isEmpty(page.posts)) {
            return;
        }
        return String(+currentPage + 1);
    }
}