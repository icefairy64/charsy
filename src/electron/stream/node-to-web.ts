import { Readable, Writable } from 'stream';
import { ReadableStream as NodeReadableStream } from 'stream/web';
import { Buffer as NodeBuffer } from 'buffer';
import _, { chunk } from 'lodash';
import { sleep } from '@/electron/util/common-util';

export function convertNodeReadableToWeb(source: Readable): ReadableStream & { _adapting?: Readable } {
    let error: Error | undefined;

    const stream = new NodeReadableStream({
        type: 'bytes',

        start(controller) {
            if (source.readableEnded) {
                throw new Error('Stream is closed');
            }
            source.on('error', e => {
                error = e;
                controller.error(e);
            });
            source.on('end', () => controller.close());
            source.on('data', chunk => {
                controller.enqueue(chunk);
            });
        },

        cancel() {
            source.destroy();
        }
    }) as ReadableStream & { _adapting?: Readable };

    stream._adapting = source;
    return stream;
}

export function convertWebReadableToNode(source: ReadableStream & { _adapting?: Readable }): Readable {
    let reader: ReadableStreamDefaultReader | undefined;

    return new Readable({
        construct(callback: (error?: (Error | null)) => void) {
            try {
                reader = source.getReader();
                callback();
            }
            catch (e) {
                callback(e);
            }
        },

        async read(size: number) {
            if (!reader) {
                return;
            }
            const result = await reader.read();
            if (result.done || !result.value) {
                this.push(null);
                return;
            }
            const encoding = _.isString(result.value) ? 'utf-8' : undefined;
            if (result.value.length > 0) {
                this.push(result.value, encoding);
            }
        },

        async destroy(error: Error | null, callback: (error: (Error | null)) => void) {
            if (!reader) {
                callback(new Error('No reader present'));
                return;
            }
            try {
                await reader.cancel(error?.toString());
                callback(null);
            }
            catch (e) {
                callback(e);
            }
        }
    });
}

export function convertWebWritableToNode(target: WritableStream): Writable {
    let writer: WritableStreamDefaultWriter | undefined;

    return new Writable({
        construct(callback: (error?: (Error | null)) => void) {
            try {
                writer = target.getWriter();
                callback();
            }
            catch (e) {
                callback(e);
            }
        },

        async write(chunk: any, encoding: BufferEncoding, callback: (error?: (Error | null)) => void) {
            if (!writer) {
                callback(new Error('No writer present'));
                return;
            }

            try {
                writer.ready;
                writer.write(chunk);
                callback();
            }
            catch (e) {
                callback(e);
            }
        },

        async final(callback: (error?: (Error | null)) => void) {
            if (!writer) {
                callback(new Error('No writer present'));
                return;
            }

            try {
                await writer.close();
                callback();
            }
            catch (e) {
                callback(e);
            }
        }
    })
}