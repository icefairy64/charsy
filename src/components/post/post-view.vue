<template>
  <div :class="{ 'px-post-view': true, 'px-post-view-touch': touchControls }" @resize="onResize">
    <div
        class="px-post-view-content"
        ref="content"
        :style="{
          '--px-post-view-content-offset-x': `${contentOffset.x}px`,
          '--px-post-view-content-offset-y': `${contentOffset.y}px`,
          '--px-post-view-content-scale': scale
        }"
        @pointerdown="onContentMouseDown"
        @pointerup="onContentMouseUp"
        @pointermove="onContentMouseMove"
        @dblclick="onContentDblClick"
        @click="onContentClick">
      <transition name="px-post-view-content-fade">
      <div v-if="fileType === 'image' && previewUrl != null && contentBlobUrl == null" class="px-post-view-content-image px-post-view-content-preview" :style="{
        backgroundImage: `url(${previewUrl})`
      }"></div>
      <div v-else-if="fileType === 'image' && (previewUrl == null || contentBlobUrl != null)" class="px-post-view-content-image" :style="{
        backgroundImage: `url(${contentUrl})`
      }"></div>
      <video v-else-if="fileType === 'video'" :autoplay="true" :loop="true" :poster="previewUrl"
        @canplay="onVideoEvent"
        @loadeddata="onVideoEvent"
        @loadedmetadata="onVideoEvent"
        @play="onVideoEvent"
        @progress="onVideoEvent"
        @stalled="onVideoEvent"
        @suspend="onVideoEvent"
        @waiting="onVideoEvent">
        <source :src="contentUrl" :type="contentMediaTypeString">
        An error occurred during video load
      </video>
      </transition>
    </div>
    <div :class="{ 'px-post-view-overlay': true, 'px-acrylic': true, 'px-elevated': true, 'px-post-view-overlay-hidden': touchControls && !overlayShown }">
      <span class="px-post-view-overlay-title px-text-header-1" v-if="post.title">
        {{ post.title }}
        <span class="px-post-view-overlay-author px-text-size-7 px-text-light" v-if="post.title && post.author">
          by {{ post.author }}
        </span>
      </span>
      <div class="px-post-view-overlay-tag-container">
        <px-badge
            v-for="tag in sortedTags"
            :key="getShortTagName(tag)"
            :actionable="true"
            :class="`px-tag-${getTagCategory(tag)}`"
            @click="$emit('tagclick', tag)">
          {{ getShortTagName(tag) }}
        </px-badge>
      </div>
    </div>
    <div v-if="contentBlobProgress" class="px-post-view-progress-bar" :style="{
      '--px-progress': contentBlobProgress.downloaded / contentBlobProgress.total
    }"></div>
  </div>
</template>

<script>
import {
  getCachePassthroughUrl,
  getPostMainAttachmentInfo,
  getShortTagName, getTagCategory,
  sortTagsByCategoryPriority
} from "@/electron/util/post-util.ts";
import PxBadge from "@/components/common/badge.vue";
import ResizeAware from '@/mixins/resize-aware.vue';
import {getMediaTypeString} from "@/electron/util/media-type-util.ts";
import { useLogger } from "@/composables/logging-composable";
import { useScreenLayout } from "@/composables/platform-composable";
import _ from "lodash";
import { ref } from "vue";
import { useHotkeys } from "@/composables/keyboard-composable";

export default {
  name: "px-post-view",
  components: { PxBadge },
  mixins: [ResizeAware],
  props: {
    post: {
      type: Object,
      required: true
    },
    previewUrl: {
      type: String
    }
  },
  setup(props, context) {
    const { logger } = useLogger(),
        { screenLayout } = useScreenLayout(),
        hotkeys = ref([{
          key: 'ArrowLeft',
          handler: () => context.emit('prev')
        }, {
          key: 'ArrowRight',
          handler: () => context.emit('next')
        }]);

    useHotkeys(hotkeys);

    return {
      logger,
      screenLayout
    };
  },
  watch: {
    async post() {
      this.freeContentBlob();
      this.contentScaled = false;
      this.contentOffset = {
        x: 0,
        y: 0
      }
      if (this.fileType === 'image' && this.previewUrl != null) {
        await this.loadContentBlob().catch(e => this.logger.error(`Failed to load content blob for ${this.contentUrl}`, e));
      }
    }
  },
  data() {
    return {
      contentBlob: null,
      contentBlobUrl: null,
      contentBlobProgress: null,
      contentOffset: {
        x: 0,
        y: 0
      },
      contentScaled: false,
      contentScaleDirection: null,
      contentPanning: false,
      contentViewSize: {
        width: 0,
        height: 0
      },
      overlayShown: false,
      scale: 1
    }
  },
  computed: {
    mainAttachmentInfo() {
      return getPostMainAttachmentInfo(this.post);
    },
    fileType() {
      const fileType = this.mainAttachmentInfo?.mediaType?.type;
      if (!fileType) {
        this.logger.error(`Unknown file type for ${JSON.stringify(this.mainAttachmentInfo)}`);
      }
      return fileType;
    },
    contentUrl() {
      if (this.contentBlobUrl) {
        return this.contentBlobUrl;
      }
      const url = this.mainAttachmentInfo?.fileUrl;
      return url && getCachePassthroughUrl(url);
    },
    sortedTags() {
      if (this.post != null) {
        return sortTagsByCategoryPriority(this.post.tags);
      }
      return [];
    },
    contentMediaTypeString() {
      const mediaType = this.mainAttachmentInfo?.mediaType;
      return mediaType && getMediaTypeString(mediaType);
    },
    contentFillScale() {
      if (!this.post) {
        return 1;
      }
      const postWidth = this.post.width,
          postHeight = this.post.height,
          postRatio = postWidth / postHeight,
          contentWidth = this.contentViewSize.width,
          contentHeight = this.contentViewSize.height,
          contentRatio = contentWidth / contentHeight;
      if (postRatio > contentRatio) {
        // Post is wider
        const k = postWidth / contentWidth;
        return {
          scale: contentHeight / postHeight * k,
          scrolling: 'horizontal'
        };
      } else {
        // Post is taller
        const k = postHeight / contentHeight;
        return {
          scale: contentWidth / postWidth * k,
          scrolling: 'vertical'
        };
      }
    },
    maxOffsets() {
      if (!this.post) {
        return {
          x: 0,
          y: 0
        };
      }
      const contentWidth = this.contentViewSize.width,
          contentHeight = this.contentViewSize.height,
          ratio = this.post.width / this.post.height;
      return {
        x: this.contentFillScale.scrolling === 'horizontal' ? contentWidth * (this.scale - 1) / 2 : Math.max(contentHeight * ratio * this.scale - contentWidth, 0) / 2,
        y: this.contentFillScale.scrolling === 'vertical' ? contentHeight * (this.scale - 1) / 2 : Math.max(contentWidth * ratio * this.scale - contentHeight, 0) / 2
      };
    },
    touchControls() {
      return this.screenLayout.size === 'SMALL' || this.screenLayout.orientation === 'VERTICAL';
    }
  },
  emits: {
    tagclick: null,
    next: null,
    prev: null
  },
  mounted() {
    this.$refs.content.addEventListener('wheel', this.onWheelEvent.bind(this));
    if (this.fileType === 'image' && this.previewUrl != null) {
      this.loadContentBlob().catch(e => this.logger.error(`Failed to load content blob for ${this.contentUrl}`, e));
    }
    this.logger.debug(`Opening post ${JSON.stringify(this.post)}`);
  },
  unmounted() {
    this.freeContentBlob();
  },
  methods: {
    getShortTagName,
    getTagCategory,
    onContentMouseDown() {
      this.contentPanning = true;
    },
    onContentMouseUp() {
      this.contentPanning = false;
    },
    /** @param ev {MouseEvent} */
    onContentMouseMove(ev) {
      if (this.contentPanning && this.contentScaled) {
        if (this.contentFillScale.scrolling === 'horizontal' || this.scale > this.contentFillScale.scale) {
          this.contentOffset.x += ev.movementX;
        }
        if (this.contentFillScale.scrolling === 'vertical' || this.scale > this.contentFillScale.scale) {
          this.contentOffset.y += ev.movementY;
        }
      }
      this.contentOffset.x = _.clamp(this.contentOffset.x, -this.maxOffsets.x, this.maxOffsets.x);
      this.contentOffset.y = _.clamp(this.contentOffset.y, -this.maxOffsets.y, this.maxOffsets.y);
    },
    onContentDblClick() {
      this.contentScaled = !this.contentScaled;
      if (!this.contentScaled) {
        this.contentOffset = {
          x: 0,
          y: 0
        }
      }
      this.scale = this.contentScaled ? this.contentFillScale.scale : 1;
    },
    onContentClick() {
      if (this.touchControls) {
        this.overlayShown = !this.overlayShown;
      }
    },
    onResize() {
      const contentEl = this.$refs.content;
      this.contentViewSize.width = contentEl.clientWidth;
      this.contentViewSize.height = contentEl.clientHeight;
    },
    /**
     * @param ev {WheelEvent}
     */
    onWheelEvent(ev) {
      const direction = Math.sign(ev.deltaY),
        factor = direction < 0 ? 1.2 : (1 / 1.2),
        targetScale = this.scale * factor;

      if (targetScale > 1) {
        this.contentScaled = true;
        this.scale = targetScale;
        this.contentOffset.x *= factor;
        this.contentOffset.y *= factor;
        this.contentOffset.x = _.clamp(this.contentOffset.x, -this.maxOffsets.x, this.maxOffsets.x);
        this.contentOffset.y = _.clamp(this.contentOffset.y, -this.maxOffsets.y, this.maxOffsets.y);
      } else {
        this.contentScaled = false;
        this.scale = 1;
        this.contentOffset.x = 0;
        this.contentOffset.y = 0;
      }
    },
    async loadContentBlob() {
      const loadedPost = this.post;
      const response = await fetch(this.contentUrl);
      let downloadedBytes = 0;
      const bodyReader = response.body.getReader(),
        chunks = [];
      this.contentBlobProgress = {
        total: +response.headers.get('content-length'),
        downloaded: downloadedBytes
      }
      let done = false;
      while (!done) {
        const result = await bodyReader.read();
        if (this.post !== loadedPost) {
          this.logger.debug(`Stopping download because of post change`);
          return;
        }
        done = result.done;
        if (!done) {
          downloadedBytes += result.value.length;
          this.contentBlobProgress.downloaded = downloadedBytes;
          chunks.push(result.value);
        }
      }
      const contentBlob = new Blob(chunks);
      if (this.post === loadedPost) {
        this.contentBlob = contentBlob;
        this.contentBlobUrl = URL.createObjectURL(this.contentBlob);
        this.contentBlobProgress = null;
      }
    },
    freeContentBlob() {
      if (this.contentBlobUrl) {
        URL.revokeObjectURL(this.contentBlobUrl);
        this.contentBlob = null;
        this.contentBlobUrl = null;
        this.contentBlobProgress = null;
      }
    },
    onVideoEvent(e) {
      this.logger.trace(`Video event ${e.type}`);
    }
  }
}
</script>

<style scoped>
  .px-post-view {
    position: relative;
    overflow: hidden;
  }
  .px-post-view-content {
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: row;
    align-content: stretch;
    align-items: stretch;
    justify-content: stretch;
    justify-items: center;
    position: relative;

    --px-post-view-content-offset-x: 0px;
    --px-post-view-content-offset-y: 0px;
    --px-post-view-content-scale: 100%;
  }
  .px-post-view-content > * {
    flex: 1;

    transform: translate(var(--px-post-view-content-offset-x), var(--px-post-view-content-offset-y)) scale(var(--px-post-view-content-scale));
  }
  .px-post-view-content-image {
    width: 100%;
    height: 100%;
    position: absolute;
    background-position: center;
    background-repeat: no-repeat;
    background-size: contain;
    -webkit-user-drag: none;
  }
  .px-post-view-content-preview {
    filter: blur(20px);
  }
  .px-post-view-content video {
    position: absolute;
    width: 100%;
    height: 100%;
  }
  .px-post-view-overlay {
    position: absolute;
    bottom: 0;
    left: 0;
    max-height: 25%;
    width: 100%;
    padding: var(--px-margin-spacing) calc(var(--px-margin-spacing) * 2);
    display: flex;
    flex-direction: column;
    justify-items: flex-end;
    justify-content: flex-end;
    box-sizing: border-box;
    color: var(--px-accent-color-text-highlight);
    overflow: hidden;
    transition: all 0.1s ease;
  }
  .px-post-view:not(.px-post-view-touch) .px-post-view-overlay:not(:hover) {
    opacity: 0;
  }
  .px-post-view.px-post-view-touch .px-post-view-overlay.px-post-view-overlay-hidden {
    opacity: 0;
  }
  .px-post-view-overlay-tag-container {
    display: flex;
    flex-wrap: wrap;
    overflow-y: scroll;
  }
  .px-post-view-overlay-tag-container > * {
    --px-tag-container-tag-spacing: calc(var(--px-margin-spacing) * 0.5);
    margin-right: var(--px-tag-container-tag-spacing);
    margin-bottom: var(--px-tag-container-tag-spacing);
  }
  .px-badge.px-tag-artist {
    --px-badge-color: #a42565;
    --px-badge-text-color: white;
  }
  .px-badge.px-tag-copyright {
    --px-badge-color: var(--px-accent-color-2);
    --px-badge-text-color: white;
  }
  .px-badge.px-tag-character {
    --px-badge-color: var(--px-accent-color-3);
    --px-badge-text-color: white;
  }
  .px-badge.px-tag-relationship {
    --px-badge-color: #ab5222;
    --px-badge-text-color: white;
  }
  .px-badge.px-tag-extreme_content {
    --px-badge-color: #950feb;
    --px-badge-text-color: white;
  }

  .px-post-view-progress-bar {
    background-color: var(--px-accent-color-2);
    pointer-events: none;
    position: absolute;
    height: 20px;
    top: 0;
    left: 0;
    width: calc(var(--px-progress) * 100%);
  }

  /* Transitions */
  .px-post-view-content-fade-enter-to,
  .px-post-view-content-fade-leave-from {
    opacity: 1;
  }

  .px-post-view-content-fade-enter-from,
  .px-post-view-content-fade-leave-to {
    opacity: 0;
  }

  .px-post-view-content-fade-leave-to {
    transition-delay: 0.2s;
  }

  .px-post-view-content-fade-enter-to {
    transition-delay: 0.3s;
  }

  .px-post-view-content-fade-enter-active,
  .px-post-view-content-fade-leave-active {
    transition: all 0.3s ease;
  }
</style>