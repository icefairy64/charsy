<template>
  <div
      :class="['px-waterfall', ...(singleLineHeight ? ['px-waterfall-singleline'] : []), ...(maxLines ? ['px-waterfall-finite'] : [])]"
      ref="root"
      :style="{
        height: hardHeight && `${hardHeight}px`
      }"
      @resize="onResize">
    <div ref="scroll-start" class="px-waterfall-startmarker" :style="{
      height: `${loadRange}px`
    }">-</div>
    <transition-group name="waterfall-fade">
      <px-waterfall-slot
        v-for="{ box, item } in visibleItemBoxes"
        :key="getKey(item)"
        :box="box"
        :data-entry-index="getDisplayIndex(box)"
        :style="{
          '--px-batch-entry-index': getDisplayIndex(box),
          '--px-entry-index': getEntryIndex(item)
        }">
        <slot
          name="item"
          :item="item"
          :box="box">
          Placeholder
        </slot>
      </px-waterfall-slot>
    </transition-group>
    <div ref="scroll-end" class="px-waterfall-endmarker" :style="{
      height: `${loadRange}px`
    }">-</div>
  </div>
</template>

<script>
import PxWaterfallSlot from "@/components/layout/waterfall-slot.vue";
import ResizeAware from "@/mixins/resize-aware.vue";
import _ from 'lodash';
import { Subject } from "rxjs";
import { ref } from "vue";
import { useIntersectionContainer } from "@/composables/intersection-composable";
import { filter, scan, skip, throttleTime } from "rxjs/operators";
export default {
  name: "px-waterfall",
  components: {PxWaterfallSlot},
  mixins: [ResizeAware],
  props: {
    items: Array,
    lineGap: Number,
    targetRatio: Number,
    itemKeyMapper: Function,
    itemSizeMapper: Function,
    loadRange: {
      type: Number,
      default: 100
    },
    cullRange: {
      type: Number,
      default: 600
    },
    singleLineRatio: {
      type: Number
    },
    maxLines: Number,
    margin: {
      type: Object,
      default: null
    },
    disableAutoLoad: {
      type: Boolean,
      default: false
    }
  },
  emits: {
    'data-load-request': null
  },
  data: function () {
    return {
      itemBoxes: [],
      visibleItemBoxes: [],
      timeoutHandle: null,
      singleLineHeight: null,
      finiteHeight: null
    }
  },
  setup(props) {
    const root = ref(null),
      intersectionSubject = ref(new Subject());
    const { addEl: observeElIntersection } = useIntersectionContainer(props.disableAutoLoad ? ref(document.body) : root, (key, intersecting) => {
      intersectionSubject.value.next({ key, intersecting });
    });
    return {
      root,
      intersectionSubject,
      observeElIntersection
    };
  },
  mounted() {
    this.resizeSubject = new Subject();
    this.resizeSubject.subscribe(() => this.doResize());
    this.calculateItemPositions();
    this.$nextTick(() => {
      if (this.isEndNear() && !this.disableAutoLoad) {
        this.requestDataLoad();
      }
    });
    this.initIntersection();
  },
  computed: {
    hardHeight() {
      if (this.singleLineHeight) {
        return this.singleLineHeight;
      } else if (this.finiteHeight) {
        return this.finiteHeight;
      }
      return null;
    }
  },
  watch: {
    items: {
      handler: function (items) {
        this.calculateItemPositions();
        if (this.isEndNear() && !this.disableAutoLoad) {
          this.requestDataLoad();
        }
        if (_.isEmpty(items)) {
          this.$refs.root.scrollTop = 0;
        }
      },
      deep: true
    },
    itemBoxes: function () {
      this.cull();
    }
  },
  methods: {
    initIntersection() {
      this.observeElIntersection('_scroll-end', this.$refs["scroll-end"]);
      this.observeElIntersection('_scroll-start', this.$refs["scroll-start"]);

      this.intersectionSubject.pipe(
          scan((a, { key, intersecting }) => {
            if (intersecting) {
              a[key] = true;
            }
            if (!intersecting && a[key]) {
              delete a[key];
            }
            return a;
          }, {}),
          throttleTime(50, undefined, { trailing: true })
      ).subscribe(() => this.cull());

      if (!this.disableAutoLoad) {
        this.intersectionSubject.pipe(filter(x => x.key === '_scroll-end' && x.intersecting), skip(1))
            .subscribe(() => this.requestDataLoad());
      }

      if (this.disableAutoLoad) {
        this.intersectionSubject.pipe(filter(x => x.key === '_scroll-start' && x.intersecting))
            .subscribe(() => this.requestDataLoad());
      }
    },
    getKey(item) {
      if (this.itemKeyMapper) {
        return this.itemKeyMapper(item);
      } else {
        return item.id || item;
      }
    },
    getBatchEntryIndex(item) {
      return item._batchEntryIndex || 0;
    },
    getEntryIndex(item) {
      return this.items.indexOf(item);
    },
    getDisplayIndex(box) {
      return this.visibleItemBoxes.findIndex(b => b.box === box);
    },
    isEndNear() {
      const visibleAreaEnd = this.$el.scrollTop + this.$el.clientHeight;
      return visibleAreaEnd + this.loadRange > this.$el.scrollHeight;
    },
    isItemInCullRange(itemBox) {
      return !(itemBox.y > this.$el.scrollTop + this.$el.clientHeight + this.cullRange)
          && !((itemBox.y + itemBox.height) < this.$el.scrollTop - this.cullRange);
    },
    onResize() {
      this.resizeSubject.next(null);
    },
    doResize() {
      const scrollHeight = this.$el.scrollHeight,
          scrollTop = this.$el.scrollTop;
      this.calculateItemPositions();
      const newHeight = this.$el.scrollHeight;
      this.$el.scrollTo(0, scrollTop * (newHeight / scrollHeight));
    },
    cull() {
      this.visibleItemBoxes = this.itemBoxes
          .map((box, index) => ({ box, index, item: this.items[index] }))
          .filter(item => this.isItemInCullRange(item.box));
    },
    requestDataLoad() {
      this.$emit('data-load-request');
    },
    calculateItemPositions() {
      const containerWidth = this.$el.clientWidth - (this.margin?.x ? this.margin.x * 2 : 0),
        rows = [];

      this.singleLineHeight = this.singleLineRatio && containerWidth / this.singleLineRatio;

      function newRow() {
        rows.push([]);
      }

      const rowWidthClean = (row) => {
        const height = rowHeight(row);
        return row.map(x => x.width * height / x.height).reduce((a, x) => a + x, 0);
      }

      const rowHeight = (row) => {
        return row.map(x => x.height).reduce((a, x) => Math.max(a, x), 0);
      }

      function pushItem(item) {
        if (rows.length === 0) {
          newRow();
        }

        if (this.maxLines == null || rows.length <= this.maxLines) {
          const row = rows[rows.length - 1];

          row.push(item);

          if (this.targetRatio != null && rowWidthClean(row) / rowHeight(row) > this.targetRatio) {
            newRow();
          }
        }
      }

      this.items
          .map(this.itemSizeMapper)
          .forEach(x => pushItem.call(this, x));

      this.itemBoxes = [];

      let y = this.margin?.y ?? 0;

      for (const row of rows.filter(row => row.length > 0)) {
        const rowCleanWidth = rowWidthClean(row);
        row.forEach(box => {
          box.width = box.width * containerWidth / rowCleanWidth;
          box.height = box.height * containerWidth / rowCleanWidth;
        });
        const gapWidth = (row.length - 1) * this.lineGap;
        const gapLessRealRowWidth = containerWidth - gapWidth;
        row.forEach(box => {
          box.width = box.width * gapLessRealRowWidth / containerWidth;
          box.height = box.height * gapLessRealRowWidth / containerWidth;
        });
        const maxHeight = this.singleLineHeight || Math.min(containerWidth / this.targetRatio, row.map(box => box.height).reduce((a, x) => Math.max(a, x)));
        let x = this.margin?.x ?? 0;
        row.forEach((box, cellIndex) => {
          box.width = box.width * maxHeight / box.height;
          box.height = maxHeight;
          box.x = x;
          box.y = y;
          box.cellIndex = cellIndex;
          this.itemBoxes.push(box);
          x += box.width + this.lineGap;
        });
        y += maxHeight + this.lineGap;
      }

      if (this.maxLines != null) {
        this.finiteHeight = y - this.lineGap;
      }

      this.$refs['scroll-end'].style.top = `${y - this.lineGap - this.loadRange}px`;
      this.cull();
    }
  }
}
</script>

<style scoped>
  .px-waterfall {
    position: relative;
    overflow-y: scroll;
    overflow-x: hidden;
  }
  .px-waterfall.px-waterfall-singleline {
    overflow-y: hidden;
    overflow-x: scroll;
  }
  .px-waterfall.px-waterfall-finite {
    overflow: hidden;
  }
  .px-waterfall-endmarker, .px-waterfall-startmarker {
    position: absolute;
    opacity: 0;
    pointer-events: none;
    width: 100%;
  }
  .px-waterfall-endmarker {
    bottom: 0;
  }
  .px-waterfall-startmarker {
    top: 0;
  }

  /* Transitions */

  .waterfall-fade-enter-active,
  .waterfall-fade-leave-active,
  .waterfall-fade-move {
    transition: all 0.1s ease;
  }

  .waterfall-fade-enter-active {
    transition-delay: calc(var(--px-batch-entry-index) * 0.02s);
  }

  .waterfall-fade-leave-active {
    transition-delay: min(calc(var(--px-entry-index) * 0.02s), 1s);
  }

  .waterfall-fade-enter-from,
  .waterfall-fade-leave-to {
    opacity: 0;
    transform: translateY(10px);
  }
</style>