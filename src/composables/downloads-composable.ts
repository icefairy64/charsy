import { onMounted, ref } from 'vue';
import { IpcEventSubscribeFn } from '@/composables/ipc-event-composable';
import { getDownloadList } from '@/api/download';
import { DownloadInfo, IPC_EVENT_DOWNLOAD_INFO } from '@/electron/api/download-symbols';

export function useDownloads(onIpcEvent: IpcEventSubscribeFn, sorter?: (a: DownloadInfo, b: DownloadInfo) => number) {
    const downloads = ref<DownloadInfo[]>([]);
    const loadDownloads = async () => {
        downloads.value = await getDownloadList() || [];
    };

    onMounted(() => {
        onIpcEvent(IPC_EVENT_DOWNLOAD_INFO, (info: DownloadInfo) => {
            const presentInfo = downloads.value.find(i => i.post.url === info.post.url);
            let shouldSort = false;
            if (presentInfo) {
                downloads.value[downloads.value.indexOf(presentInfo)] = info;
                shouldSort = presentInfo.status !== info.status
            } else {
                downloads.value.push(info);
                shouldSort = true;
            }

            if (shouldSort) {
                downloads.value = [...downloads.value].sort(sorter);
            }
        });
        loadDownloads();
    });

    return {
        downloads,
        loadDownloads
    };
}