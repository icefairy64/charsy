import { onMounted, onUnmounted, Ref } from 'vue';

export interface Hotkey {
    key: 'Ctrl' | 'Alt' | 'Shift' | 'Meta' | 'Escape' | 'Enter' | 'Tab' | ' ' | string,
    handler: () => boolean | undefined
    shift?: boolean,
    ctrl?: boolean,
    alt?: boolean,
    meta?: boolean
}

export function useHotkeys(hotkeys: Ref<Hotkey[]>) {
    const listener = (ev: KeyboardEvent) => {
        for (const hotkey of hotkeys.value) {
            if (ev.key === hotkey.key && ev.altKey === !!hotkey.alt && ev.shiftKey === !!hotkey.shift && ev.ctrlKey === !!hotkey.ctrl && ev.metaKey === !!hotkey.meta) {
                if (hotkey.handler() !== false) {
                    ev.preventDefault();
                    ev.stopPropagation();
                }
            }
        }
    };

    onMounted(() => {
        document.addEventListener('keydown', listener);
    });

    onUnmounted(() => {
        document.removeEventListener('keydown', listener);
    });
}