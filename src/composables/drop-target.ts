import { onMounted, ref } from "vue";
export function useDropTarget(
    root: { value: HTMLElement },
    emit: (...args: any) => void,
    dropEffect: 'none' | 'copy' | 'link' | 'move' = 'copy',
    ddGroup: string = 'dd-group-default',
    acceptDrop: (dataTypes: readonly string[]) => boolean = (dataTypes) => dataTypes.find(dataType => dataType === ddGroup) != null
) {
    const dropValid = ref(false);

    const getDragData = (dataTransfer: DataTransfer) => {
        const dragData: Record<string, string> = {};
        for (const dataType of dataTransfer.types) {
            dragData[dataType] = dataTransfer.getData(dataType);
        }
        return dragData;
    };

    const onDragOver = (event: DragEvent) => {
        event.preventDefault();
        if (!acceptDrop(event.dataTransfer?.types || [])) {
            return;
        }
        if (event.dataTransfer != null) {
            event.dataTransfer.dropEffect = dropEffect;
        }
        dropValid.value = true;
    };

    const onDragLeave = () => {
        dropValid.value = false;
    };

    const onDrop = (event: DragEvent) => {
        event.preventDefault();
        const dragData = event.dataTransfer && getDragData(event.dataTransfer);
        emit('drop', dragData);
        dropValid.value = false;
    };

    onMounted(() => {
        root.value.addEventListener('dragover', onDragOver.bind(root.value));
        root.value.addEventListener('dragleave', onDragLeave.bind(root.value));
        root.value.addEventListener('drop', onDrop.bind(root.value));
    });

    return {
        dragOverValid: dropValid
    };
}