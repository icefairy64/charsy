import { getIpcEventObservable } from '@/ipc-event-reactive';
import { Subscription } from 'rxjs';
import { onUnmounted } from 'vue';

export type IpcEventSubscribeFn = (eventName: string, handler: (...args: any[]) => void) => void;

export default function useIpcEvents() {
    const eventSubscriptions: Subscription[] = [];
    const onIpcEvent: IpcEventSubscribeFn = (eventName: string, handler: (...args: any[]) => void) => {
        const observable = getIpcEventObservable(eventName);
        eventSubscriptions.push(observable.subscribe((args: any[]) => handler(...args)));
    };
    onUnmounted(() => {
        eventSubscriptions.forEach(subscription => subscription.unsubscribe());
    })
    return {
        onIpcEvent
    };
}