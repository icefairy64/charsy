import { inject, InjectionKey, provide, ref, Ref } from 'vue';
import { PageItem, ServiceDescriptor } from '@/electron/service/service';
import { PostCollection } from '@/electron/api/storage-symbols';
import { Bookmark } from '@/electron/api/bookmark-symbols';

export interface BaseGalleryContext {
    path: string,
    items?: PageItem[],
    page?: string | number
}

export interface ServiceGalleryContext extends BaseGalleryContext {
    pathType: 'service',
    descriptor?: ServiceDescriptor
}

export interface SearchGalleryContext extends BaseGalleryContext {
    pathType: 'search',
    descriptor?: {
        query: string
    }
}

export interface CollectionGalleryContext extends BaseGalleryContext {
    pathType: 'collection',
    descriptor: PostCollection
}

export interface BookmarkGalleryContext extends BaseGalleryContext {
    pathType: 'bookmark',
    descriptor: Bookmark
}

export type GalleryContext = ServiceGalleryContext | SearchGalleryContext | CollectionGalleryContext | BookmarkGalleryContext;

export const GALLERY_CONTEXT_KEY: InjectionKey<Ref<GalleryContext | null>> = Symbol();
export const GALLERY_CONTEXT_UPDATE_KEY: InjectionKey<(context: GalleryContext | null) => void> = Symbol();

export function provideGalleryContext() {
    const context = ref<GalleryContext | null>(null);

    const updateContext = (newContext: GalleryContext | null) => {
        context.value = newContext;
    }

    provide(GALLERY_CONTEXT_KEY, context);
    provide(GALLERY_CONTEXT_UPDATE_KEY, updateContext);

    return {
        context,
        updateContext
    };
}

export function useGalleryContext() {
    const context = inject(GALLERY_CONTEXT_KEY),
        updateContext = inject(GALLERY_CONTEXT_UPDATE_KEY);

    return {
        context,
        updateContext
    };
}