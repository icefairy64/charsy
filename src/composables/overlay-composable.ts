import { onMounted, ref } from 'vue';
import { globalEventBus } from '@/event-bus';
import { OVERLAY_HIDE, OVERLAY_SHOW } from '@/ui-events';

export function useOverlayStatus() {
    const statusRef = ref();

    onMounted(() => {
        let overlays = 0;
        function setOverlays(newOverlays: number) {
            overlays = newOverlays;
            statusRef.value = overlays > 0;
        }
        globalEventBus.on(OVERLAY_SHOW, () => setOverlays(overlays + 1));
        globalEventBus.on(OVERLAY_HIDE, () => setOverlays(overlays - 1));
    });

    return {
        overlayStatus: statusRef
    };
}