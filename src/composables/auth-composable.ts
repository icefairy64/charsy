import { onMounted, Ref, ref } from 'vue';
import { ServiceUserInfo } from '@/electron/api/service-symbols';
import { authenticateService, getServiceUserData, isServiceAuthSupported } from '@/api/service';
import { watch } from 'vue';

export function useServiceAuth(path: Ref<string>, errorHandler?: (error: string) => void) {
    const userData = ref<ServiceUserInfo | null>(null),
        userDataLoading = ref(false),
        authSupported = ref(false),
        loadUserData = async () => {
            userData.value = null;
            if (!authSupported.value) {
                return;
            }
            try {
                userDataLoading.value = true;
                userData.value = await getServiceUserData(path.value);
            }
            catch (e) {
                errorHandler?.(`Failed to retrieve user data: ${e.toString()}`);
            }
            finally {
                userDataLoading.value = false;
            }
        },
        authenticate = async (credentials: any) => {
            if (!authSupported.value) {
                errorHandler?.('Auth is not supported for this service');
            }
            try {
                await authenticateService(path.value, credentials);
                await loadUserData();
            }
            catch (e) {
                errorHandler?.(`Failed to authenticate: ${e.toString()}`);
            }
        },
        checkAuthSupport = async () => {
            try {
                authSupported.value = false;
                authSupported.value = await isServiceAuthSupported(path.value);
            }
            catch (e) {
                errorHandler?.(`Failed to check auth support: ${e.toString()}`);
            }
        },
        init = async () => {
            await checkAuthSupport();
            await loadUserData();
        };
    onMounted(init);
    watch(path, init);
    return {
        userData,
        authenticate,
        authSupported,
        userDataLoading
    }
}