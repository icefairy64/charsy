import { Post } from '@/electron/service/service';
import { computed } from '@vue/reactivity';
import { addPostToCollection, removePostFromCollection } from '@/api/storage';
import { MenuItem, useContextMenu } from '@/composables/context-menu-composable';
import { Ref } from 'vue';
import { PostCollection } from '@/electron/api/storage-symbols';
import { ToastConfig } from '@/composables/toast-composable';

export function useGalleryItemMenu(
    root: Ref<HTMLElement> | undefined,
    galleryType: string,
    galleryDescriptor: any,
    showInfoToast: (options: ToastConfig) => void,
    showErrorToast: (options: ToastConfig) => void,
    allCollections: Ref<PostCollection[]>,
    getActionableItems: () => Post[] | undefined,
    postCollections?: Ref<string[]>,
    beforeContextMenuFn?: () => boolean | undefined
) {
    const contextMenuItems = computed(() => {
        const collectionsEligibleForAddition = allCollections.value.filter(c => !postCollections?.value.includes(c.name));
        const actionableItems = getActionableItems?.();
        if (!actionableItems) {
            return [];
        }
        const items: MenuItem[] = [{
            text: `Add ${actionableItems.length > 1 ? `${actionableItems.length} ` : ''}to collection...`,
            items: collectionsEligibleForAddition.map(collection => ({
                text: collection.name,
                handler: async () => {
                    const results = await Promise.all(actionableItems.map(post =>
                        addPostToCollection(post, collection.name)
                            .then(() => true)
                            .catch(e => {
                                showErrorToast({
                                    summary: 'Failed to add post',
                                    details: e.toString()
                                });
                                return false;
                            })
                    ));
                    const successfulResultCount = results.filter(x => x).length;
                    if (successfulResultCount > 0) {
                        showInfoToast({
                            summary: actionableItems.length === 1 ? 'Added!' : `Added ${successfulResultCount} items`,
                            timeout: 1000
                        });
                    }
                }
            })),
            expanded: true
        }];
        if (galleryType === 'collection') {
            items.push({
                text: `Remove ${actionableItems.length > 1 ? `${actionableItems.length} ` : ''}from current collection`,
                handler: async () => {
                    const results = await Promise.all(actionableItems.map(post =>
                        removePostFromCollection(post, galleryDescriptor.name)
                            .then(() => true)
                            .catch(e => {
                                showErrorToast({
                                    summary: 'Failed to remove post',
                                    details: e.toString()
                                });
                                return false;
                            })
                    ));
                    const successfulResultCount = results.filter(x => x).length;
                    if (successfulResultCount > 0) {
                        showInfoToast({
                            summary: actionableItems.length === 1 ? 'Removed!' : `Removed ${successfulResultCount} items`,
                            timeout: 1000
                        });
                    }
                }
            })
        }
        return items;
    });

    return useContextMenu(root, contextMenuItems, beforeContextMenuFn);
}