import { onMounted, Ref } from 'vue';
import _ from 'lodash';
import { globalEventBus } from '@/event-bus';
import { CONTEXT_MENU_OPEN_REQUEST } from '@/ui-events';

export interface MenuItem {
    icon?: string,
    text: string,
    items?: MenuItem[],
    handler?: () => void,
    expanded?: boolean
}

export interface MenuShowOptions {
    at?: {
        x: number,
        y: number
    },
    direction: 'down' | 'up',
    event?: MouseEvent
}

export function useContextMenu(root: Ref<HTMLElement> | undefined, items: Ref<MenuItem[]>, beforeFn?: () => boolean | undefined) {
    if (root) {
        onMounted(() => {
            root.value.addEventListener('contextmenu', ev => {
                ev.preventDefault();
                if (beforeFn?.() !== false) {
                    if (!_.isEmpty(items.value)) {
                        globalEventBus.fireEvent(CONTEXT_MENU_OPEN_REQUEST, ev, items.value);
                    }
                }
            });
        });
    }

    return {
        showMenu: (options: MenuShowOptions) => {
            globalEventBus.fireEvent(CONTEXT_MENU_OPEN_REQUEST, options.event, items.value, options);
        }
    }
}