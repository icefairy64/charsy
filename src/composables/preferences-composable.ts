import { IPC_EVENT_PREFS_UPDATED, Preferences } from '@/electron/api/preferences-symbols';
import { inject, InjectionKey, provide, Ref, ref } from 'vue';
import { IpcEventSubscribeFn } from '@/composables/ipc-event-composable';
import { useIpcObservable } from '@/composables/rx-composable';
import { getCurrentPreferences } from '@/api/preferences';
import { computed } from '@vue/reactivity';

export const PREFERENCES_INJECTION_KEY: InjectionKey<Ref<Preferences>> = Symbol();

export function usePreferences<T>(mapper: (prefs: Preferences) => T) {
    const prefsRef = inject(PREFERENCES_INJECTION_KEY),
        valueRef = computed(() => prefsRef?.value ? mapper(prefsRef?.value) : null)

    return {
        valueRef
    };
}

export function useIpcPreferences(onIpcEvent: IpcEventSubscribeFn) {
    const valueRef = ref<Preferences | null>(null);

    const { observable: prefs$ } = useIpcObservable(
        onIpcEvent,
        () => getCurrentPreferences(),
        IPC_EVENT_PREFS_UPDATED,
        undefined);

    prefs$.subscribe(value => valueRef.value = value);

    return {
        valueRef
    };
}

export function providePreferences(prefs: Ref<Preferences>) {
    provide(PREFERENCES_INJECTION_KEY, prefs);
}