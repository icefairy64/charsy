import { getCurrentInstance, Ref } from 'vue';
import { Preferences } from '@/electron/api/preferences-symbols';
import { usePreferences } from '@/composables/preferences-composable';
import { createLogger } from '@/logging';
import { computed } from '@vue/reactivity';

export function useLogger(componentName?: string, preferences?: Ref<Preferences | null>) {
    if (!preferences) {
        const { valueRef } = usePreferences(x => x);
        preferences = valueRef;
    }
    const instance = getCurrentInstance(),
        instanceTypeName = instance?.type.name,
        loggerRef = computed(() => {
            return createLogger(componentName || instanceTypeName || 'Unknown', preferences?.value?.logging?.threshold)
        });
    return {
        logger: loggerRef
    };
}