import { inject, InjectionKey, provide, Ref } from 'vue';
import { Deferred } from '@/electron/util/deferred';

export const CONFIRMATION_DIALOG_CONFIRM: InjectionKey<(title?: string, body?: string, buttons?: { confirm: string, decline: string }) => Promise<boolean>> = Symbol();

export function provideConfirmationDialog(show: Ref<(title?: string, body?: string, buttons?: { confirm: string, decline: string }) => void>) {
    let deferred: Deferred<boolean> | undefined;

    const onConfirmationResult = (result: boolean) => {
        deferred?.resolve(result);
        deferred = undefined;
    };

    const confirm = (title?: string, body?: string, buttons?: { confirm: string, decline: string }) => {
        if (deferred) {
            return Promise.reject('Another confirmation is in progress');
        }
        deferred = new Deferred<boolean>();
        show.value(title, body, buttons);
        return deferred.promise;
    };

    provide(CONFIRMATION_DIALOG_CONFIRM, confirm);

    return {
        confirm,
        onConfirmationResult
    };
}

export function useConfirmationDialog() {
    const confirm = inject(CONFIRMATION_DIALOG_CONFIRM);
    return {
        confirm
    };
}