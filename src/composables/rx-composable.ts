import { merge, Observable, Subject} from 'rxjs';
import { onUnmounted, Ref, ref, watch } from 'vue';
import { IpcEventSubscribeFn } from '@/composables/ipc-event-composable';
import { startWith, switchMap } from 'rxjs/operators';
import { from } from 'rxjs';

export function useObservable<T>(observable: Observable<T>) {
    const valueRef = ref<T>();
    const subscription = observable.subscribe(val => valueRef.value = val);
    onUnmounted(() => {
        subscription.unsubscribe();
    });
    return {
        valueRef
    };
}

function getRefObservable<T>(ref: Ref<T>): Observable<T> {
    const subject = new Subject<T>();
    watch(ref, val => subject.next(val));
    return subject.pipe(
        startWith(ref.value)
    );
}

export function useIpcObservable<T>(onIpcEvent: IpcEventSubscribeFn,
                                       snapshotFn: () => Promise<T>,
                                       updateEventName: string,
                                       snapshotParamRef: undefined,
                                       updateArgsMapper?: (...args: any) => T): { observable: Observable<T> };

export function useIpcObservable<T, P>(onIpcEvent: IpcEventSubscribeFn,
                                       snapshotFn: (param: P) => Promise<T>,
                                       updateEventName: string,
                                       snapshotParamRef: Ref<P>,
                                       updateArgsMapper?: (...args: any) => T): { observable: Observable<T> };

export function useIpcObservable<T, P>(onIpcEvent: IpcEventSubscribeFn,
                                    snapshotFn: (() => Promise<T>) | ((param: P) => Promise<T>),
                                    updateEventName: string,
                                    snapshotParamRef?: Ref<P>,
                                    updateArgsMapper?: (...args: any) => T): { observable: Observable<T> } {
    let snapshot$: Observable<T>;
    if (snapshotParamRef) {
        const param$ = getRefObservable(snapshotParamRef);
        snapshot$ = param$.pipe(
            switchMap(param => {
                const subject = new Subject<T>();
                snapshotFn(param).then(val => subject.next(val));
                return subject;
            })
        )
    } else {
        // @ts-ignore
        snapshot$ = from(snapshotFn());
    }

    const eventDataSubject = new Subject<T>();
    onIpcEvent(updateEventName, (...args) => {
        let val = args[0];
        if (updateArgsMapper) {
            val = updateArgsMapper(...args);
        }
        eventDataSubject.next(val);
    });

    const combined$ = merge(snapshot$, eventDataSubject);
    return {
        observable: combined$
    };
}