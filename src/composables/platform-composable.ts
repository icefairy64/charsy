import { inject, InjectionKey, onMounted, onUnmounted, provide, ref, Ref } from 'vue';
import _ from 'lodash';
import { DelayedTask } from '@/electron/util/delayed-task';

export type PlatformType = 'WINDOWS' | 'MACOS' | 'LINUX' | 'BROWSER' | 'UNKNOWN';
export interface ScreenLayoutType {
    orientation: 'VERTICAL' | 'HORIZONTAL',
    size: 'LARGE' | 'SMALL'
}

const PLATFORM_INJECTION_KEY: InjectionKey<PlatformType> = Symbol();
const LAYOUT_TYPE_INJECTION_KEY: InjectionKey<Ref<ScreenLayoutType>> = Symbol();

export function usePlatform() {
    return {
        platform: inject(PLATFORM_INJECTION_KEY)
    };
}

export function providePlatform() {
    function initPlatform() {
        if (PX_PLATFORM_BROWSER) {
            return 'BROWSER';
        }
        const userAgent = navigator.userAgent;
        if (userAgent.includes('Win')) {
            return 'WINDOWS';
        } else if (userAgent.includes('Mac')) {
            return 'MACOS';
        } else if (userAgent.includes('Linux')) {
            return 'LINUX';
        } else {
            return 'UNKNOWN'
        }
    }

    const platform = initPlatform();
    provide(PLATFORM_INJECTION_KEY, platform);
    return {
        platform: platform
    };
}

export function useScreenLayout() {
    return {
        screenLayout: inject(LAYOUT_TYPE_INJECTION_KEY)
    };
}

export function provideScreenLayout() {
    const layoutType = ref<ScreenLayoutType>({
        orientation: 'HORIZONTAL',
        size: 'LARGE'
    });

    if (PX_PLATFORM_BROWSER) {
        const observer = new ResizeObserver(entries => {
            const updateTask = new DelayedTask((layout: ScreenLayoutType) => {
                layoutType.value = layout;
            });
            const entry = _.last(entries);
            if (entry) {
                const size = (_.isArray(entry.contentBoxSize) ? entry.contentBoxSize[0] : entry.contentBoxSize) as ResizeObserverSize;
                updateTask.delay(100, [{
                    orientation: size.blockSize * 0.9 > size.inlineSize ? 'VERTICAL' : 'HORIZONTAL',
                    size: size.inlineSize < 600 ? 'SMALL' : 'LARGE'
                }]);
            }
        });

        onMounted(() => {
            observer.observe(document.body);
        });

        onUnmounted(() => {
            observer.disconnect();
        });
    }

    provide(LAYOUT_TYPE_INJECTION_KEY, layoutType);

    return {
        screenLayout: layoutType
    };
}