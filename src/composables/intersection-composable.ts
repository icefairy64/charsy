import { inject, InjectionKey, onMounted, onUnmounted, provide, Ref } from 'vue';

export const INTERSECTION_ADD_CHILD_EL: InjectionKey<(key: string, el: HTMLElement) => void> = Symbol();
export const INTERSECTION_REMOVE_CHILD_EL: InjectionKey<(key: string, el: HTMLElement) => void> = Symbol();

export function useIntersectionContainer(root: Ref<HTMLElement>, onIntersectionChange: (key: string, intersecting: boolean) => void) {
    let observer: IntersectionObserver;
    const elMap: Record<string, HTMLElement> = {};

    const addEl = (key: string, el: HTMLElement) => {
        observer.observe(el);
        elMap[key] = el;
    }

    const removeEl = (key: string, el: HTMLElement) => {
        delete elMap[key];
        onIntersectionChange(key, false);
        observer.unobserve(el);
    }

    onMounted(() => {
        observer = new IntersectionObserver(entries => {
            for (const entry of entries) {
                const key = Object.entries(elMap).find(e => e[1] === entry.target)?.[0];
                if (key) {
                    onIntersectionChange(key, entry.isIntersecting);
                }
            }
        }, {
            root: root.value,
            threshold: [0.05, 0.95]
        });
    });

    onUnmounted(() => {
        observer.disconnect();
    });

    provide(INTERSECTION_ADD_CHILD_EL, addEl);
    provide(INTERSECTION_REMOVE_CHILD_EL, removeEl);

    return {
        addEl,
        removeEl
    };
}

export function useIntersectionItem(root: Ref<HTMLElement>, key: string) {
    const addEl = inject(INTERSECTION_ADD_CHILD_EL)!;
    const removeEl = inject(INTERSECTION_REMOVE_CHILD_EL)!;
    let el: HTMLElement;

    onMounted(() => {
        el = root.value;
        addEl(key, root.value);
    });

    onUnmounted(() => {
        removeEl(key, el);
    });
}