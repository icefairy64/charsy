import { globalEventBus } from '@/event-bus';
import _ from 'lodash';

export const TOAST_SHOW_REQUEST = 'toast-show-request';

export interface ToastConfig {
    classList?: string[],
    timeout?: number,
    summary: string,
    details?: string
}

export function useToasts() {
    function showToast(cls: string, config: ToastConfig | string) {
        if (_.isString(config)) {
            config = {
                summary: config
            };
        } else {
            config = _.cloneDeep(config);
        }
        config.classList = [...(config.classList ?? []), cls];
        globalEventBus.fireEvent(TOAST_SHOW_REQUEST, config);
    }

    return {
        showErrorToast: _.curry(showToast)('px-toast-error'),
        showWarnToast: _.curry(showToast)('px-toast-warn'),
        showInfoToast: _.curry(showToast)('px-toast-info')
    }
}