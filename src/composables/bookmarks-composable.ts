import { IpcEventSubscribeFn } from '@/composables/ipc-event-composable';
import { onMounted, ref } from 'vue';
import {
    Bookmark,
    IPC_EVENT_BOOKMARK_CREATED,
    IPC_EVENT_BOOKMARK_MODIFIED,
    IPC_EVENT_BOOKMARK_REMOVED
} from '@/electron/api/bookmark-symbols';
import { getBookmarkList } from '@/api/bookmark';

export function useBookmarks(onIpcEvent: IpcEventSubscribeFn) {
    const bookmarks = ref([] as Bookmark[]);

    onMounted(async () => {
        bookmarks.value = await getBookmarkList();
        onIpcEvent(IPC_EVENT_BOOKMARK_CREATED, bookmark => {
            bookmarks.value.push(bookmark);
        });
        onIpcEvent(IPC_EVENT_BOOKMARK_REMOVED, bookmarkName => {
            bookmarks.value = bookmarks.value.filter(b => b.name !== bookmarkName);
        });
        onIpcEvent(IPC_EVENT_BOOKMARK_MODIFIED, bookmark => {
            const oldBookmark = bookmarks.value.find(b => b.name === bookmark.name);
            if (oldBookmark) {
                bookmarks.value[bookmarks.value.indexOf(oldBookmark)] = {
                    ...oldBookmark,
                    ...bookmark
                };
            }
        });
    });

    return {
        bookmarks
    };
}