import { onMounted, ref } from 'vue';
import { getCollectionList } from '@/api/storage';
import {
    IPC_EVENT_COLLECTION_CREATED,
    IPC_EVENT_COLLECTION_MODIFIED, IPC_EVENT_COLLECTION_REMOVED,
    PostCollection
} from '@/electron/api/storage-symbols';
import { IpcEventSubscribeFn } from '@/composables/ipc-event-composable';
import {
    IPC_EVENT_COLLECTION_SYNC_FINISHED,
    IPC_EVENT_COLLECTION_SYNC_STARTED
} from '@/electron/api/service-collection-sync-symbols';
import _ from 'lodash';

export function useCollections(onIpcEvent: IpcEventSubscribeFn) {
    const collections = ref<PostCollection[]>([]),
        syncingCollectionNames = ref<string[]>([]);
    onMounted(async () => {
        collections.value = await getCollectionList();
        onIpcEvent(IPC_EVENT_COLLECTION_CREATED, collection => {
            collections.value.push(collection);
        });
        onIpcEvent(IPC_EVENT_COLLECTION_REMOVED, name => {
            collections.value = collections.value.filter(c => c.name !== name);
        });
        onIpcEvent(IPC_EVENT_COLLECTION_MODIFIED, collection => {
            const oldCollection = collections.value.find(c => c.name === collection.name);
            if (oldCollection) {
                collections.value[collections.value.indexOf(oldCollection)] = {
                    ...oldCollection,
                    ...collection
                };
            }
        });
        onIpcEvent(IPC_EVENT_COLLECTION_SYNC_STARTED, collectionName => {
            syncingCollectionNames.value.push(collectionName);
        });
        onIpcEvent(IPC_EVENT_COLLECTION_SYNC_FINISHED, collectionName => {
            syncingCollectionNames.value = _.without(syncingCollectionNames.value, collectionName);
        });
    });
    return {
        collections,
        syncingCollectionNames
    };
}