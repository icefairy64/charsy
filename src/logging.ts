import { Logger, LogLevel } from '@/electron/log/logger';
import { consoleReporter } from '@/electron/log/console';

let logger: Logger;

export function initLogger(logLevel?: LogLevel) {
    logger = new Logger('Root', logLevel);
    logger.addReporter(consoleReporter);
    return logger;
}

export function createLogger(componentName: string, level?: LogLevel) {
    return logger.createChild(componentName, level);
}