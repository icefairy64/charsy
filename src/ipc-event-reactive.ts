import { Observable, Subject } from 'rxjs';
import { isElectron } from '@/util/remote-util';

export const eventSubjects: Record<string, Subject<any[]>> = {};

let createEventSubject: (eventName: string) => Subject<any[]>;

if (isElectron()) {
    createEventSubject = function (eventName: string) {
        const subject = new Subject<any[]>();
        CharsyApi.onMessage(eventName, (...args) => {
            subject.next(args.slice(1));
        });
        return subject;
    }
} else {
    createEventSubject = function () {
        return new Subject<any[]>();
    }
}

export function getIpcEventObservable(eventName: string) {
    return getIpcEventSubject(eventName).asObservable();
}

export function getIpcEventSubject(eventName: string) {
    if (eventSubjects[eventName] == null) {
        eventSubjects[eventName] = createEventSubject(eventName);
    }
    return eventSubjects[eventName];
}