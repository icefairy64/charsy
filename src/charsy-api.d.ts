declare module CharsyApi {
    function sendMessage<T>(channel: string, ...args: any): Promise<T>;
    function onMessage(channel: string, callback: (...args: any) => void);
}

// Defined at build-time
declare namespace PX_PLATFORM {
    const electron: boolean;
    const electronMain: boolean;
    const electronRenderer: boolean;
    const browser: boolean;
}

declare const PX_VERSION: {
    version: string,
    buildDate: number | undefined,
    buildCommit: string | undefined
};