const { contextBridge, ipcRenderer } = require('electron');

contextBridge.exposeInMainWorld('CharsyApi', {
    // FIXME: provide separate functions for each API method
    sendMessage: (channel, ...args) => ipcRenderer.invoke(channel, ...args),
    onMessage: (channel, fn) => ipcRenderer.on(channel, fn)
});