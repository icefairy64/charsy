export type EventBusListener = (...args: any[]) => void;
export type EventBusUnsubscriber = () => void;

export class EventBus {
    listeners: {
        [eventName: string]: Array<EventBusListener>
    } = {};

    on(eventName: string, handler: EventBusListener): EventBusUnsubscriber {
        if (!this.listeners[eventName]) {
            this.listeners[eventName] = [];
        }
        this.listeners[eventName].push(handler);
        return () => this.listeners[eventName] = this.listeners[eventName].filter(l => l !== handler);
    }

    fireEvent(eventName: string, ...args: any[]) {
        if (this.listeners[eventName]) {
            for (const listener of this.listeners[eventName]) {
                listener(...args);
            }
        }
    }
}

export const globalEventBus = new EventBus();