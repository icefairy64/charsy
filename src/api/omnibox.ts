import { IPC_OMNIBOX_QUERY, OmniboxResult } from '@/electron/api/omnibox-symbols';
import { invokeIpcFn } from '@/util/remote-util';

export async function omniboxQuery(contexts: string[], query: string): Promise<OmniboxResult[]> {
    return invokeIpcFn(IPC_OMNIBOX_QUERY, contexts, query);
}