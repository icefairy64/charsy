import { Bookmark, BookmarkPost } from '@/electron/api/bookmark-symbols';
import {
    IPC_BOOKMARK_CREATE,
    IPC_BOOKMARK_LIST,
    IPC_BOOKMARK_MARK_ALL_POSTS_AS_SEEN,
    IPC_BOOKMARK_MARK_POST_AS_SEEN,
    IPC_BOOKMARK_MODIFY,
    IPC_BOOKMARK_POST_IS_SEEN,
    IPC_BOOKMARK_POST_LIST,
    IPC_BOOKMARK_REMOVE,
    IPC_BOOKMARK_UNSEEN_POST_COUNT
} from '@/electron/api/storage-symbols';
import { Post } from '@/electron/service/service';
import _ from 'lodash';

import { invokeIpcFn } from '@/util/remote-util';

export async function getBookmarkList(): Promise<Array<Bookmark>> {
    return invokeIpcFn(IPC_BOOKMARK_LIST);
}

export async function createBookmark(bookmark: Bookmark, seenPosts: Array<Post>) {
    return invokeIpcFn(IPC_BOOKMARK_CREATE, _.cloneDeep(bookmark), _.cloneDeep(seenPosts));
}

export async function removeBookmark(bookmark: Bookmark) {
    return invokeIpcFn(IPC_BOOKMARK_REMOVE, bookmark);
}

export async function modifyBookmark(bookmark: Bookmark) {
    return invokeIpcFn(IPC_BOOKMARK_MODIFY, bookmark);
}

export async function getPostsInBookmark(bookmarkName: string, offset: number, limit: number): Promise<Array<Post & BookmarkPost>> {
    return invokeIpcFn(IPC_BOOKMARK_POST_LIST, bookmarkName, offset, limit);
}

export async function getBookmarkUnseenPostCount(bookmarkName: string): Promise<number> {
    return invokeIpcFn(IPC_BOOKMARK_UNSEEN_POST_COUNT, bookmarkName);
}

export async function isBookmarkPostSeen(bookmarkName: string, postUrl: string): Promise<boolean> {
    return invokeIpcFn(IPC_BOOKMARK_POST_IS_SEEN, bookmarkName, postUrl);
}

export async function markAllPostsForBookmarkAsSeen(bookmarkName: string) {
    return invokeIpcFn(IPC_BOOKMARK_MARK_ALL_POSTS_AS_SEEN, bookmarkName);
}

export async function markPostForBookmarkAsSeen(bookmarkName: string, post: Post) {
    return invokeIpcFn(IPC_BOOKMARK_MARK_POST_AS_SEEN, bookmarkName, post.url);
}