import { IPC_PREFS_GET_CURRENT, IPC_PREFS_UPDATE, Preferences } from '@/electron/api/preferences-symbols';
import { invokeIpcFn } from '@/util/remote-util';

export async function getCurrentPreferences(): Promise<Preferences> {
    return invokeIpcFn(IPC_PREFS_GET_CURRENT);
}

export async function updatePreferences(preferences: Preferences): Promise<void> {
    return invokeIpcFn(IPC_PREFS_UPDATE, preferences);
}