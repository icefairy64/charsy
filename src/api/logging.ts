import { LogLevel } from '@/electron/log/logger';
import { invokeIpcFn } from '@/util/remote-util';
import { IPC_LOG_REPORT } from '@/electron/api/logging-symbols';

export function remoteLog(level: LogLevel, timestamp: number, message: string, error?: Error) {
    return invokeIpcFn(IPC_LOG_REPORT, level, timestamp, message, error);
}