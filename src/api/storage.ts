import {
    IPC_COLLECTION_ADD_POST,
    IPC_COLLECTION_CREATE,
    IPC_COLLECTION_GET_POSTS,
    IPC_COLLECTION_LIST,
    IPC_COLLECTION_LIST_FOR_POST,
    IPC_COLLECTION_MODIFY,
    IPC_COLLECTION_REMOVE,
    IPC_COLLECTION_REMOVE_POST,
    PostCollection
} from '@/electron/api/storage-symbols';
import { Post } from '@/electron/service/service';

import _ from 'lodash';
import { invokeIpcFn } from '@/util/remote-util';

export async function createCollection(collection: PostCollection): Promise<void> {
    return invokeIpcFn(IPC_COLLECTION_CREATE, collection);
}

export async function removeCollection(collection: PostCollection): Promise<void> {
    return invokeIpcFn(IPC_COLLECTION_REMOVE, collection);
}

export async function modifyCollection(collection: PostCollection): Promise<void> {
    return invokeIpcFn(IPC_COLLECTION_MODIFY, collection);
}

export async function getCollectionList(): Promise<Array<PostCollection>> {
    return invokeIpcFn(IPC_COLLECTION_LIST);
}

export async function addPostToCollection(post: Post, collectionName: string): Promise<void> {
    return invokeIpcFn(IPC_COLLECTION_ADD_POST, collectionName, _.cloneDeep(post));
}

export async function removePostFromCollection(post: Post, collectionName: string): Promise<void> {
    return invokeIpcFn(IPC_COLLECTION_REMOVE_POST, collectionName, post);
}

export async function getPostsInCollection(collectionName: string, query: string | null, offset: number, limit: number): Promise<Array<Post>> {
    return invokeIpcFn(IPC_COLLECTION_GET_POSTS, collectionName, query, offset, limit);
}

export async function getPostCollections(postUrl: String): Promise<Array<string>> {
    return invokeIpcFn(IPC_COLLECTION_LIST_FOR_POST, postUrl);
}