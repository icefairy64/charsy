import { Post } from '@/electron/service/service';
import { isElectron } from '@/util/remote-util';

export let openPostInWindow: (post: Post, forceNewWindow?: boolean) => void;

if (isElectron()) {
    openPostInWindow = function (post: Post, forceNewWindow: boolean = false) {
        throw new Error('Deprecated');
    }
}