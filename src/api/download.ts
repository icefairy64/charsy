import { DownloadStatus } from '@/electron/api/download';
import {
    DownloadInfo,
    IPC_DOWNLOAD_ENQUEUE,
    IPC_DOWNLOAD_LIST,
    IPC_DOWNLOAD_POSTS_STATUS
} from '@/electron/api/download-symbols';
import { Post } from '@/electron/service/service';
import { invokeIpcFn } from '@/util/remote-util';

export async function getDownloadList(): Promise<DownloadInfo[]> {
    return invokeIpcFn(IPC_DOWNLOAD_LIST);
}

export async function enqueuePostDownload(post: Post): Promise<void> {
    return invokeIpcFn(IPC_DOWNLOAD_ENQUEUE, post);
}

export async function getPostsDownloadStatuses(posts: Post[]): Promise<(DownloadStatus | null)[]> {
    return invokeIpcFn(IPC_DOWNLOAD_POSTS_STATUS, posts.map(p => p.url));
}