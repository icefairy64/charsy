import { getPostsInCollection } from '@/api/storage';

import {
    IPC_SERVICE_AUTH_SUPPORTED,
    IPC_SERVICE_AUTH_USER_INFO, IPC_SERVICE_AUTHENTICATE, IPC_SERVICE_DESCRIPTOR_CONFIG_LIST,
    IPC_SERVICE_HOME_SCREEN, IPC_SERVICE_HOME_SCREEN_SUPPORTED,
    IPC_SERVICE_LIST,
    IPC_SERVICE_PAGE, IPC_SERVICE_SEARCH_AUTOCOMPLETE,
    IPC_SERVICE_SEARCH_PATH,
    ServiceDescription, ServiceDescriptorConfigInfo
} from '@/electron/api/service-symbols';
import { Page, SearchAutocompleteSuggestion, ServiceHomeScreenItem } from '@/electron/service/service';
import { invokeIpcFn } from '@/util/remote-util';
import { OmniboxResult } from '@/electron/api/omnibox-symbols';

const collectionPathPrefix = '/collection/';
const collectionPageSize = 40;
const collectionPathRegex = /\/collection\/([^/]+?)(?:\/(.+?))?$/

function parseCollectionPath(path: string) {
    const match = collectionPathRegex.exec(path);
    if (match) {
        return {
            collectionName: match[1],
            rest: match[2]
        };
    }
}

export async function loadServicePage(path: string, page: number | string, allowCache?: boolean): Promise<Page> {
    const collectionPathInfo = parseCollectionPath(path);
    if (collectionPathInfo) {
        const { collectionName, rest } = collectionPathInfo;
        let query = null;
        const match = /search:(.+?)$/.exec(rest);
        if (match?.length === 2) {
            query = match[1];
        }
        const posts = await getPostsInCollection(collectionName, query, (+page - 1) * collectionPageSize, collectionPageSize);
        return {
            content: posts,
            nextPageToken: posts.length === collectionPageSize ? String(+page + 1) : undefined
        };
    } else {
        return invokeIpcFn(IPC_SERVICE_PAGE, path, page, allowCache);
    }
}

export async function getServiceList(): Promise<Array<ServiceDescription>> {
    return invokeIpcFn(IPC_SERVICE_LIST);
}

export async function getServiceSearchPath(path: string, query: string): Promise<string | null> {
    const collectionPathInfo = parseCollectionPath(path);
    if (collectionPathInfo) {
        return `${collectionPathPrefix}${collectionPathInfo.collectionName}/search:${query}`;
    } else {
        return invokeIpcFn(IPC_SERVICE_SEARCH_PATH, path, query);
    }
}

export function getOmniboxResultDisplayName(result: OmniboxResult) {
    if (result.resultType === 'COLLECTION') {
        return `Collection - ${result.resultName}`;
    } else if (result.resultType === 'BOOKMARK') {
        return `Bookmark - ${result.resultName}`;
    } else if (result.resultType === 'COLLECTION_SEARCH') {
        return `Collection - ${result.collectionName}`
    }
    return result.resultName;
}

export async function getServiceSearchAutocompleteSuggestions(path: string, query: string): Promise<SearchAutocompleteSuggestion[] | null> {
    return invokeIpcFn(IPC_SERVICE_SEARCH_AUTOCOMPLETE, path || '', query);
}
export async function getServiceHomeScreen(path: string): Promise<ServiceHomeScreenItem[] | null> {
    return invokeIpcFn(IPC_SERVICE_HOME_SCREEN, path);
}

export async function isServiceHomeScreenSupported(path: string): Promise<boolean> {
    return invokeIpcFn(IPC_SERVICE_HOME_SCREEN_SUPPORTED, path);
}

export async function getServiceUserData(path: string): Promise<any> {
    return invokeIpcFn(IPC_SERVICE_AUTH_USER_INFO, path);
}

export async function authenticateService(path: string, credentials: any): Promise<void> {
    return invokeIpcFn(IPC_SERVICE_AUTHENTICATE, path, credentials);
}

export async function isServiceAuthSupported(path: string): Promise<boolean> {
    return invokeIpcFn(IPC_SERVICE_AUTH_SUPPORTED, path);
}

export async function getServiceDescriptorConfigs(): Promise<Record<string, ServiceDescriptorConfigInfo | undefined>> {
    return invokeIpcFn(IPC_SERVICE_DESCRIPTOR_CONFIG_LIST);
}