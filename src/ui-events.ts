export const GALLERY_SEARCH = 'gallery-search';
export const CONTEXT_MENU_OPEN_REQUEST = 'context-menu-open-request';
export const OMNIBOX_SUGGESTION_SELECT = 'omnibox-suggestion-select';
export const SEARCH_REGISTER_ACTIVE_VIEW = 'search-register-active-view';
export const SEARCH_LEAVE_ACTIVE_VIEW = 'search-leave-active-view';
export const OVERLAY_SHOW = 'overlay-show';
export const OVERLAY_HIDE = 'overlay-hide';