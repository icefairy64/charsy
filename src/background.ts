import { ServiceApi } from './electron/api/service';
import { app, BrowserWindow, protocol } from 'electron';
import { WindowManagementApi } from './electron/api/window-management';
import { StorageApi } from './electron/api/storage';
import { BookmarkApi } from './electron/api/bookmark';
import { SCHEMA_CACHE_PASSTHROUGH } from '@/electron/api/storage-symbols';
import { DownloadApi } from '@/electron/api/download';
import { promises as fsP, readFile } from 'fs';
import { ServiceCollectionSyncApi } from '@/electron/api/service-collection-sync';
import { URL } from 'url';
import path from 'path';
import OmniboxApi from '@/electron/api/omnibox';
import { PreferencesApi } from '@/electron/api/preferences';
import { initLogger } from '@/logging';
import { Logger } from '@/electron/log/logger';
import { LoggingApi } from '@/electron/api/logging';
import { FileLogReporter } from '@/electron/log/file';
import { ScreenApi } from '@/electron/api/screen';

let logger: Logger;
let fileReporter: FileLogReporter;

async function loadConfigurationFromFile<T>(path: string): Promise<T> {
    const content = await fsP.readFile(path, {
        encoding: 'utf8'
    });
    return JSON.parse(content);
}

async function startIpc() {
    logger.info('Initializing preferences');
    const preferencesApi = new PreferencesApi('preferences.json');
    preferencesApi.registerIpcHandlers();

    logger = initLogger(preferencesApi.getCurrentPrefs()?.logging?.threshold);
    fileReporter = new FileLogReporter();
    logger.addReporter(fileReporter.log.bind(fileReporter));

    logger.info('Initializing Omnibox');
    const omniboxApi = new OmniboxApi();
    omniboxApi.registerIpcHandlers();

    logger.info('Initializing services and storage');
    const serviceApi = new ServiceApi(process.env.SERVICE_CONFIG ?? 'services.json', omniboxApi, preferencesApi);
    const storageApi = new StorageApi(preferencesApi, process.env.DB_PATH ?? 'storage.sqlite');
    serviceApi.registerIpcHandlers();
    storageApi.registerIpcHandlers();

    storageApi.registerOmnibox(omniboxApi);

    new WindowManagementApi().registerIpcHandlers();

    logger.info('Initializing bookmarks');
    const bookmarkApi = new BookmarkApi(storageApi, serviceApi);
    bookmarkApi.registerIpcHandlers();

    logger.info('Initializing downloads');
    const downloadApi = new DownloadApi(await loadConfigurationFromFile(process.env.DOWNLOADS_CONFIG ?? 'downloads.json'), bookmarkApi, storageApi);
    downloadApi.registerIpcHandlers();

    logger.info('Initializing screens');
    const screensApi = new ScreenApi(preferencesApi, omniboxApi, storageApi, serviceApi);
    screensApi.registerIpcHandlers();

    logger.info('Initializing logging');
    const loggingApi = new LoggingApi();
    loggingApi.registerIpcHandlers();

    logger.info('Initializing collection sync');
    new ServiceCollectionSyncApi(serviceApi, storageApi);
}

function registerPrivelegedSchemes() {
    protocol.registerSchemesAsPrivileged([{
        scheme: `${SCHEMA_CACHE_PASSTHROUGH}s`,
        privileges: {
            secure: true,
            standard: true,
            supportFetchAPI: true,
            stream: true
        }
    }, {
        scheme: `${SCHEMA_CACHE_PASSTHROUGH}`,
        privileges: {
            secure: true,
            standard: true,
            supportFetchAPI: true,
            stream: true
        }
    }]);
}

// FIXME: figure out how to import this properly
// Pulled from Vue CLI Electron Builder plugin to avoid problematic ES6 import
function createProtocol(scheme: string) {
    protocol.registerBufferProtocol(
        scheme,
        (request, respond) => {
            let pathName = new URL(request.url).pathname
            pathName = decodeURI(pathName) // Needed in case URL contains spaces

            readFile(path.join(__dirname, pathName), (error, data) => {
                if (error) {
                    logger.error(`Failed to read ${pathName} on ${scheme} protocol`, error);
                }
                const extension = path.extname(pathName).toLowerCase()
                let mimeType = ''

                if (extension === '.js') {
                    mimeType = 'text/javascript'
                } else if (extension === '.html') {
                    mimeType = 'text/html'
                } else if (extension === '.css') {
                    mimeType = 'text/css'
                } else if (extension === '.svg' || extension === '.svgz') {
                    mimeType = 'image/svg+xml'
                } else if (extension === '.json') {
                    mimeType = 'application/json'
                }

                respond({ mimeType, data })
            })
        }
    )
}

async function createWindow() {
    // Create the browser window.
    const win = new BrowserWindow({
        width: 1280,
        height: 800,
        autoHideMenuBar: true,
        titleBarStyle: process.platform === 'darwin' ? 'hiddenInset' : 'hidden',
        titleBarOverlay: process.platform === 'darwin' ? true : {
            color: '#f1d205',
            symbolColor: 'black'
        },
        vibrancy: 'sidebar',
        transparent: process.platform === 'darwin',
        webPreferences: {
            // @ts-ignore
            preload: path.resolve(__dirname, 'preload.js')
        }
    });

    createProtocol('app')
    await win.loadURL(MAIN_WINDOW_VITE_DEV_SERVER_URL)
}

logger = initLogger();
registerPrivelegedSchemes();

app.whenReady().then(async () => {
    await startIpc();
    await createWindow();
}).catch(e => logger.error('Failed to start app', e));

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('before-quit', () => {
    fileReporter.free();
});

app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
        createWindow();
    }
});