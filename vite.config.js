import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  define: {
    PX_PLATFORM_BROWSER: true,
    PX_PLATFORM_ELECTRON: false,
    PX_PLATFORM_ELECTRON_MAIN: false,
    PX_VERSION: {
      version: '0.1.0'
    }
  },
  build: {
    rollupOptions: {
      treeshake: {
        preset: 'smallest',
        moduleSideEffects: true
      }
    }
  },
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "./src"),
      "~@": path.resolve(__dirname, "./src")
    },
  }
})