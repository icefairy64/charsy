import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  define: {
    PX_PLATFORM_BROWSER: false,
    PX_PLATFORM_ELECTRON: true,
    PX_PLATFORM_ELECTRON_MAIN: false,
    PX_VERSION: {
      version: '0.1.0'
    }
  },
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "./src"),
    },
  }
})